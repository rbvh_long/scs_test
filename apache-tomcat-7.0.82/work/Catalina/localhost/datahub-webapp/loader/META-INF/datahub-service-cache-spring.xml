<!--

    [y] hybris Platform

    Copyright (c) 2017 SAP SE or an SAP affiliate company.
    All rights reserved.

    This software is the confidential and proprietary information of SAP
    ("Confidential Information"). You shall not disclose such Confidential
    Information and shall use it only in accordance with the terms of the
    license agreement you entered into with SAP.

-->
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	   xmlns:hz="http://www.hazelcast.com/schema/spring"
	   xmlns:p="http://www.springframework.org/schema/p"
	   xmlns:cache="http://www.springframework.org/schema/cache"
	   xmlns="http://www.springframework.org/schema/beans"
	   xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
	   http://www.springframework.org/schema/cache http://www.springframework.org/schema/cache/spring-cache.xsd
	   http://www.hazelcast.com/schema/spring
       http://www.hazelcast.com/schema/spring/hazelcast-spring.xsd">

	<cache:annotation-driven />

	<hz:hazelcast id="hazelcastInstance">
		<hz:config>
			<hz:group name="${datahub.hz.group}" password="${datahub.hz.password}" />
			<hz:network port="${datahub.hz.port.number}" port-auto-increment="${datahub.hz.port.autoincrement}">
				<hz:join>
					<hz:multicast enabled="false" />
					<hz:tcp-ip enabled="true">
						<hz:member>${datahub.cluster.seed.hostname}</hz:member>
						<hz:member>${datahub.cluster.node.hostname}</hz:member>
					</hz:tcp-ip>
				</hz:join>
			</hz:network>
			<hz:cache name="rawItemStatusCountsCache" />
			<hz:cache name="canonicalItemStatusCountsCache" />
			<hz:cache name="canonicalPublicationStatusCountsCache" />
		</hz:config>
	</hz:hazelcast>

	<bean id="hazelcastCacheManager"
		  class="com.hazelcast.spring.cache.HazelcastCacheManager">
		<constructor-arg ref="hazelcastInstance" />
	</bean>

	<bean id="cacheManager"
		  class="org.springframework.cache.ehcache.EhCacheCacheManager"
		  p:cacheManager-ref="ehcache" />

	<bean id="ehcache" class="org.springframework.cache.ehcache.EhCacheManagerFactoryBean"
		  p:configLocation="classpath:${datahub.caching.configuration.resource.name}"
		  p:shared="true" />

	<alias name="defaultCanonicalTypeCache" alias="canonicalTypeCache" />
	<bean id="defaultCanonicalTypeCache" class="com.hybris.datahub.cache.impl.DefaultItemTypeCache" />

	<alias name="defaultTargetTypeCache" alias="targetTypeCache" />
	<bean id="defaultTargetTypeCache" class="com.hybris.datahub.cache.impl.DefaultItemTypeCache" />


	<bean class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="staticMethod" value="com.hybris.datahub.model.CanonicalItem.setItemTypeCache" />
		<property name="arguments">
			<list>
				<ref bean="canonicalTypeCache" />
			</list>
		</property>
	</bean>

	<bean class="org.springframework.beans.factory.config.MethodInvokingFactoryBean">
		<property name="staticMethod" value="com.hybris.datahub.model.TargetItem.setItemTypeCache" />
		<property name="arguments">
			<list>
				<ref bean="targetTypeCache" />
			</list>
		</property>
	</bean>


	<!-- Spring method level caches -->
	<bean id="canonicalPrimaryKeyAttributesByTypeCache" class="org.springframework.cache.ehcache.EhCacheFactoryBean">
		<property name="statisticsEnabled" value="true" />
		<property name="maxEntriesLocalHeap"
				  value="${datahub.caching.canonicalPrimaryKeyAttributesByTypeCache.maxEntriesLocalHeap}" />
		<property name="timeToIdleSeconds"
				  value="${datahub.caching.canonicalPrimaryKeyAttributesByTypeCache.timeToIdleSeconds}" />
		<property name="timeToLiveSeconds"
				  value="${datahub.caching.canonicalPrimaryKeyAttributesByTypeCache.timeToLiveSeconds}" />
	</bean>

	<alias name="defaultSpelResolveCache" alias="spelResolveCache" />
	<bean id="defaultSpelResolveCache" class="org.springframework.cache.ehcache.EhCacheFactoryBean">
		<property name="statisticsEnabled" value="true" />
		<property name="cacheName" value="spelResolveCache" />
		<property name="maxEntriesLocalHeap"
				  value="${datahub.caching.spelResolveCache.maxEntriesLocalHeap}" />
		<property name="timeToIdleSeconds"
				  value="${datahub.caching.spelResolveCache.timeToIdleSeconds}" />
		<property name="timeToLiveSeconds"
				  value="${datahub.caching.spelResolveCache.timeToLiveSeconds}" />
		<property name="memoryStoreEvictionPolicy"
				  value="${datahub.caching.spelResolveCache.evictionPolicy}" />
	</bean>

</beans>