--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- -----------------------------------------------------------------------
-- DataHubVersion
-- -----------------------------------------------------------------------

DROP TABLE "DataHubVersion" IF EXISTS;

-- -----------------------------------------------------------------------
-- hybris_sequences
-- -----------------------------------------------------------------------

DROP TABLE "hybris_sequences" IF EXISTS;

-- -----------------------------------------------------------------------
-- ManagedTarItem_expCodeAttrMap
-- -----------------------------------------------------------------------

DROP TABLE "ManagedTarItem_expCodeAttrMap" IF EXISTS;

-- -----------------------------------------------------------------------
-- DataHubFeed_Pool
-- -----------------------------------------------------------------------

DROP TABLE "DataHubFeed_Pool" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanonicalItem_RawItem
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItem_RawItem" IF EXISTS;

-- -----------------------------------------------------------------------
-- TargetItemMetadata_dependsOn
-- -----------------------------------------------------------------------

DROP TABLE "TargetItemMetadata_dependsOn" IF EXISTS;

-- -----------------------------------------------------------------------
-- TargetSysExportCode
-- -----------------------------------------------------------------------

DROP TABLE "TargetSysExportCode" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanonicalPubStatusCount
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalPubStatusCount" IF EXISTS;

-- -----------------------------------------------------------------------
-- PublicationRetry
-- -----------------------------------------------------------------------

DROP TABLE "PublicationRetry" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanItemPubStatus
-- -----------------------------------------------------------------------

DROP TABLE "CanItemPubStatus" IF EXISTS;

-- -----------------------------------------------------------------------
-- CompositionAction
-- -----------------------------------------------------------------------

DROP TABLE "CompositionAction" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanonicalItemMeta
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItemMeta" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanonicalItemStatusCount
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItemStatusCount" IF EXISTS;

-- -----------------------------------------------------------------------
-- TargetItem
-- -----------------------------------------------------------------------

DROP TABLE "TargetItem" IF EXISTS;

-- -----------------------------------------------------------------------
-- RawAttrModDef
-- -----------------------------------------------------------------------

DROP TABLE "RawAttrModDef" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanonicalAttrModDef
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalAttrModDef" IF EXISTS;

-- -----------------------------------------------------------------------
-- DataHubPool
-- -----------------------------------------------------------------------

DROP TABLE "DataHubPool" IF EXISTS;

-- -----------------------------------------------------------------------
-- DataHubFeed
-- -----------------------------------------------------------------------

DROP TABLE "DataHubFeed" IF EXISTS;

-- -----------------------------------------------------------------------
-- TargetAttrDef
-- -----------------------------------------------------------------------

DROP TABLE "TargetAttrDef" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanonicalItem
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItem" IF EXISTS;

-- -----------------------------------------------------------------------
-- TargetSystem
-- -----------------------------------------------------------------------

DROP TABLE "TargetSystem" IF EXISTS;

-- -----------------------------------------------------------------------
-- RawItem
-- -----------------------------------------------------------------------

DROP TABLE "RawItem" IF EXISTS;

-- -----------------------------------------------------------------------
-- DataLoadingAction
-- -----------------------------------------------------------------------

DROP TABLE "DataLoadingAction" IF EXISTS;

-- -----------------------------------------------------------------------
-- RawItemMeta
-- -----------------------------------------------------------------------

DROP TABLE "RawItemMeta" IF EXISTS;

-- -----------------------------------------------------------------------
-- RawItemStatusCount
-- -----------------------------------------------------------------------

DROP TABLE "RawItemStatusCount" IF EXISTS;

-- -----------------------------------------------------------------------
-- PublicationError
-- -----------------------------------------------------------------------

DROP TABLE "PublicationError" IF EXISTS;

-- -----------------------------------------------------------------------
-- PublicationAction
-- -----------------------------------------------------------------------

DROP TABLE "PublicationAction" IF EXISTS;

-- -----------------------------------------------------------------------
-- TargetSystemPub
-- -----------------------------------------------------------------------

DROP TABLE "TargetSystemPub" IF EXISTS;

-- -----------------------------------------------------------------------
-- TargetItemMeta
-- -----------------------------------------------------------------------

DROP TABLE "TargetItemMeta" IF EXISTS;

-- -----------------------------------------------------------------------
-- CanonicalAttrDef
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalAttrDef" IF EXISTS;
