--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- -----------------------------------------------------------------------
-- DataHubVersion
-- -----------------------------------------------------------------------

DROP TABLE "DataHubVersion" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- hybris_sequences
-- -----------------------------------------------------------------------

DROP TABLE "hybris_sequences" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- ManagedTarItem_expCodeAttrMap
-- -----------------------------------------------------------------------

DROP TABLE "ManagedTarItem_expCodeAttrMap" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- DataHubFeed_Pool
-- -----------------------------------------------------------------------

DROP TABLE "DataHubFeed_Pool" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- CanonicalItem_RawItem
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItem_RawItem" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- TargetItemMetadata_dependsOn
-- -----------------------------------------------------------------------

DROP TABLE "TargetItemMetadata_dependsOn" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- TargetSysExportCode
-- -----------------------------------------------------------------------

DROP TABLE "TargetSysExportCode" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- CanonicalPubStatusCount
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalPubStatusCount" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- PublicationRetry
-- -----------------------------------------------------------------------

DROP TABLE "PublicationRetry" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- CanItemPubStatus
-- -----------------------------------------------------------------------

DROP TABLE "CanItemPubStatus" CASCADE CONSTRAINTS;


-- -----------------------------------------------------------------------
-- CompositionAction
-- -----------------------------------------------------------------------

DROP TABLE "CompositionAction" CASCADE CONSTRAINTS;


-- -----------------------------------------------------------------------
-- CanonicalItemMeta
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItemMeta" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- CanonicalItemStatusCount
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItemStatusCount" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- TargetItem
-- -----------------------------------------------------------------------

DROP TABLE "TargetItem" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- YSchemalessAttribute
-- -----------------------------------------------------------------------


DROP TABLE "RawAttrModDef" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- CanonicalAttrModDef
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalAttrModDef" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- DataHubPool
-- -----------------------------------------------------------------------

DROP TABLE "DataHubPool" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- DataHubFeed
-- -----------------------------------------------------------------------

DROP TABLE "DataHubFeed" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- TargetAttrDef
-- -----------------------------------------------------------------------

DROP TABLE "TargetAttrDef" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- CanonicalItem
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalItem" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- TargetSystem
-- -----------------------------------------------------------------------

DROP TABLE "TargetSystem" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- RawItem
-- -----------------------------------------------------------------------

DROP TABLE "RawItem" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- DataLoadingAction
-- -----------------------------------------------------------------------

DROP TABLE "DataLoadingAction" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- RawItemStatusCount
-- -----------------------------------------------------------------------

DROP TABLE "RawItemStatusCount" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- RawItemMeta
-- -----------------------------------------------------------------------

DROP TABLE "RawItemMeta" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- PublicationError
-- -----------------------------------------------------------------------

DROP TABLE "PublicationError" CASCADE CONSTRAINTS;


-- -----------------------------------------------------------------------
-- PublicationAction
-- -----------------------------------------------------------------------

DROP TABLE "PublicationAction" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- TargetSystemPub
-- -----------------------------------------------------------------------

DROP TABLE "TargetSystemPub" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- TargetItemMeta
-- -----------------------------------------------------------------------

DROP TABLE "TargetItemMeta" CASCADE CONSTRAINTS;

-- -----------------------------------------------------------------------
-- CanonicalAttrDef
-- -----------------------------------------------------------------------

DROP TABLE "CanonicalAttrDef" CASCADE CONSTRAINTS;