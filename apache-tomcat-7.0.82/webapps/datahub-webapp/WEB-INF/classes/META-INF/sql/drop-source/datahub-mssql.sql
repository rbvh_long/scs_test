--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- -----------------------------------------------------------------------
-- DataHubVersion
-- -----------------------------------------------------------------------

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'DataHubVersion')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7ffc12 nvarchar(256), @cn_7625b5a_15766d03c65__7ffb12 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'DataHubVersion'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7ffc12, @cn_7625b5a_15766d03c65__7ffb12
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7ffc12+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7ffb12)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7ffc12, @cn_7625b5a_15766d03c65__7ffb12
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "DataHubVersion"
END;

-- -----------------------------------------------------------------------
-- hybris_sequences 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'hybris_sequences')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7ffc nvarchar(256), @cn_7625b5a_15766d03c65__7ffb nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'hybris_sequences'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7ffc, @cn_7625b5a_15766d03c65__7ffb
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7ffc+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7ffb)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7ffc, @cn_7625b5a_15766d03c65__7ffb
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "hybris_sequences"
END;

-- ----------------------------------------------------------------------- 
-- ManagedTarItem_expCodeAttrMap 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'ManagedTarItem_expCodeAttrMap')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7ff0 nvarchar(256), @cn_7625b5a_15766d03c65__7fef nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'ManagedTarItem_expCodeAttrMap'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7ff0, @cn_7625b5a_15766d03c65__7fef
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7ff0+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fef)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7ff0, @cn_7625b5a_15766d03c65__7fef
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "ManagedTarItem_expCodeAttrMap"
END;

-- ----------------------------------------------------------------------- 
-- DataHubFeed_Pool 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'DataHubFeed_Pool')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fee nvarchar(256), @cn_7625b5a_15766d03c65__7fed nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'DataHubFeed_Pool'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fee, @cn_7625b5a_15766d03c65__7fed
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fee+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fed)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fee, @cn_7625b5a_15766d03c65__7fed
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "DataHubFeed_Pool"
END;

-- ----------------------------------------------------------------------- 
-- CanonicalItem_RawItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanonicalItem_RawItem')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fec nvarchar(256), @cn_7625b5a_15766d03c65__7feb nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanonicalItem_RawItem'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fec, @cn_7625b5a_15766d03c65__7feb
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fec+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7feb)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fec, @cn_7625b5a_15766d03c65__7feb
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanonicalItem_RawItem"
END;

-- ----------------------------------------------------------------------- 
-- TargetItemMetadata_dependsOn 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'TargetItemMetadata_dependsOn')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fea nvarchar(256), @cn_7625b5a_15766d03c65__7fe9 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'TargetItemMetadata_dependsOn'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fea, @cn_7625b5a_15766d03c65__7fe9
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fea+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fe9)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fea, @cn_7625b5a_15766d03c65__7fe9
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "TargetItemMetadata_dependsOn"
END;

-- ----------------------------------------------------------------------- 
-- TargetSysExportCode 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'TargetSysExportCode')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fe8 nvarchar(256), @cn_7625b5a_15766d03c65__7fe7 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'TargetSysExportCode'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe8, @cn_7625b5a_15766d03c65__7fe7
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fe8+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fe7)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe8, @cn_7625b5a_15766d03c65__7fe7
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "TargetSysExportCode"
END;

-- ----------------------------------------------------------------------- 
-- CanonicalPubStatusCount 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanonicalPubStatusCount')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fe6 nvarchar(256), @cn_7625b5a_15766d03c65__7fe5 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanonicalPubStatusCount'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe6, @cn_7625b5a_15766d03c65__7fe5
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fe6+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fe5)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe6, @cn_7625b5a_15766d03c65__7fe5
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanonicalPubStatusCount"
END;

-- ----------------------------------------------------------------------- 
-- PublicationRetry 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'PublicationRetry')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fe4 nvarchar(256), @cn_7625b5a_15766d03c65__7fe3 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'PublicationRetry'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe4, @cn_7625b5a_15766d03c65__7fe3
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fe4+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fe3)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe4, @cn_7625b5a_15766d03c65__7fe3
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "PublicationRetry"
END;

-- ----------------------------------------------------------------------- 
-- CanItemPubStatus 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanItemPubStatus')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fe2 nvarchar(256), @cn_7625b5a_15766d03c65__7fe1 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanItemPubStatus'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe2, @cn_7625b5a_15766d03c65__7fe1
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fe2+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fe1)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fe2, @cn_7625b5a_15766d03c65__7fe1
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanItemPubStatus"
END;

-- ----------------------------------------------------------------------- 
-- CompositionAction 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CompositionAction')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fde nvarchar(256), @cn_7625b5a_15766d03c65__7fdd nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CompositionAction'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fde, @cn_7625b5a_15766d03c65__7fdd
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fde+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fdd)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fde, @cn_7625b5a_15766d03c65__7fdd
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CompositionAction"
END;

-- ----------------------------------------------------------------------- 
-- CanonicalItemMeta 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanonicalItemMeta')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fda nvarchar(256), @cn_7625b5a_15766d03c65__7fd9 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanonicalItemMeta'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fda, @cn_7625b5a_15766d03c65__7fd9
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fda+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fd9)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fda, @cn_7625b5a_15766d03c65__7fd9
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanonicalItemMeta"
END;

-- ----------------------------------------------------------------------- 
-- CanonicalItemStatusCount 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanonicalItemStatusCount')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fd8 nvarchar(256), @cn_7625b5a_15766d03c65__7fd7 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanonicalItemStatusCount'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd8, @cn_7625b5a_15766d03c65__7fd7
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fd8+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fd7)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd8, @cn_7625b5a_15766d03c65__7fd7
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanonicalItemStatusCount"
END;

-- ----------------------------------------------------------------------- 
-- TargetItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'TargetItem')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fd6 nvarchar(256), @cn_7625b5a_15766d03c65__7fd5 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'TargetItem'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd6, @cn_7625b5a_15766d03c65__7fd5
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fd6+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fd5)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd6, @cn_7625b5a_15766d03c65__7fd5
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "TargetItem"
END;

-- ----------------------------------------------------------------------- 
-- RawAttrModDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'RawAttrModDef')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fd2 nvarchar(256), @cn_7625b5a_15766d03c65__7fd1 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'RawAttrModDef'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd2, @cn_7625b5a_15766d03c65__7fd1
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fd2+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fd1)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd2, @cn_7625b5a_15766d03c65__7fd1
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "RawAttrModDef"
END;

-- ----------------------------------------------------------------------- 
-- CanonicalAttrModDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanonicalAttrModDef')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fd0 nvarchar(256), @cn_7625b5a_15766d03c65__7fcf nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanonicalAttrModDef'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd0, @cn_7625b5a_15766d03c65__7fcf
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fd0+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fcf)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fd0, @cn_7625b5a_15766d03c65__7fcf
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanonicalAttrModDef"
END;

-- ----------------------------------------------------------------------- 
-- DataHubPool 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'DataHubPool')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fce nvarchar(256), @cn_7625b5a_15766d03c65__7fcd nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'DataHubPool'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fce, @cn_7625b5a_15766d03c65__7fcd
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fce+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fcd)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fce, @cn_7625b5a_15766d03c65__7fcd
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "DataHubPool"
END;

-- ----------------------------------------------------------------------- 
-- DataHubFeed 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'DataHubFeed')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fcc nvarchar(256), @cn_7625b5a_15766d03c65__7fcb nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'DataHubFeed'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fcc, @cn_7625b5a_15766d03c65__7fcb
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fcc+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fcb)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fcc, @cn_7625b5a_15766d03c65__7fcb
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "DataHubFeed"
END;

-- ----------------------------------------------------------------------- 
-- TargetAttrDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'TargetAttrDef')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fca nvarchar(256), @cn_7625b5a_15766d03c65__7fc9 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'TargetAttrDef'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fca, @cn_7625b5a_15766d03c65__7fc9
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fca+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fc9)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fca, @cn_7625b5a_15766d03c65__7fc9
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "TargetAttrDef"
END;

-- ----------------------------------------------------------------------- 
-- CanonicalItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanonicalItem')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fc8 nvarchar(256), @cn_7625b5a_15766d03c65__7fc7 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanonicalItem'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc8, @cn_7625b5a_15766d03c65__7fc7
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fc8+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fc7)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc8, @cn_7625b5a_15766d03c65__7fc7
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanonicalItem"
END;

-- ----------------------------------------------------------------------- 
-- TargetSystem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'TargetSystem')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fc6 nvarchar(256), @cn_7625b5a_15766d03c65__7fc5 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'TargetSystem'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc6, @cn_7625b5a_15766d03c65__7fc5
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fc6+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fc5)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc6, @cn_7625b5a_15766d03c65__7fc5
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "TargetSystem"
END;

-- ----------------------------------------------------------------------- 
-- RawItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'RawItem')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fc4 nvarchar(256), @cn_7625b5a_15766d03c65__7fc3 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'RawItem'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc4, @cn_7625b5a_15766d03c65__7fc3
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fc4+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fc3)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc4, @cn_7625b5a_15766d03c65__7fc3
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "RawItem"
END;

-- ----------------------------------------------------------------------- 
-- DataLoadingAction 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'DataLoadingAction')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fc2 nvarchar(256), @cn_7625b5a_15766d03c65__7fc1 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'DataLoadingAction'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc2, @cn_7625b5a_15766d03c65__7fc1
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fc2+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fc1)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fc2, @cn_7625b5a_15766d03c65__7fc1
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "DataLoadingAction"
END;

-- ----------------------------------------------------------------------- 
-- RawItemStatusCount 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'RawItemStatusCount')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fbe nvarchar(256), @cn_7625b5a_15766d03c65__7fbd nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'RawItemStatusCount'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fbe, @cn_7625b5a_15766d03c65__7fbd
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fbe+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fbd)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fbe, @cn_7625b5a_15766d03c65__7fbd
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "RawItemStatusCount"
END;

-- ----------------------------------------------------------------------- 
-- RawItemMeta 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'RawItemMeta')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fbc nvarchar(256), @cn_7625b5a_15766d03c65__7fbb nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'RawItemMeta'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fbc, @cn_7625b5a_15766d03c65__7fbb
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fbc+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fbb)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fbc, @cn_7625b5a_15766d03c65__7fbb
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "RawItemMeta"
END;

-- ----------------------------------------------------------------------- 
-- PublicationError 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'PublicationError')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fba nvarchar(256), @cn_7625b5a_15766d03c65__7fb9 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'PublicationError'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fba, @cn_7625b5a_15766d03c65__7fb9
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fba+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fb9)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fba, @cn_7625b5a_15766d03c65__7fb9
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "PublicationError"
END;

-- ----------------------------------------------------------------------- 
-- PublicationAction 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'PublicationAction')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fb6 nvarchar(256), @cn_7625b5a_15766d03c65__7fb5 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'PublicationAction'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb6, @cn_7625b5a_15766d03c65__7fb5
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fb6+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fb5)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb6, @cn_7625b5a_15766d03c65__7fb5
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "PublicationAction"
END;

-- ----------------------------------------------------------------------- 
-- TargetSystemPub 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'TargetSystemPub')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fb4 nvarchar(256), @cn_7625b5a_15766d03c65__7fb3 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'TargetSystemPub'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb4, @cn_7625b5a_15766d03c65__7fb3
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fb4+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fb3)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb4, @cn_7625b5a_15766d03c65__7fb3
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "TargetSystemPub"
END;

-- ----------------------------------------------------------------------- 
-- TargetItemMeta 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'TargetItemMeta')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fb2 nvarchar(256), @cn_7625b5a_15766d03c65__7fb1 nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'TargetItemMeta'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb2, @cn_7625b5a_15766d03c65__7fb1
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fb2+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7fb1)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb2, @cn_7625b5a_15766d03c65__7fb1
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "TargetItemMeta"
END;

-- ----------------------------------------------------------------------- 
-- CanonicalAttrDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
IF EXISTS (SELECT 1 FROM sysobjects WHERE type = 'U' AND name = 'CanonicalAttrDef')
BEGIN
  DECLARE @tn_7625b5a_15766d03c65__7fb0 nvarchar(256), @cn_7625b5a_15766d03c65__7faf nvarchar(256)
  DECLARE refcursor CURSOR FOR
  SELECT object_name(objs.parent_obj) tablename, objs.name constraintname
    FROM sysobjects objs JOIN sysconstraints cons ON objs.id = cons.constid
    WHERE objs.xtype != 'PK' AND object_name(objs.parent_obj) = 'CanonicalAttrDef'  OPEN refcursor
  FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb0, @cn_7625b5a_15766d03c65__7faf
  WHILE @@FETCH_STATUS = 0
    BEGIN
      EXEC ('ALTER TABLE '+@tn_7625b5a_15766d03c65__7fb0+' DROP CONSTRAINT '+@cn_7625b5a_15766d03c65__7faf)
      FETCH NEXT FROM refcursor INTO @tn_7625b5a_15766d03c65__7fb0, @cn_7625b5a_15766d03c65__7faf
    END
  CLOSE refcursor
  DEALLOCATE refcursor
  DROP TABLE "CanonicalAttrDef"
END;

