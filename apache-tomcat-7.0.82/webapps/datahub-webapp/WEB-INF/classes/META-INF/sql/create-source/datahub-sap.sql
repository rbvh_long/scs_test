--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- -----------------------------------------------------------------------
-- CanonicalAttrDef 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanonicalAttrDef"
(
    "modifiedtime" TIMESTAMP,
    "canonicalattrmodeldef" NUMERIC(38,0),
    "referenceattribute" VARCHAR(4000),
    "active" INTEGER,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "rawitemtype" VARCHAR(255),
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalAttrDef_canonicalattrmodeldef" ON "CanonicalAttrDef" ("canonicalattrmodeldef");

CREATE INDEX "IDX_CanAttrDef_active_rawType" ON "CanonicalAttrDef" ("rawitemtype", "active");

CREATE INDEX "IDX_CanonicalAttrDef_rawItemType" ON "CanonicalAttrDef" ("rawitemtype");

-- ----------------------------------------------------------------------- 
-- TargetItemMeta 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "TargetItemMeta"
(
    "filterexpression" VARCHAR(4000),
    "filtereditempubstatus" VARCHAR(255),
    "targetsystem" NUMERIC(38,0),
    "description" VARCHAR(255),
    "version" NUMERIC(20,5),
    "canonicalitemsource" NUMERIC(38,0),
    "isexportcodeexpression" INTEGER,
    "modifiedtime" TIMESTAMP,
    "itemtype" VARCHAR(255) NOT NULL,
    "itemmetadataid" NUMERIC(20,5) NOT NULL,
    "isupdatable" INTEGER,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "exportcode" VARCHAR(255),
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetItemMeta_targetsystem" ON "TargetItemMeta" ("targetsystem");

CREATE INDEX "FK_TargetItemMeta_canonicalitemsource" ON "TargetItemMeta" ("canonicalitemsource");

CREATE UNIQUE INDEX "AK_TargetItemMeta" ON "TargetItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_TargetMeta_itemType" ON "TargetItemMeta" ("itemtype");

-- ----------------------------------------------------------------------- 
-- TargetSystemPub 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "TargetSystemPub"
(
    "canonicalitemcount" NUMERIC(20,5),
    "endtime" TIMESTAMP,
    "targetsystem" NUMERIC(38,0),
    "compositetargetsystempub" NUMERIC(38,0),
    "starttime" TIMESTAMP,
    "internalerrorcount" NUMERIC(20,5),
    "modifiedtime" TIMESTAMP,
    "publicationaction" NUMERIC(38,0),
    "subpuborder" INTEGER,
    "publicationtype" VARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "externalerrorcount" NUMERIC(20,5),
    "status" VARCHAR(255) NOT NULL,
    "ignoredcount" NUMERIC(20,5),
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetSystemPub_targetsystem" ON "TargetSystemPub" ("targetsystem");

CREATE INDEX "FK_TargetSystemPub_compositetargetsystempub" ON "TargetSystemPub" ("compositetargetsystempub");

CREATE INDEX "FK_TargetSystemPub_publicationaction" ON "TargetSystemPub" ("publicationaction");

-- ----------------------------------------------------------------------- 
-- PublicationAction 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "PublicationAction"
(
    "modifiedtime" TIMESTAMP,
    "pool" NUMERIC(38,0),
    "endtime" TIMESTAMP,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "starttime" TIMESTAMP,
    "status" VARCHAR(255) NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_PublicationAction_pool" ON "PublicationAction" ("pool");

-- -----------------------------------------------------------------------
-- PublicationError 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "PublicationError"
(
    "canonicalitempublicationstatus" NUMERIC(38,0),
    "code" VARCHAR(255) NOT NULL,
    "modifiedtime" TIMESTAMP,
    "targetsystempublication" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "message" NCLOB NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_PublicationError_canonicalitempublicationstatus" ON "PublicationError" ("canonicalitempublicationstatus");

CREATE INDEX "FK_PublicationError_targetsystempublication" ON "PublicationError" ("targetsystempublication");

-- ----------------------------------------------------------------------- 
-- RawItemStatusCount 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "RawItemStatusCount"
(
    "modifiedtime" TIMESTAMP,
    "pool" NUMERIC(38,0) NOT NULL,
    "pendingcount" NUMERIC(20,5) NOT NULL,
    "processedcount" NUMERIC(20,5) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "ignoredcount" NUMERIC(20,5) NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawItemStatusCount_pool" ON "RawItemStatusCount" ("pool");

-- CREATE INDEX "IDX_rawItemStatusCount_pool" ON "RawItemStatusCount" ("pool");

-- ----------------------------------------------------------------------- 
-- RawItemMeta 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "RawItemMeta"
(
    "modifiedtime" TIMESTAMP,
    "itemtype" VARCHAR(255) NOT NULL,
    "itemmetadataid" NUMERIC(20,5) NOT NULL,
    "description" VARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "version" NUMERIC(20,5),
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "AK_RawItemMeta" ON "RawItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_RawMeta_itemType" ON "RawItemMeta" ("itemtype");

-- -----------------------------------------------------------------------
-- DataLoadingAction 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "DataLoadingAction"
(
    "feed" NUMERIC(38,0),
    "modifiedtime" TIMESTAMP,
    "count" NUMERIC(20,5),
    "endtime" TIMESTAMP,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "starttime" TIMESTAMP,
    "message" VARCHAR(255),
    "status" VARCHAR(255) NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_DataLoadingAction_feed" ON "DataLoadingAction" ("feed");

CREATE INDEX "IDX_DataLoadingAction_endTime" ON "DataLoadingAction" ("endtime");

CREATE INDEX "IDX_DataLoadingAction_status" ON "DataLoadingAction" ("status");

-- ----------------------------------------------------------------------- 
-- RawItem 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "RawItem"
(
    "modifiedtime" TIMESTAMP,
    "itemtype" VARCHAR(255),
    "isocode" VARCHAR(255),
    "dataloadingaction" NUMERIC(38,0),
    "batchid" VARCHAR(255),
    "traceid" VARCHAR(36),
    "uuid" VARCHAR(36),
    "datapool" NUMERIC(38,0),
    "extensionsource" VARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "delete" INTEGER,
    "version" NUMERIC(20,5),
    "status" VARCHAR(255),
    "typecode" VARCHAR(128),
    "schemaless_attrs" BLOB,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawItem_dataloadingaction" ON "RawItem" ("dataloadingaction");

CREATE INDEX "FK_RawItem_datapool" ON "RawItem" ("datapool");

CREATE INDEX "IDX_ManagedRaw_itemType" ON "RawItem" ("itemtype");

CREATE INDEX "IDX_RawItem_status" ON "RawItem" ("status");

CREATE INDEX "IDX_RawItem_ActIdTypePoolStat" ON "RawItem" ("dataloadingaction", "id", "itemtype", "datapool", "status");

-- ----------------------------------------------------------------------- 
-- TargetSystem 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "TargetSystem"
(
    "targetsystemname" VARCHAR(255) NOT NULL,
    "password" VARCHAR(255),
    "modifiedtime" TIMESTAMP,
    "targetsystemtype" VARCHAR(255) NOT NULL,
    "exporturl" VARCHAR(255) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "username" VARCHAR(255),
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_TargetSystem_targetSystemName" ON "TargetSystem" ("targetsystemname");

-- ----------------------------------------------------------------------- 
-- CanonicalItem 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanonicalItem"
(
    "modifiedtime" TIMESTAMP,
    "itemtype" VARCHAR(255),
    "compositionaction" NUMERIC(38,0),
    "datapool" NUMERIC(38,0),
    "id" NUMERIC(38,0) NOT NULL,
    "batchid" VARCHAR(255),
    "traceid" VARCHAR(36),
    "uuid" VARCHAR(36),
    "documentid" VARCHAR(255),
    "creationtime" TIMESTAMP,
    "compositionstatusdetail" VARCHAR(255),
    "version" NUMERIC(20,5),
    "integrationkey" VARCHAR(255),
    "status" VARCHAR(255) NOT NULL,
    "typecode" VARCHAR(128),
    "schemaless_attrs" BLOB,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalItem_compositionaction" ON "CanonicalItem" ("compositionaction");

CREATE INDEX "FK_CanonicalItem_datapool" ON "CanonicalItem" ("datapool");

CREATE INDEX "IDX_CanonicalItem_status" ON "CanonicalItem" ("status");

CREATE INDEX "IDX_ManagedCanonical_itemType" ON "CanonicalItem" ("itemtype");

CREATE INDEX "IDX_id_status" ON "CanonicalItem" ("id", "status");

CREATE INDEX "IDX_integrationKey_status" ON "CanonicalItem" ("integrationkey", "status");

CREATE INDEX "IDX_CanonIte_KeyTypePoolStatId" ON "CanonicalItem" ("integrationkey", "itemtype", "datapool", "status", "id");

-- ----------------------------------------------------------------------- 
-- TargetAttrDef 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "TargetAttrDef"
(
    "targetitemmetadata" NUMERIC(38,0),
    "active" INTEGER,
    "attributename" VARCHAR(4000) NOT NULL,
    "collection" INTEGER,
    "transformationexpression" VARCHAR(4000),
    "mandatoryinheader" INTEGER,
    "modifiedtime" TIMESTAMP,
    "localizable" INTEGER,
    "exportcodeexpression" INTEGER,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "secured" INTEGER NOT NULL,
    "exportcode" VARCHAR(255),
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetAttrDef_targetitemmetadata" ON "TargetAttrDef" ("targetitemmetadata");

-- ----------------------------------------------------------------------- 
-- DataHubFeed 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "DataHubFeed"
(
    "defaultcompositionstrategy" VARCHAR(255),
    "modifiedtime" TIMESTAMP,
    "poolingstrategy" VARCHAR(255),
    "name" VARCHAR(255) NOT NULL,
    "description" VARCHAR(255),
    "poolingcondition" VARCHAR(255),
    "defaultpublicationstrategy" VARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_DataHubFeed_name" ON "DataHubFeed" ("name");

-- ----------------------------------------------------------------------- 
-- DataHubPool 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "DataHubPool"
(
    "poolname" VARCHAR(255) NOT NULL,
    "publicationstrategy" VARCHAR(255),
    "modifiedtime" TIMESTAMP,
    "compositionstrategy" VARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_DataHubPool_poolName" ON "DataHubPool" ("poolname");

-- ----------------------------------------------------------------------- 
-- CanonicalAttrModDef 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanonicalAttrModDef"
(
    "canonicalitemmetadata" NUMERIC(38,0),
    "modifiedtime" TIMESTAMP,
    "localizable" INTEGER,
    "attributename" VARCHAR(4000) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "collection" INTEGER,
    "secured" INTEGER NOT NULL,
    "primarykey" INTEGER,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalAttrModDef_canonicalitemmetadata" ON "CanonicalAttrModDef" ("canonicalitemmetadata");

CREATE INDEX "IDX_CanAttrMod_PK" ON "CanonicalAttrModDef" ("primarykey");

-- ----------------------------------------------------------------------- 
-- RawAttrModDef 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "RawAttrModDef"
(
    "modifiedtime" TIMESTAMP,
    "rawitemmetadata" NUMERIC(38,0),
    "localizable" INTEGER,
    "attributename" VARCHAR(4000) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "collection" INTEGER,
    "secured" INTEGER NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawAttrModDef_rawitemmetadata" ON "RawAttrModDef" ("rawitemmetadata");

-- -----------------------------------------------------------------------
-- TargetItem 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "TargetItem"
(
    "modifiedtime" TIMESTAMP,
    "itemtype" VARCHAR(255),
    "targetsystempublication" NUMERIC(38,0),
    "datapool" NUMERIC(38,0),
    "canonicalitem" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "version" NUMERIC(20,5),
    "exportcode" VARCHAR(255),
    "typecode" VARCHAR(128),
    "schemaless_attrs" BLOB,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetItem_targetsystempublication" ON "TargetItem" ("targetsystempublication");

CREATE INDEX "FK_TargetItem_datapool" ON "TargetItem" ("datapool");

CREATE INDEX "FK_TargetItem_canonicalitem" ON "TargetItem" ("canonicalitem");

CREATE INDEX "IDX_ManagedTarget_itemType" ON "TargetItem" ("itemtype");

CREATE INDEX "IDX_TarItemIdTypePublication" ON "TargetItem" ("id", "itemtype", "targetsystempublication");

-- ----------------------------------------------------------------------- 
-- CanonicalItemStatusCount 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanonicalItemStatusCount"
(
    "deletedcount" NUMERIC(20,5) NOT NULL,
    "modifiedtime" TIMESTAMP,
    "successcount" NUMERIC(20,5) NOT NULL,
    "pool" NUMERIC(38,0) NOT NULL,
    "archivedcount" NUMERIC(20,5) NOT NULL,
    "errorcount" NUMERIC(20,5) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalItemStatusCount_pool" ON "CanonicalItemStatusCount" ("pool");

-- CREATE INDEX "IDX_canItmStsCount_pool" ON "CanonicalItemStatusCount" ("pool");

-- ----------------------------------------------------------------------- 
-- CanonicalItemMeta 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanonicalItemMeta"
(
    "id" NUMERIC(38,0) NOT NULL,
    "itemmetadataid" NUMERIC(20,5) NOT NULL,
    "itemtype" VARCHAR(255) NOT NULL,
    "typecode" VARCHAR(128),
    "description" VARCHAR(255),
    "documentid" VARCHAR(4000),
    "creationtime" TIMESTAMP,
    "modifiedtime" TIMESTAMP,
    "version" NUMERIC(20,5),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "AK_CanonicalItemMeta" ON "CanonicalItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_CanonicalMeta_itemType" ON "CanonicalItemMeta" ("itemtype");

-- -----------------------------------------------------------------------
-- CompositionAction 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CompositionAction"
(
    "modifiedtime" TIMESTAMP,
    "count" NUMERIC(20,5),
    "pool" NUMERIC(38,0),
    "endtime" TIMESTAMP,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "starttime" TIMESTAMP,
    "status" VARCHAR(255) NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CompositionAction_pool" ON "CompositionAction" ("pool");

CREATE INDEX "IDX_CompAction_status" ON "CompositionAction" ("status");

-- -----------------------------------------------------------------------
-- CanItemPubStatus 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanItemPubStatus"
(
    "modifiedtime" TIMESTAMP,
    "statusdetail" VARCHAR(255),
    "targetsystempublication" NUMERIC(38,0) NOT NULL,
    "canonicalitem" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "status" VARCHAR(255) NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanItemPubStatus_targetsystempublication" ON "CanItemPubStatus" ("targetsystempublication");

CREATE INDEX "FK_CanItemPubStatus_canonicalitem" ON "CanItemPubStatus" ("canonicalitem");

CREATE INDEX "IDX_CanItemPubStatus_status" ON "CanItemPubStatus" ("status");

-- ----------------------------------------------------------------------- 
-- PublicationRetry 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "PublicationRetry"
(
    "targetsystemid" NUMERIC(20,5) NOT NULL,
    "retrycount" INTEGER NOT NULL,
    "modifiedtime" TIMESTAMP,
    "canonicalitemid" NUMERIC(20,5) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "IDX_pubRetry_canonicalItemId" ON "PublicationRetry" ("canonicalitemid");

CREATE INDEX "IDX_pubRetry_targetSystemId" ON "PublicationRetry" ("targetsystemid");

-- ----------------------------------------------------------------------- 
-- CanonicalPubStatusCount 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanonicalPubStatusCount"
(
    "internalerrorcount" NUMERIC(20,5) NOT NULL,
    "modifiedtime" TIMESTAMP,
    "successcount" NUMERIC(20,5) NOT NULL,
    "pool" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" TIMESTAMP,
    "externalerrorcount" NUMERIC(20,5) NOT NULL,
    "ignoredcount" NUMERIC(20,5) NOT NULL,
    "typecode" VARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalPubStatusCount_pool" ON "CanonicalPubStatusCount" ("pool");

-- CREATE INDEX "IDX_canPubStsCount_pool" ON "CanonicalPubStatusCount" ("pool");

-- ----------------------------------------------------------------------- 
-- TargetSysExportCode 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "TargetSysExportCode"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" VARCHAR(255)
);

CREATE INDEX "IDX_TargetSysExportCode_destid" ON "TargetSysExportCode" ("destid");

CREATE INDEX "IDX_TargetSysExportCode_srcid" ON "TargetSysExportCode" ("srcid");

-- ----------------------------------------------------------------------- 
-- TargetItemMetadata_dependsOn 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "TargetItemMetadata_dependsOn"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" NUMERIC(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_TargetItemMetadata_dependsOn_" ON "TargetItemMetadata_dependsOn" ("srcid", "destid");

CREATE INDEX "IDX_TargetItemMetadata_dependsOn_srcid" ON "TargetItemMetadata_dependsOn" ("srcid");

-- ----------------------------------------------------------------------- 
-- CanonicalItem_RawItem 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "CanonicalItem_RawItem"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" NUMERIC(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_CanonicalItem_RawItem_" ON "CanonicalItem_RawItem" ("srcid", "destid");

CREATE INDEX "IDX_CanonicalItem_RawItem_srcid" ON "CanonicalItem_RawItem" ("srcid");

-- ----------------------------------------------------------------------- 
-- DataHubFeed_Pool 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "DataHubFeed_Pool"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" NUMERIC(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_DataHubFeed_Pool_" ON "DataHubFeed_Pool" ("srcid", "destid");

CREATE INDEX "IDX_DataHubFeed_Pool_srcid" ON "DataHubFeed_Pool" ("srcid");

-- ----------------------------------------------------------------------- 
-- ManagedTarItem_expCodeAttrMap 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "ManagedTarItem_expCodeAttrMap"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "key" VARCHAR(255) NOT NULL,
    "value" VARCHAR(255)
);

CREATE UNIQUE INDEX "PK_ManagedTarItem_expCodeAttrMap_" ON "ManagedTarItem_expCodeAttrMap" ("srcid", "key");

-- -----------------------------------------------------------------------
-- hybris_sequences 
-- ----------------------------------------------------------------------- 

CREATE COLUMN TABLE "hybris_sequences"
(
    "name" VARCHAR(200) NOT NULL,
    "value" NUMERIC(20,5),
    PRIMARY KEY ("name")
);

-- -----------------------------------------------------------------------
-- DataHubVersion
-- -----------------------------------------------------------------------

CREATE COLUMN TABLE "DataHubVersion"
(
    "version" VARCHAR(128) NOT NULL
);

INSERT INTO "DataHubVersion" values ('6.4.0');

