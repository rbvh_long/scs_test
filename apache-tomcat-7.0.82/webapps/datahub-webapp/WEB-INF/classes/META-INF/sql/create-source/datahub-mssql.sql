--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- -----------------------------------------------------------------------
-- CanonicalAttrDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanonicalAttrDef"
(
    "modifiedtime" DATETIME,
    "canonicalattrmodeldef" NUMERIC(38,0),
    "referenceattribute" NVARCHAR(4000),
    "active" INT,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "rawitemtype" NVARCHAR(255),
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalAttrDef_canonicalattrmodeldef" ON "CanonicalAttrDef" ("canonicalattrmodeldef");

CREATE INDEX "IDX_CanAttrDef_active_rawType" ON "CanonicalAttrDef" ("rawitemtype", "active");

CREATE INDEX "IDX_CanonicalAttrDef_rawItemType" ON "CanonicalAttrDef" ("rawitemtype");

-- ----------------------------------------------------------------------- 
-- TargetItemMeta 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "TargetItemMeta"
(
    "filterexpression" NVARCHAR(4000),
    "filtereditempubstatus" NVARCHAR(255),
    "targetsystem" NUMERIC(38,0),
    "description" NVARCHAR(255),
    "version" NUMERIC(20,5),
    "canonicalitemsource" NUMERIC(38,0),
    "isexportcodeexpression" INT,
    "modifiedtime" DATETIME,
    "itemtype" NVARCHAR(255) NOT NULL,
    "itemmetadataid" NUMERIC(20,5) NOT NULL,
    "isupdatable" INT,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "exportcode" NVARCHAR(255),
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetItemMeta_targetsystem" ON "TargetItemMeta" ("targetsystem");

CREATE INDEX "FK_TargetItemMeta_canonicalitemsource" ON "TargetItemMeta" ("canonicalitemsource");

CREATE UNIQUE INDEX "AK_TargetItemMeta" ON "TargetItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_TargetMeta_itemType" ON "TargetItemMeta" ("itemtype");

-- ----------------------------------------------------------------------- 
-- TargetSystemPub 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "TargetSystemPub"
(
    "canonicalitemcount" NUMERIC(20,5),
    "endtime" DATETIME,
    "targetsystem" NUMERIC(38,0),
    "compositetargetsystempub" NUMERIC(38,0),
    "starttime" DATETIME,
    "internalerrorcount" NUMERIC(20,5),
    "modifiedtime" DATETIME,
    "publicationaction" NUMERIC(38,0),
    "subpuborder" INT,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "publicationtype" NVARCHAR(255),
    "externalerrorcount" NUMERIC(20,5),
    "ignoredcount" NUMERIC(20,5),
    "status" NVARCHAR(255) NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetSystemPub_targetsystem" ON "TargetSystemPub" ("targetsystem");

CREATE INDEX "FK_TargetSystemPub_compositetargetsystempub" ON "TargetSystemPub" ("compositetargetsystempub");

CREATE INDEX "FK_TargetSystemPub_publicationaction" ON "TargetSystemPub" ("publicationaction");

-- ----------------------------------------------------------------------- 
-- PublicationAction 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "PublicationAction"
(
    "modifiedtime" DATETIME,
    "pool" NUMERIC(38,0),
    "endtime" DATETIME,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "starttime" DATETIME,
    "status" NVARCHAR(255) NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_PublicationAction_pool" ON "PublicationAction" ("pool");

-- ----------------------------------------------------------------------- 
-- PublicationError 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "PublicationError"
(
    "canonicalitempublicationstatus" NUMERIC(38,0),
    "code" NVARCHAR(255) NOT NULL,
    "modifiedtime" DATETIME,
    "targetsystempublication" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "message" TEXT NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_PublicationError_canonicalitempublicationstatus" ON "PublicationError" ("canonicalitempublicationstatus");

CREATE INDEX "FK_PublicationError_targetsystempublication" ON "PublicationError" ("targetsystempublication");

-- ----------------------------------------------------------------------- 
-- RawItemStatusCount 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "RawItemStatusCount"
(
    "modifiedtime" DATETIME,
    "pool" NUMERIC(38,0) NOT NULL,
    "pendingcount" NUMERIC(20,5) NOT NULL,
    "processedcount" NUMERIC(20,5) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "ignoredcount" NUMERIC(20,5) NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawItemStatusCount_pool" ON "RawItemStatusCount" ("pool");

CREATE INDEX "IDX_rawItemStatusCount_pool" ON "RawItemStatusCount" ("pool");

-- ----------------------------------------------------------------------- 
-- RawItemMeta 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "RawItemMeta"
(
    "modifiedtime" DATETIME,
    "itemtype" NVARCHAR(255) NOT NULL,
    "itemmetadataid" NUMERIC(20,5) NOT NULL,
    "description" NVARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "version" NUMERIC(20,5),
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "AK_RawItemMeta" ON "RawItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_RawMeta_itemType" ON "RawItemMeta" ("itemtype");

-- ----------------------------------------------------------------------- 
-- DataLoadingAction 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "DataLoadingAction"
(
    "feed" NUMERIC(38,0),
    "modifiedtime" DATETIME,
    "count" NUMERIC(20,5),
    "endtime" DATETIME,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "starttime" DATETIME,
    "message" NVARCHAR(255),
    "status" NVARCHAR(255) NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_DataLoadingAction_feed" ON "DataLoadingAction" ("feed");

CREATE INDEX "IDX_DataLoadingAction_endTime" ON "DataLoadingAction" ("endtime");

CREATE INDEX "IDX_DataLoadingAction_status" ON "DataLoadingAction" ("status");

-- ----------------------------------------------------------------------- 
-- RawItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "RawItem"
(
    "modifiedtime" DATETIME,
    "itemtype" NVARCHAR(255),
    "isocode" NVARCHAR(255),
    "dataloadingaction" NUMERIC(38,0),
    "batchid" NVARCHAR(255),
    "traceid" NVARCHAR(36),
    "uuid" NVARCHAR(36),
    "datapool" NUMERIC(38,0),
    "extensionsource" NVARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "delete" INT,
    "version" NUMERIC(20,5),
    "status" NVARCHAR(255),
    "typecode" NVARCHAR(128),
    "schemaless_attrs" IMAGE,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawItem_dataloadingaction" ON "RawItem" ("dataloadingaction");

CREATE INDEX "FK_RawItem_datapool" ON "RawItem" ("datapool");

CREATE INDEX "IDX_ManagedRaw_itemType" ON "RawItem" ("itemtype");

CREATE INDEX "IDX_RawItem_status" ON "RawItem" ("status");

CREATE INDEX "IDX_RawItem_ActIdTypePoolStat" ON "RawItem" ("dataloadingaction", "id", "itemtype", "datapool", "status");

-- ----------------------------------------------------------------------- 
-- TargetSystem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "TargetSystem"
(
    "targetsystemname" NVARCHAR(255) NOT NULL,
    "password" NVARCHAR(255),
    "modifiedtime" DATETIME,
    "targetsystemtype" NVARCHAR(255) NOT NULL,
    "exporturl" NVARCHAR(255) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "username" NVARCHAR(255),
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_TargetSystem_targetSystemName" ON "TargetSystem" ("targetsystemname");

-- ----------------------------------------------------------------------- 
-- CanonicalItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanonicalItem"
(
    "modifiedtime" DATETIME,
    "itemtype" NVARCHAR(255),
    "compositionaction" NUMERIC(38,0),
    "datapool" NUMERIC(38,0),
    "id" NUMERIC(38,0) NOT NULL,
    "batchid" NVARCHAR(255),
    "traceid" NVARCHAR(36),
    "uuid" NVARCHAR(36),
    "documentid" NVARCHAR(255),
    "creationtime" DATETIME,
    "compositionstatusdetail" NVARCHAR(255),
    "version" NUMERIC(20,5),
    "integrationkey" NVARCHAR(255),
    "status" NVARCHAR(255) NOT NULL,
    "typecode" NVARCHAR(128),
    "schemaless_attrs" IMAGE,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalItem_compositionaction" ON "CanonicalItem" ("compositionaction");

CREATE INDEX "FK_CanonicalItem_datapool" ON "CanonicalItem" ("datapool");

CREATE INDEX "IDX_CanonicalItem_status" ON "CanonicalItem" ("status");

CREATE INDEX "IDX_ManagedCanonical_itemType" ON "CanonicalItem" ("itemtype");

CREATE INDEX "IDX_id_status" ON "CanonicalItem" ("id", "status");

CREATE INDEX "IDX_integrationKey_status" ON "CanonicalItem" ("integrationkey", "status");

CREATE INDEX "IDX_CanonIte_KeyTypePoolStatId" ON "CanonicalItem" ("integrationkey", "itemtype", "datapool", "status", "id");

-- ----------------------------------------------------------------------- 
-- TargetAttrDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "TargetAttrDef"
(
    "targetitemmetadata" NUMERIC(38,0),
    "active" INT,
    "attributename" NVARCHAR(4000) NOT NULL,
    "collection" INT,
    "transformationexpression" NVARCHAR(4000),
    "mandatoryinheader" INT,
    "modifiedtime" DATETIME,
    "localizable" INT,
    "exportcodeexpression" INT,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "secured" INT NOT NULL,
    "exportcode" NVARCHAR(255),
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetAttrDef_targetitemmetadata" ON "TargetAttrDef" ("targetitemmetadata");

-- ----------------------------------------------------------------------- 
-- DataHubFeed 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "DataHubFeed"
(
    "defaultcompositionstrategy" NVARCHAR(255),
    "modifiedtime" DATETIME,
    "poolingstrategy" NVARCHAR(255),
    "name" NVARCHAR(255) NOT NULL,
    "description" NVARCHAR(255),
    "poolingcondition" NVARCHAR(255),
    "defaultpublicationstrategy" NVARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_DataHubFeed_name" ON "DataHubFeed" ("name");

-- ----------------------------------------------------------------------- 
-- DataHubPool 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "DataHubPool"
(
    "poolname" NVARCHAR(255) NOT NULL,
    "publicationstrategy" NVARCHAR(255),
    "modifiedtime" DATETIME,
    "compositionstrategy" NVARCHAR(255),
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_DataHubPool_poolName" ON "DataHubPool" ("poolname");

-- ----------------------------------------------------------------------- 
-- CanonicalAttrModDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanonicalAttrModDef"
(
    "canonicalitemmetadata" NUMERIC(38,0),
    "modifiedtime" DATETIME,
    "localizable" INT,
    "attributename" NVARCHAR(4000) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "collection" INT,
    "secured" INT NOT NULL,
    "primarykey" INT,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalAttrModDef_canonicalitemmetadata" ON "CanonicalAttrModDef" ("canonicalitemmetadata");

CREATE INDEX "IDX_CanAttrMod_PK" ON "CanonicalAttrModDef" ("primarykey");

-- ----------------------------------------------------------------------- 
-- RawAttrModDef 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "RawAttrModDef"
(
    "modifiedtime" DATETIME,
    "rawitemmetadata" NUMERIC(38,0),
    "localizable" INT,
    "attributename" NVARCHAR(4000) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "collection" INT,
    "secured" INT NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawAttrModDef_rawitemmetadata" ON "RawAttrModDef" ("rawitemmetadata");

-- ----------------------------------------------------------------------- 
-- TargetItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "TargetItem"
(
    "modifiedtime" DATETIME,
    "itemtype" NVARCHAR(255),
    "targetsystempublication" NUMERIC(38,0),
    "datapool" NUMERIC(38,0),
    "canonicalitem" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "version" NUMERIC(20,5),
    "exportcode" NVARCHAR(255),
    "typecode" NVARCHAR(128),
    "schemaless_attrs" IMAGE,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetItem_targetsystempublication" ON "TargetItem" ("targetsystempublication");

CREATE INDEX "FK_TargetItem_datapool" ON "TargetItem" ("datapool");

CREATE INDEX "FK_TargetItem_canonicalitem" ON "TargetItem" ("canonicalitem");

CREATE INDEX "IDX_ManagedTarget_itemType" ON "TargetItem" ("itemtype");

CREATE INDEX "IDX_TarItemIdTypePublication" ON "TargetItem" ("id", "itemtype", "targetsystempublication");

-- ----------------------------------------------------------------------- 
-- CanonicalItemStatusCount 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanonicalItemStatusCount"
(
    "deletedcount" NUMERIC(20,5) NOT NULL,
    "modifiedtime" DATETIME,
    "successcount" NUMERIC(20,5) NOT NULL,
    "pool" NUMERIC(38,0) NOT NULL,
    "archivedcount" NUMERIC(20,5) NOT NULL,
    "errorcount" NUMERIC(20,5) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalItemStatusCount_pool" ON "CanonicalItemStatusCount" ("pool");

CREATE INDEX "IDX_canItmStsCount_pool" ON "CanonicalItemStatusCount" ("pool");

-- ----------------------------------------------------------------------- 
-- CanonicalItemMeta 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanonicalItemMeta"
(
    "id" NUMERIC(38,0) NOT NULL,
    "itemmetadataid" NUMERIC(20,5) NOT NULL,
    "itemtype" NVARCHAR(255) NOT NULL,
    "typecode" NVARCHAR(128),
    "documentid" VARCHAR(4000),
    "creationtime" DATETIME,
    "modifiedtime" DATETIME,
    "description" NVARCHAR(255),
    "version" NUMERIC(20,5),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "AK_CanonicalItemMeta" ON "CanonicalItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_CanonicalMeta_itemType" ON "CanonicalItemMeta" ("itemtype");

-- ----------------------------------------------------------------------- 
-- CompositionAction 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CompositionAction"
(
    "modifiedtime" DATETIME,
    "count" NUMERIC(20,5),
    "pool" NUMERIC(38,0),
    "endtime" DATETIME,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "starttime" DATETIME,
    "status" NVARCHAR(255) NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CompositionAction_pool" ON "CompositionAction" ("pool");

CREATE INDEX "IDX_CompAction_status" ON "CompositionAction" ("status");

-- ----------------------------------------------------------------------- 
-- CanItemPubStatus 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanItemPubStatus"
(
    "modifiedtime" DATETIME,
    "statusdetail" NVARCHAR(255),
    "targetsystempublication" NUMERIC(38,0) NOT NULL,
    "canonicalitem" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "status" NVARCHAR(255) NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanItemPubStatus_targetsystempublication" ON "CanItemPubStatus" ("targetsystempublication");

CREATE INDEX "FK_CanItemPubStatus_canonicalitem" ON "CanItemPubStatus" ("canonicalitem");

CREATE INDEX "IDX_CanItemPubStatus_status" ON "CanItemPubStatus" ("status");

-- ----------------------------------------------------------------------- 
-- PublicationRetry 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "PublicationRetry"
(
    "targetsystemid" NUMERIC(20,5) NOT NULL,
    "retrycount" INT NOT NULL,
    "modifiedtime" DATETIME,
    "canonicalitemid" NUMERIC(20,5) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "IDX_pubRetry_canonicalItemId" ON "PublicationRetry" ("canonicalitemid");

CREATE INDEX "IDX_pubRetry_targetSystemId" ON "PublicationRetry" ("targetsystemid");

-- ----------------------------------------------------------------------- 
-- CanonicalPubStatusCount 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanonicalPubStatusCount"
(
    "internalerrorcount" NUMERIC(20,5) NOT NULL,
    "modifiedtime" DATETIME,
    "successcount" NUMERIC(20,5) NOT NULL,
    "pool" NUMERIC(38,0) NOT NULL,
    "id" NUMERIC(38,0) NOT NULL,
    "creationtime" DATETIME,
    "externalerrorcount" NUMERIC(20,5) NOT NULL,
    "ignoredcount" NUMERIC(20,5) NOT NULL,
    "typecode" NVARCHAR(128),
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalPubStatusCount_pool" ON "CanonicalPubStatusCount" ("pool");

CREATE INDEX "IDX_canPubStsCount_pool" ON "CanonicalPubStatusCount" ("pool");

-- ----------------------------------------------------------------------- 
-- TargetSysExportCode 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "TargetSysExportCode"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" NVARCHAR(255)
);

CREATE INDEX "IDX_TargetSysExportCode_destid" ON "TargetSysExportCode" ("destid");

CREATE INDEX "IDX_TargetSysExportCode_srcid" ON "TargetSysExportCode" ("srcid");

-- ----------------------------------------------------------------------- 
-- TargetItemMetadata_dependsOn 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "TargetItemMetadata_dependsOn"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" NUMERIC(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_TargetItemMetadata_dependsOn_" ON "TargetItemMetadata_dependsOn" ("srcid", "destid");

CREATE INDEX "IDX_TargetItemMetadata_dependsOn_srcid" ON "TargetItemMetadata_dependsOn" ("srcid");

-- ----------------------------------------------------------------------- 
-- CanonicalItem_RawItem 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "CanonicalItem_RawItem"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" NUMERIC(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_CanonicalItem_RawItem_" ON "CanonicalItem_RawItem" ("srcid", "destid");

CREATE INDEX "IDX_CanonicalItem_RawItem_srcid" ON "CanonicalItem_RawItem" ("srcid");

-- ----------------------------------------------------------------------- 
-- DataHubFeed_Pool 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "DataHubFeed_Pool"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "destid" NUMERIC(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_DataHubFeed_Pool_" ON "DataHubFeed_Pool" ("srcid", "destid");

CREATE INDEX "IDX_DataHubFeed_Pool_srcid" ON "DataHubFeed_Pool" ("srcid");

-- ----------------------------------------------------------------------- 
-- ManagedTarItem_expCodeAttrMap 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "ManagedTarItem_expCodeAttrMap"
(
    "srcid" NUMERIC(38,0) NOT NULL,
    "key" NVARCHAR(255) NOT NULL,
    "value" NVARCHAR(255)
);

CREATE UNIQUE INDEX "PK_ManagedTarItem_expCodeAttrMap_" ON "ManagedTarItem_expCodeAttrMap" ("srcid", "key");

-- ----------------------------------------------------------------------- 
-- hybris_sequences 
-- ----------------------------------------------------------------------- 

SET quoted_identifier on;
CREATE TABLE "hybris_sequences"
(
    "name" NVARCHAR(200) NOT NULL,
    "value" NUMERIC(20,5),
    PRIMARY KEY ("name")
);

-- -----------------------------------------------------------------------
-- DataHubVersion
-- -----------------------------------------------------------------------

CREATE TABLE "DataHubVersion"
(
    "version" NVARCHAR(128) NOT NULL
);

INSERT INTO "DataHubVersion" values ('6.4.0');

