--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

-- -----------------------------------------------------------------------
-- CanonicalAttrDef
-- -----------------------------------------------------------------------

CREATE TABLE "CanonicalAttrDef"
(
    "modifiedtime" DATE,
    "canonicalattrmodeldef" NUMBER(38,0),
    "referenceattribute" VARCHAR2(4000),
    "active" NUMBER,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "rawitemtype" VARCHAR2(255),
    "typecode" VARCHAR2(128),    
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalAtt_alattrmodeldef" ON "CanonicalAttrDef" ("canonicalattrmodeldef");

CREATE INDEX "IDX_CanAttrDef_active_rawType" ON "CanonicalAttrDef" ("rawitemtype", "active");

CREATE INDEX "IDX_CanonicalAt_ef_rawItemType" ON "CanonicalAttrDef" ("rawitemtype");

-- -----------------------------------------------------------------------
-- TargetItemMeta
-- -----------------------------------------------------------------------

CREATE TABLE "TargetItemMeta"
(
    "filterexpression" VARCHAR2(4000),
    "filtereditempubstatus" VARCHAR2(255),
    "targetsystem" NUMBER(38,0),
    "description" VARCHAR2(255),
    "version" NUMBER(20,5),
    "canonicalitemsource" NUMBER(38,0),
    "isexportcodeexpression" NUMBER,
    "modifiedtime" DATE,
    "itemtype" VARCHAR2(255) NOT NULL,
    "itemmetadataid" NUMBER(20,5) NOT NULL,
    "isupdatable" NUMBER,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "exportcode" VARCHAR2(255),
    "typecode" VARCHAR2(128),    
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetItemMeta_targetsystem" ON "TargetItemMeta" ("targetsystem");

CREATE INDEX "FK_TargetItemMe_icalitemsource" ON "TargetItemMeta" ("canonicalitemsource");

CREATE UNIQUE INDEX "AK_TargetItemMeta" ON "TargetItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_TargetMeta_itemType" ON "TargetItemMeta" ("itemtype");

-- -----------------------------------------------------------------------
-- TargetSystemPub
-- -----------------------------------------------------------------------

CREATE TABLE "TargetSystemPub"
(
    "canonicalitemcount" NUMBER(20,5),
    "endtime" DATE,
    "targetsystem" NUMBER(38,0),
    "compositetargetsystempub" NUMBER(38,0),
    "starttime" DATE,
    "internalerrorcount" NUMBER(20,5),
    "modifiedtime" DATE,
    "publicationaction" NUMBER(38,0),
    "subpuborder" NUMBER,
    "publicationtype" VARCHAR2(255),
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "externalerrorcount" NUMBER(20,5),
    "status" VARCHAR2(255) NOT NULL,
    "ignoredcount" NUMBER(20,5),
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetSystem_b_targetsystem" ON "TargetSystemPub" ("targetsystem");

CREATE INDEX "FK_TargetSystem_argetsystempub" ON "TargetSystemPub" ("compositetargetsystempub");

CREATE INDEX "FK_TargetSystem_licationaction" ON "TargetSystemPub" ("publicationaction");

-- -----------------------------------------------------------------------
-- PublicationAction
-- -----------------------------------------------------------------------

CREATE TABLE "PublicationAction"
(
    "modifiedtime" DATE,
    "pool" NUMBER(38,0),
    "endtime" DATE,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "starttime" DATE,
    "status" VARCHAR2(255) NOT NULL,
    "typecode" VARCHAR2(128),    
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_PublicationAction_pool" ON "PublicationAction" ("pool");

-- -----------------------------------------------------------------------
-- PublicationError
-- -----------------------------------------------------------------------

CREATE TABLE "PublicationError"
(
    "canonicalitempublicationstatus" NUMBER(38,0),
    "code" VARCHAR2(255) NOT NULL,
    "modifiedtime" DATE,
    "targetsystempublication" NUMBER(38,0) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "message" NCLOB NOT NULL,
    "typecode" VARCHAR2(128),    
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_PublicationE_licationstatus" ON "PublicationError" ("canonicalitempublicationstatus");

CREATE INDEX "FK_PublicationE_tempublication" ON "PublicationError" ("targetsystempublication");

-- -----------------------------------------------------------------------
-- RawItemStatusCount
-- -----------------------------------------------------------------------

CREATE TABLE "RawItemStatusCount"
(
    "modifiedtime" DATE,
    "pool" NUMBER(38,0) NOT NULL,
    "pendingcount" NUMBER(20,5) NOT NULL,
    "processedcount" NUMBER(20,5) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "ignoredcount" NUMBER(20,5) NOT NULL,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawItemStatusCount_pool" ON "RawItemStatusCount" ("pool");

-- -----------------------------------------------------------------------
-- RawItemMeta
-- -----------------------------------------------------------------------

CREATE TABLE "RawItemMeta"
(
    "modifiedtime" DATE,
    "itemtype" VARCHAR2(255) NOT NULL,
    "itemmetadataid" NUMBER(20,5) NOT NULL,
    "description" VARCHAR2(255),
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "version" NUMBER(20,5),
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "AK_RawItemMeta" ON "RawItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_RawMeta_itemType" ON "RawItemMeta" ("itemtype");

-- -----------------------------------------------------------------------
-- DataLoadingAction
-- -----------------------------------------------------------------------

CREATE TABLE "DataLoadingAction"
(
    "feed" NUMBER(38,0),
    "modifiedtime" DATE,
    "count" NUMBER(20,5),
    "endtime" DATE,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "starttime" DATE,
    "message" VARCHAR2(255),
    "status" VARCHAR2(255) NOT NULL,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_DataLoadingAction_feed" ON "DataLoadingAction" ("feed");

CREATE INDEX "IDX_DataLoadingAction_endTime" ON "DataLoadingAction" ("endtime");

CREATE INDEX "IDX_DataLoadingAction_status" ON "DataLoadingAction" ("status");

-- -----------------------------------------------------------------------
-- RawItem
-- -----------------------------------------------------------------------

CREATE TABLE "RawItem"
(
    "modifiedtime" DATE,
    "itemtype" VARCHAR2(255),
    "isocode" VARCHAR2(255),
    "dataloadingaction" NUMBER(38,0),
    "batchid" VARCHAR2(255),
    "traceid" VARCHAR2(36),
    "uuid" VARCHAR2(36),
    "datapool" NUMBER(38,0),
    "extensionsource" VARCHAR2(255),
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "delete" NUMBER,
    "version" NUMBER(20,5),
    "status" VARCHAR2(255),
    "typecode" VARCHAR2(128),
    "schemaless_attrs" BLOB,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawItem_dataloadingaction" ON "RawItem" ("dataloadingaction");

CREATE INDEX "FK_RawItem_datapool" ON "RawItem" ("datapool");

CREATE INDEX "IDX_ManagedRaw_itemType" ON "RawItem" ("itemtype");

CREATE INDEX "IDX_RawItem_status" ON "RawItem" ("status");

CREATE INDEX "IDX_RawItem_ActIdTypePoolStat" ON "RawItem" ("dataloadingaction", "id", "itemtype", "datapool", "status");

-- -----------------------------------------------------------------------
-- TargetSystem
-- -----------------------------------------------------------------------

CREATE TABLE "TargetSystem"
(
    "targetsystemname" VARCHAR2(255) NOT NULL,
    "password" VARCHAR2(255),
    "modifiedtime" DATE,
    "targetsystemtype" VARCHAR2(255) NOT NULL,
    "exporturl" VARCHAR2(255) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "username" VARCHAR2(255),
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_TargetSystem_rgetSystemName" ON "TargetSystem" ("targetsystemname");

-- -----------------------------------------------------------------------
-- CanonicalItem
-- -----------------------------------------------------------------------

CREATE TABLE "CanonicalItem"
(
    "modifiedtime" DATE,
    "itemtype" VARCHAR2(255),
    "compositionaction" NUMBER(38,0),
    "datapool" NUMBER(38,0),
    "id" NUMBER(38,0) NOT NULL,
    "batchid" VARCHAR2(255),
    "traceid" VARCHAR2(36),
    "uuid" VARCHAR2(36),
    "documentid" VARCHAR2(255),
    "creationtime" DATE,
    "compositionstatusdetail" VARCHAR2(255),
    "version" NUMBER(20,5),
    "integrationkey" VARCHAR2(255),
    "status" VARCHAR2(255) NOT NULL,
    "typecode" VARCHAR2(128),
    "schemaless_attrs" BLOB,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalIte_positionaction" ON "CanonicalItem" ("compositionaction");

CREATE INDEX "FK_CanonicalItem_datapool" ON "CanonicalItem" ("datapool");

CREATE INDEX "IDX_CanonicalItem_status" ON "CanonicalItem" ("status");

CREATE INDEX "IDX_ManagedCanonical_itemType" ON "CanonicalItem" ("itemtype");

CREATE INDEX "IDX_id_status" ON "CanonicalItem" ("id", "status");

CREATE INDEX "IDX_integrationKey_status" ON "CanonicalItem" ("integrationkey", "status");

CREATE INDEX "IDX_CanonIte_KeyTypePoolStatId" ON "CanonicalItem" ("integrationkey", "itemtype", "datapool", "status", "id");

-- -----------------------------------------------------------------------
-- TargetAttrDef
-- -----------------------------------------------------------------------

CREATE TABLE "TargetAttrDef"
(
    "targetitemmetadata" NUMBER(38,0),
    "active" NUMBER,
    "attributename" VARCHAR2(4000) NOT NULL,
    "collection" NUMBER,
    "transformationexpression" VARCHAR2(4000),
    "mandatoryinheader" NUMBER,
    "modifiedtime" DATE,
    "localizable" NUMBER,
    "exportcodeexpression" NUMBER,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "secured" NUMBER NOT NULL,
    "exportcode" VARCHAR2(255),
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetAttrDe_etitemmetadata" ON "TargetAttrDef" ("targetitemmetadata");

-- -----------------------------------------------------------------------
-- DataHubFeed
-- -----------------------------------------------------------------------

CREATE TABLE "DataHubFeed"
(
    "defaultcompositionstrategy" VARCHAR2(255),
    "modifiedtime" DATE,
    "poolingstrategy" VARCHAR2(255),
    "name" VARCHAR2(255) NOT NULL,
    "description" VARCHAR2(255),
    "poolingcondition" VARCHAR2(255),
    "defaultpublicationstrategy" VARCHAR2(255),
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_DataHubFeed_name" ON "DataHubFeed" ("name");

-- -----------------------------------------------------------------------
-- DataHubPool
-- -----------------------------------------------------------------------

CREATE TABLE "DataHubPool"
(
    "poolname" VARCHAR2(255) NOT NULL,
    "publicationstrategy" VARCHAR2(255),
    "modifiedtime" DATE,
    "compositionstrategy" VARCHAR2(255),
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "UX_DataHubPool_poolName" ON "DataHubPool" ("poolname");

-- -----------------------------------------------------------------------
-- CanonicalAttrModDef
-- -----------------------------------------------------------------------

CREATE TABLE "CanonicalAttrModDef"
(
    "canonicalitemmetadata" NUMBER(38,0),
    "modifiedtime" DATE,
    "localizable" NUMBER,
    "attributename" VARCHAR2(4000) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "collection" NUMBER,
    "secured" NUMBER NOT NULL,
    "primarykey" NUMBER,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalAtt_alitemmetadata" ON "CanonicalAttrModDef" ("canonicalitemmetadata");

CREATE INDEX "IDX_CanAttrMod_PK" ON "CanonicalAttrModDef" ("primarykey");

-- -----------------------------------------------------------------------
-- RawAttrModDef
-- -----------------------------------------------------------------------

CREATE TABLE "RawAttrModDef"
(
    "modifiedtime" DATE,
    "rawitemmetadata" NUMBER(38,0),
    "localizable" NUMBER,
    "attributename" VARCHAR2(4000) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "collection" NUMBER,
    "secured" NUMBER NOT NULL,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_RawAttrModDe_awitemmetadata" ON "RawAttrModDef" ("rawitemmetadata");

-- -----------------------------------------------------------------------
-- TargetItem
-- -----------------------------------------------------------------------

CREATE TABLE "TargetItem"
(
    "modifiedtime" DATE,
    "itemtype" VARCHAR2(255),
    "targetsystempublication" NUMBER(38,0),
    "datapool" NUMBER(38,0),
    "canonicalitem" NUMBER(38,0) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "version" NUMBER(20,5),
    "exportcode" VARCHAR2(255),
    "typecode" VARCHAR2(128),
    "schemaless_attrs" BLOB,
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_TargetItem_t_tempublication" ON "TargetItem" ("targetsystempublication");

CREATE INDEX "FK_TargetItem_datapool" ON "TargetItem" ("datapool");

CREATE INDEX "FK_TargetItem_canonicalitem" ON "TargetItem" ("canonicalitem");

CREATE INDEX "IDX_ManagedTarget_itemType" ON "TargetItem" ("itemtype");

CREATE INDEX "IDX_TarItemIdTypePublication" ON "TargetItem" ("id", "itemtype", "targetsystempublication");

-- -----------------------------------------------------------------------
-- CanonicalItemStatusCount
-- -----------------------------------------------------------------------

CREATE TABLE "CanonicalItemStatusCount"
(
    "deletedcount" NUMBER(20,5) NOT NULL,
    "modifiedtime" DATE,
    "successcount" NUMBER(20,5) NOT NULL,
    "pool" NUMBER(38,0) NOT NULL,
    "archivedcount" NUMBER(20,5) NOT NULL,
    "errorcount" NUMBER(20,5) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalIte_atusCount_pool" ON "CanonicalItemStatusCount" ("pool");

-- -----------------------------------------------------------------------
-- CanonicalItemMeta
-- -----------------------------------------------------------------------

CREATE TABLE "CanonicalItemMeta"
(
    "id" NUMBER(38,0) NOT NULL,
    "itemmetadataid" NUMBER(20,5) NOT NULL,
    "itemtype" VARCHAR2(255) NOT NULL,
    "typecode" VARCHAR2(128),
    "description" VARCHAR2(255),
    "documentid" VARCHAR2(4000),
    "creationtime" DATE,
    "modifiedtime" DATE,
    "version" NUMBER(20,5),
    PRIMARY KEY ("id")
);

CREATE UNIQUE INDEX "AK_CanonicalItemMeta" ON "CanonicalItemMeta" ("itemmetadataid");

CREATE INDEX "IDX_CanonicalMeta_itemType" ON "CanonicalItemMeta" ("itemtype");

-- -----------------------------------------------------------------------
-- CompositionAction
-- -----------------------------------------------------------------------

CREATE TABLE "CompositionAction"
(
    "modifiedtime" DATE,
    "count" NUMBER(20,5),
    "pool" NUMBER(38,0),
    "endtime" DATE,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "starttime" DATE,
    "status" VARCHAR2(255) NOT NULL,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CompositionAction_pool" ON "CompositionAction" ("pool");

CREATE INDEX "IDX_CompAction_status" ON "CompositionAction" ("status");

-- -----------------------------------------------------------------------
-- CanItemPubStatus
-- -----------------------------------------------------------------------

CREATE TABLE "CanItemPubStatus"
(
    "modifiedtime" DATE,
    "statusdetail" VARCHAR2(255),
    "targetsystempublication" NUMBER(38,0) NOT NULL,
    "canonicalitem" NUMBER(38,0) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "status" VARCHAR2(255) NOT NULL,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanItemPubSt_tempublication" ON "CanItemPubStatus" ("targetsystempublication");

CREATE INDEX "FK_CanItemPubSt_canonicalitem" ON "CanItemPubStatus" ("canonicalitem");

CREATE INDEX "IDX_CanItemPubStatus_status" ON "CanItemPubStatus" ("status");

-- -----------------------------------------------------------------------
-- PublicationRetry
-- -----------------------------------------------------------------------

CREATE TABLE "PublicationRetry"
(
    "targetsystemid" NUMBER(20,5) NOT NULL,
    "retrycount" NUMBER NOT NULL,
    "modifiedtime" DATE,
    "canonicalitemid" NUMBER(20,5) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "IDX_pubRetry_canonicalItemId" ON "PublicationRetry" ("canonicalitemid");

CREATE INDEX "IDX_pubRetry_targetSystemId" ON "PublicationRetry" ("targetsystemid");

-- -----------------------------------------------------------------------
-- CanonicalPubStatusCount
-- -----------------------------------------------------------------------

CREATE TABLE "CanonicalPubStatusCount"
(
    "internalerrorcount" NUMBER(20,5) NOT NULL,
    "modifiedtime" DATE,
    "successcount" NUMBER(20,5) NOT NULL,
    "pool" NUMBER(38,0) NOT NULL,
    "id" NUMBER(38,0) NOT NULL,
    "creationtime" DATE,
    "externalerrorcount" NUMBER(20,5) NOT NULL,
    "ignoredcount" NUMBER(20,5) NOT NULL,
    "typecode" VARCHAR2(128),   
    PRIMARY KEY ("id")
);

CREATE INDEX "FK_CanonicalPub_atusCount_pool" ON "CanonicalPubStatusCount" ("pool");

-- -----------------------------------------------------------------------
-- TargetSysExportCode
-- -----------------------------------------------------------------------

CREATE TABLE "TargetSysExportCode"
(   
    "srcid" NUMBER(38,0) NOT NULL,
    "destid" VARCHAR2(255)
);

CREATE INDEX "IDX_TargetSysExportCode_destid" ON "TargetSysExportCode" ("destid");

CREATE INDEX "IDX_TargetSysExportCode_srcid" ON "TargetSysExportCode" ("srcid");

-- -----------------------------------------------------------------------
-- TargetItemMetadata_dependsOn
-- -----------------------------------------------------------------------

CREATE TABLE "TargetItemMetadata_dependsOn"
(   
    "srcid" NUMBER(38,0) NOT NULL,
    "destid" NUMBER(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_TargetItemMe_ata_dependsOn_" ON "TargetItemMetadata_dependsOn" ("srcid", "destid");

CREATE INDEX "IDX_TargetItemM_ependsOn_srcid" ON "TargetItemMetadata_dependsOn" ("srcid");

-- -----------------------------------------------------------------------
-- CanonicalItem_RawItem
-- -----------------------------------------------------------------------

CREATE TABLE "CanonicalItem_RawItem"
(   
    "srcid" NUMBER(38,0) NOT NULL,
    "destid" NUMBER(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_CanonicalItem_RawItem_" ON "CanonicalItem_RawItem" ("srcid", "destid");

CREATE INDEX "IDX_CanonicalIt_RawItem_srcid" ON "CanonicalItem_RawItem" ("srcid");

-- -----------------------------------------------------------------------
-- DataHubFeed_Pool
-- -----------------------------------------------------------------------

CREATE TABLE "DataHubFeed_Pool"
(    
    "srcid" NUMBER(38,0) NOT NULL,
    "destid" NUMBER(38,0) NOT NULL
);

CREATE UNIQUE INDEX "PK_DataHubFeed_Pool_" ON "DataHubFeed_Pool" ("srcid", "destid");

CREATE INDEX "IDX_DataHubFeed_Pool_srcid" ON "DataHubFeed_Pool" ("srcid");

-- -----------------------------------------------------------------------
-- ManagedTarItem_expCodeAttrMap
-- -----------------------------------------------------------------------

CREATE TABLE "ManagedTarItem_expCodeAttrMap"
(    
    "srcid" NUMBER(38,0) NOT NULL,
    "key" VARCHAR2(255) NOT NULL,
    "value" VARCHAR2(255)
);

CREATE UNIQUE INDEX "PK_ManagedTarIt_xpCodeAttrMap_" ON "ManagedTarItem_expCodeAttrMap" ("srcid", "key");


-- -----------------------------------------------------------------------
-- hybris_sequences
-- -----------------------------------------------------------------------

CREATE TABLE "hybris_sequences"
(
    "name" VARCHAR2(200) NOT NULL,    
    "value" NUMBER(20,5),
    PRIMARY KEY ("name")
);

-- -----------------------------------------------------------------------
-- DataHubVersion
-- -----------------------------------------------------------------------

CREATE TABLE "DataHubVersion"
(
    "version" VARCHAR(128) NOT NULL
);

INSERT INTO "DataHubVersion" values ('6.4.0');

