--
-- [y] hybris Platform
--
-- Copyright (c) 2017 SAP SE or an SAP affiliate company.
-- All rights reserved.
--
-- This software is the confidential and proprietary information of SAP
-- ("Confidential Information"). You shall not disclose such Confidential
-- Information and shall use it only in accordance with the terms of the
-- license agreement you entered into with SAP.
--

# -----------------------------------------------------------------------
# CanonicalAttrDef
# -----------------------------------------------------------------------

CREATE TABLE `CanonicalAttrDef`
(
    `modifiedtime` DATETIME,
    `canonicalattrmodeldef` DECIMAL(38,0),
    `referenceattribute` VARCHAR(4000) NULL,
    `active` INTEGER,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `rawitemtype` VARCHAR(255) NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_CanonicalAttrDef_canonicalattrmodeldef` ON `CanonicalAttrDef` (`canonicalattrmodeldef`);

CREATE INDEX `IDX_CanAttrDef_active_rawType` ON `CanonicalAttrDef` (`rawitemtype`, `active`);

CREATE INDEX `IDX_CanonicalAttrDef_rawItemType` ON `CanonicalAttrDef` (`rawitemtype`);

# -----------------------------------------------------------------------
# TargetItemMeta
# -----------------------------------------------------------------------

CREATE TABLE `TargetItemMeta`
(
    `filterexpression` VARCHAR(4000) NULL,
    `filtereditempubstatus` VARCHAR(255) NULL,
    `targetsystem` DECIMAL(38,0),
    `description` VARCHAR(255) NULL,
    `version` DECIMAL(20,5),
    `canonicalitemsource` DECIMAL(38,0),
    `isexportcodeexpression` INTEGER,
    `modifiedtime` DATETIME,
    `itemtype` VARCHAR(255) NOT NULL,
    `itemmetadataid` DECIMAL(20,5) NOT NULL,
    `isupdatable` INTEGER,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `exportcode` VARCHAR(255) NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_TargetItemMeta_targetsystem` ON `TargetItemMeta` (`targetsystem`);

CREATE INDEX `FK_TargetItemMeta_canonicalitemsource` ON `TargetItemMeta` (`canonicalitemsource`);

CREATE UNIQUE INDEX `AK_TargetItemMeta` ON `TargetItemMeta` (`itemmetadataid`);

CREATE INDEX `IDX_TargetMeta_itemType` ON `TargetItemMeta` (`itemtype`);

# -----------------------------------------------------------------------
# TargetSystemPub
# -----------------------------------------------------------------------

CREATE TABLE `TargetSystemPub`
(
    `canonicalitemcount` DECIMAL(20,5),
    `compositetargetsystempub` DECIMAL(38,0),
    `endtime` DATETIME,
    `targetsystem` DECIMAL(38,0),
    `starttime` DATETIME,
    `internalerrorcount` DECIMAL(20,5),
    `modifiedtime` DATETIME,
    `subpuborder` INTEGER,
    `publicationaction` DECIMAL(38,0),
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `publicationtype` VARCHAR(255) NULL,
    `externalerrorcount` DECIMAL(20,5),
    `status` VARCHAR(255) NOT NULL,
    `ignoredcount` DECIMAL(20,5),
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_TargetSystemPub_compositetargetsystempub` ON `TargetSystemPub` (`compositetargetsystempub`);

CREATE INDEX `FK_TargetSystemPub_targetsystem` ON `TargetSystemPub` (`targetsystem`);

CREATE INDEX `FK_TargetSystemPub_publicationaction` ON `TargetSystemPub` (`publicationaction`);

# -----------------------------------------------------------------------
# PublicationAction
# -----------------------------------------------------------------------

CREATE TABLE `PublicationAction`
(
    `modifiedtime` DATETIME,
    `pool` DECIMAL(38,0),
    `endtime` DATETIME,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `starttime` DATETIME,
    `status` VARCHAR(255) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_PublicationAction_pool` ON `PublicationAction` (`pool`);

# -----------------------------------------------------------------------
# PublicationError
# -----------------------------------------------------------------------

CREATE TABLE `PublicationError`
(
    `canonicalitempublicationstatus` DECIMAL(38,0),
    `code` VARCHAR(255) NOT NULL,
    `modifiedtime` DATETIME,
    `targetsystempublication` DECIMAL(38,0) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `message` LONGTEXT NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_PublicationError_canonicalitempublicationstatus` ON `PublicationError` (`canonicalitempublicationstatus`);

CREATE INDEX `FK_PublicationError_targetsystempublication` ON `PublicationError` (`targetsystempublication`);

# -----------------------------------------------------------------------
# RawItemStatusCount
# -----------------------------------------------------------------------

CREATE TABLE `RawItemStatusCount`
(
    `modifiedtime` DATETIME,
    `pool` DECIMAL(38,0) NOT NULL,
    `pendingcount` DECIMAL(20,5) NOT NULL,
    `processedcount` DECIMAL(20,5) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `ignoredcount` DECIMAL(20,5) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_RawItemStatusCount_pool` ON `RawItemStatusCount` (`pool`);

CREATE INDEX `IDX_rawItemStatusCount_pool` ON `RawItemStatusCount` (`pool`);

# -----------------------------------------------------------------------
# RawItemMeta
# -----------------------------------------------------------------------

CREATE TABLE `RawItemMeta`
(
    `modifiedtime` DATETIME,
    `itemtype` VARCHAR(255) NOT NULL,
    `itemmetadataid` DECIMAL(20,5) NOT NULL,
    `description` VARCHAR(255) NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `version` DECIMAL(20,5),
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX `AK_RawItemMeta` ON `RawItemMeta` (`itemmetadataid`);

CREATE INDEX `IDX_RawMeta_itemType` ON `RawItemMeta` (`itemtype`);

# -----------------------------------------------------------------------
# DataLoadingAction
# -----------------------------------------------------------------------

CREATE TABLE `DataLoadingAction`
(
    `feed` DECIMAL(38,0),
    `modifiedtime` DATETIME,
    `count` DECIMAL(20,5),
    `endtime` DATETIME,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `starttime` DATETIME,
    `message` VARCHAR(255) NULL,
    `status` VARCHAR(255) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_DataLoadingAction_feed` ON `DataLoadingAction` (`feed`);

CREATE INDEX `IDX_DataLoadingAction_endTime` ON `DataLoadingAction` (`endtime`);

CREATE INDEX `IDX_DataLoadingAction_status` ON `DataLoadingAction` (`status`);

# -----------------------------------------------------------------------
# RawItem
# -----------------------------------------------------------------------

CREATE TABLE `RawItem`
(
    `modifiedtime` DATETIME,
    `itemtype` VARCHAR(255) NULL,
    `isocode` VARCHAR(255) NULL,
    `dataloadingaction` DECIMAL(38,0),
    `batchid` VARCHAR(255),
    `traceid` VARCHAR(36),
    `uuid` VARCHAR(36),
    `datapool` DECIMAL(38,0),
    `extensionsource` VARCHAR(255) NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `delete` INTEGER,
    `version` DECIMAL(20,5),
    `status` VARCHAR(255) NULL,
    `typecode` VARCHAR(128) NULL,
    `schemaless_attrs` LONGBLOB NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_RawItem_dataloadingaction` ON `RawItem` (`dataloadingaction`);

CREATE INDEX `FK_RawItem_datapool` ON `RawItem` (`datapool`);

CREATE INDEX `IDX_ManagedRaw_itemType` ON `RawItem` (`itemtype`);

CREATE INDEX `IDX_RawItem_status` ON `RawItem` (`status`);

CREATE INDEX `IDX_RawItem_ActIdTypePoolStat` ON `RawItem` (`dataloadingaction`, `id`, `itemtype`, `datapool`, `status`);

# -----------------------------------------------------------------------
# TargetSystem
# -----------------------------------------------------------------------

CREATE TABLE `TargetSystem`
(
    `targetsystemname` VARCHAR(255) NOT NULL,
    `password` VARCHAR(255) NULL,
    `modifiedtime` DATETIME,
    `targetsystemtype` VARCHAR(255) NOT NULL,
    `exporturl` VARCHAR(255) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `username` VARCHAR(255) NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX `UX_TargetSystem_targetSystemName` ON `TargetSystem` (`targetsystemname`);

# -----------------------------------------------------------------------
# CanonicalItem
# -----------------------------------------------------------------------

CREATE TABLE `CanonicalItem`
(
    `modifiedtime` DATETIME,
    `itemtype` VARCHAR(255) NULL,
    `compositionaction` DECIMAL(38,0),
    `datapool` DECIMAL(38,0),
    `id` DECIMAL(38,0) NOT NULL,
    `batchid` VARCHAR(255),
    `traceid` VARCHAR(36),
    `uuid` VARCHAR(36),
    `documentid` VARCHAR(255),
    `creationtime` DATETIME,
    `compositionstatusdetail` VARCHAR(255) NULL,
    `version` DECIMAL(20,5),
    `integrationkey` VARCHAR(255) NULL,
    `status` VARCHAR(255) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    `schemaless_attrs` LONGBLOB NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_CanonicalItem_compositionaction` ON `CanonicalItem` (`compositionaction`);

CREATE INDEX `FK_CanonicalItem_datapool` ON `CanonicalItem` (`datapool`);

CREATE INDEX `IDX_CanonicalItem_status` ON `CanonicalItem` (`status`);

CREATE INDEX `IDX_ManagedCanonical_itemType` ON `CanonicalItem` (`itemtype`);

CREATE INDEX `IDX_id_status` ON `CanonicalItem` (`id`, `status`);

CREATE INDEX `IDX_integrationKey_status` ON `CanonicalItem` (`integrationkey`, `status`);

CREATE INDEX `IDX_CanonIte_KeyTypePoolStatId` ON `CanonicalItem` (`integrationkey`, `itemtype`, `datapool`, `status`, `id`);

# -----------------------------------------------------------------------
# TargetAttrDef
# -----------------------------------------------------------------------

CREATE TABLE `TargetAttrDef`
(
    `targetitemmetadata` DECIMAL(38,0),
    `active` INTEGER,
    `attributename` VARCHAR(4000) NOT NULL,
    `collection` INTEGER,
    `transformationexpression` VARCHAR(4000) NULL,
    `mandatoryinheader` INTEGER,
    `modifiedtime` DATETIME,
    `localizable` INTEGER,
    `exportcodeexpression` INTEGER,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `secured` INTEGER NOT NULL,
    `exportcode` VARCHAR(255) NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_TargetAttrDef_targetitemmetadata` ON `TargetAttrDef` (`targetitemmetadata`);

# -----------------------------------------------------------------------
# DataHubFeed
# -----------------------------------------------------------------------

CREATE TABLE `DataHubFeed`
(
    `defaultcompositionstrategy` VARCHAR(255) NULL,
    `modifiedtime` DATETIME,
    `poolingstrategy` VARCHAR(255) NULL,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NULL,
    `poolingcondition` VARCHAR(255) NULL,
    `defaultpublicationstrategy` VARCHAR(255) NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX `UX_DataHubFeed_name` ON `DataHubFeed` (`name`);

# -----------------------------------------------------------------------
# DataHubPool
# -----------------------------------------------------------------------

CREATE TABLE `DataHubPool`
(
    `poolname` VARCHAR(255) NOT NULL,
    `publicationstrategy` VARCHAR(255) NULL,
    `modifiedtime` DATETIME,
    `compositionstrategy` VARCHAR(255) NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX `UX_DataHubPool_poolName` ON `DataHubPool` (`poolname`);

# -----------------------------------------------------------------------
# CanonicalAttrModDef
# -----------------------------------------------------------------------

CREATE TABLE `CanonicalAttrModDef`
(
    `canonicalitemmetadata` DECIMAL(38,0),
    `modifiedtime` DATETIME,
    `localizable` INTEGER,
    `attributename` VARCHAR(4000) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `collection` INTEGER,
    `secured` INTEGER NOT NULL,
    `primarykey` INTEGER,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_CanonicalAttrModDef_canonicalitemmetadata` ON `CanonicalAttrModDef` (`canonicalitemmetadata`);

CREATE INDEX `IDX_CanAttrMod_PK` ON `CanonicalAttrModDef` (`primarykey`);

# -----------------------------------------------------------------------
# RawAttrModDef
# -----------------------------------------------------------------------

CREATE TABLE `RawAttrModDef`
(
    `modifiedtime` DATETIME,
    `rawitemmetadata` DECIMAL(38,0),
    `localizable` INTEGER,
    `attributename` VARCHAR(4000) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `collection` INTEGER,
    `secured` INTEGER NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_RawAttrModDef_rawitemmetadata` ON `RawAttrModDef` (`rawitemmetadata`);

# -----------------------------------------------------------------------
# TargetItem
# -----------------------------------------------------------------------

CREATE TABLE `TargetItem`
(
    `modifiedtime` DATETIME,
    `itemtype` VARCHAR(255) NULL,
    `targetsystempublication` DECIMAL(38,0),
    `datapool` DECIMAL(38,0),
    `canonicalitem` DECIMAL(38,0) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `version` DECIMAL(20,5),
    `exportcode` VARCHAR(255) NULL,
    `typecode` VARCHAR(128) NULL,
    `schemaless_attrs` LONGBLOB NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_TargetItem_targetsystempublication` ON `TargetItem` (`targetsystempublication`);

CREATE INDEX `FK_TargetItem_datapool` ON `TargetItem` (`datapool`);

CREATE INDEX `FK_TargetItem_canonicalitem` ON `TargetItem` (`canonicalitem`);

CREATE INDEX `IDX_ManagedTarget_itemType` ON `TargetItem` (`itemtype`);

CREATE INDEX `IDX_TarItemIdTypePublication` ON `TargetItem` (`id`, `itemtype`, `targetsystempublication`);

# -----------------------------------------------------------------------
# CanonicalItemStatusCount
# -----------------------------------------------------------------------

CREATE TABLE `CanonicalItemStatusCount`
(
    `deletedcount` DECIMAL(20,5) NOT NULL,
    `modifiedtime` DATETIME,
    `successcount` DECIMAL(20,5) NOT NULL,
    `pool` DECIMAL(38,0) NOT NULL,
    `archivedcount` DECIMAL(20,5) NOT NULL,
    `errorcount` DECIMAL(20,5) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_CanonicalItemStatusCount_pool` ON `CanonicalItemStatusCount` (`pool`);

CREATE INDEX `IDX_canItmStsCount_pool` ON `CanonicalItemStatusCount` (`pool`);

# -----------------------------------------------------------------------
# CanonicalItemMeta
# -----------------------------------------------------------------------

CREATE TABLE `CanonicalItemMeta`
(
    `id` DECIMAL(38,0) NOT NULL,
    `itemmetadataid` DECIMAL(20,5) NOT NULL,
    `itemtype` VARCHAR(255) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    `description` VARCHAR(255) NULL,
    `documentid` VARCHAR(4000),
    `creationtime` DATETIME,
    `modifiedtime` DATETIME,
    `version` DECIMAL(20,5),
    PRIMARY KEY (`id`)
);

CREATE UNIQUE INDEX `AK_CanonicalItemMeta` ON `CanonicalItemMeta` (`itemmetadataid`);

CREATE INDEX `IDX_CanonicalMeta_itemType` ON `CanonicalItemMeta` (`itemtype`);

# -----------------------------------------------------------------------
# CompositionAction
# -----------------------------------------------------------------------

CREATE TABLE `CompositionAction`
(
    `modifiedtime` DATETIME,
    `count` DECIMAL(20,5),
    `pool` DECIMAL(38,0),
    `endtime` DATETIME,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `starttime` DATETIME,
    `status` VARCHAR(255) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_CompositionAction_pool` ON `CompositionAction` (`pool`);

CREATE INDEX `IDX_CompAction_status` ON `CompositionAction` (`status`);

# -----------------------------------------------------------------------
# CanItemPubStatus
# -----------------------------------------------------------------------

CREATE TABLE `CanItemPubStatus`
(
    `modifiedtime` DATETIME,
    `statusdetail` VARCHAR(255) NULL,
    `targetsystempublication` DECIMAL(38,0) NOT NULL,
    `canonicalitem` DECIMAL(38,0) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `status` VARCHAR(255) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_CanItemPubStatus_targetsystempublication` ON `CanItemPubStatus` (`targetsystempublication`);

CREATE INDEX `FK_CanItemPubStatus_canonicalitem` ON `CanItemPubStatus` (`canonicalitem`);

CREATE INDEX `IDX_CanItemPubStatus_status` ON `CanItemPubStatus` (`status`);

# -----------------------------------------------------------------------
# PublicationRetry
# -----------------------------------------------------------------------

CREATE TABLE `PublicationRetry`
(
    `targetsystemid` DECIMAL(20,5) NOT NULL,
    `retrycount` INTEGER NOT NULL,
    `modifiedtime` DATETIME,
    `canonicalitemid` DECIMAL(20,5) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `IDX_pubRetry_canonicalItemId` ON `PublicationRetry` (`canonicalitemid`);

CREATE INDEX `IDX_pubRetry_targetSystemId` ON `PublicationRetry` (`targetsystemid`);

# -----------------------------------------------------------------------
# CanonicalPubStatusCount
# -----------------------------------------------------------------------

CREATE TABLE `CanonicalPubStatusCount`
(
    `internalerrorcount` DECIMAL(20,5) NOT NULL,
    `modifiedtime` DATETIME,
    `successcount` DECIMAL(20,5) NOT NULL,
    `pool` DECIMAL(38,0) NOT NULL,
    `id` DECIMAL(38,0) NOT NULL,
    `creationtime` DATETIME,
    `externalerrorcount` DECIMAL(20,5) NOT NULL,
    `ignoredcount` DECIMAL(20,5) NOT NULL,
    `typecode` VARCHAR(128) NULL,
    PRIMARY KEY (`id`)
);

CREATE INDEX `FK_CanonicalPubStatusCount_pool` ON `CanonicalPubStatusCount` (`pool`);

CREATE INDEX `IDX_canPubStsCount_pool` ON `CanonicalPubStatusCount` (`pool`);

# -----------------------------------------------------------------------
# TargetSysExportCode
# -----------------------------------------------------------------------

CREATE TABLE `TargetSysExportCode`
(
    `srcid` DECIMAL(38,0) NOT NULL,
    `destid` VARCHAR(255) NULL
);

CREATE INDEX `IDX_TargetSysExportCode_destid` ON `TargetSysExportCode` (`destid`);

CREATE INDEX `IDX_TargetSysExportCode_srcid` ON `TargetSysExportCode` (`srcid`);

# -----------------------------------------------------------------------
# TargetItemMetadata_dependsOn
# -----------------------------------------------------------------------

CREATE TABLE `TargetItemMetadata_dependsOn`
(
    `srcid` DECIMAL(38,0) NOT NULL,
    `destid` DECIMAL(38,0) NOT NULL
);

CREATE UNIQUE INDEX `PK_TargetItemMetadata_dependsOn_` ON `TargetItemMetadata_dependsOn` (`srcid`, `destid`);

CREATE INDEX `IDX_TargetItemMetadata_dependsOn_srcid` ON `TargetItemMetadata_dependsOn` (`srcid`);

# -----------------------------------------------------------------------
# CanonicalItem_RawItem
# -----------------------------------------------------------------------

CREATE TABLE `CanonicalItem_RawItem`
(
    `srcid` DECIMAL(38,0) NOT NULL,
    `destid` DECIMAL(38,0) NOT NULL
);

CREATE UNIQUE INDEX `PK_CanonicalItem_RawItem_` ON `CanonicalItem_RawItem` (`srcid`, `destid`);

CREATE INDEX `IDX_CanonicalItem_RawItem_srcid` ON `CanonicalItem_RawItem` (`srcid`);

# -----------------------------------------------------------------------
# DataHubFeed_Pool
# -----------------------------------------------------------------------

CREATE TABLE `DataHubFeed_Pool`
(
    `srcid` DECIMAL(38,0) NOT NULL,
    `destid` DECIMAL(38,0) NOT NULL
);

CREATE UNIQUE INDEX `PK_DataHubFeed_Pool_` ON `DataHubFeed_Pool` (`srcid`, `destid`);

CREATE INDEX `IDX_DataHubFeed_Pool_srcid` ON `DataHubFeed_Pool` (`srcid`);

# -----------------------------------------------------------------------
# ManagedTarItem_expCodeAttrMap
# -----------------------------------------------------------------------

CREATE TABLE `ManagedTarItem_expCodeAttrMap`
(
    `srcid` DECIMAL(38,0) NOT NULL,
    `key` VARCHAR(255) NOT NULL,
    `value` VARCHAR(255) NULL
);

CREATE UNIQUE INDEX `PK_ManagedTarItem_expCodeAttrMap_` ON `ManagedTarItem_expCodeAttrMap` (`srcid`, `key`);

# -----------------------------------------------------------------------
# hybris_sequences
# -----------------------------------------------------------------------

CREATE TABLE `hybris_sequences`
(
    `name` VARCHAR(200) NOT NULL,
    `value` DECIMAL(20,5),
    PRIMARY KEY (`name`)
);

# -----------------------------------------------------------------------
# DataHubVersion
# -----------------------------------------------------------------------

CREATE TABLE `DataHubVersion`
(
    `version` VARCHAR(128) NOT NULL
);

INSERT INTO `DataHubVersion` values ('6.4.0');

