/*****************************************************************************
    Class:        DefaultReservationStrategy
    Copyright (c) 2015, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.oaa.commerce.services.reservation.strategy.impl;

import de.hybris.platform.core.model.order.AbstractOrderEntryModel;
import de.hybris.platform.core.model.order.AbstractOrderModel;

import org.apache.log4j.Logger;

import com.sap.retail.oaa.commerce.services.reservation.ReservationService;
import com.sap.retail.oaa.commerce.services.reservation.exception.ReservationException;
import com.sap.retail.oaa.commerce.services.reservation.jaxb.pojos.response.ReservationResponse;
import com.sap.retail.oaa.commerce.services.reservation.strategy.ReservationStrategy;
import com.sap.retail.oaa.commerce.services.rest.util.exception.CARBackendDownException;


/**
 * Default Implementation for ReservationStrategy
 */
public class DefaultReservationStrategy implements ReservationStrategy
{

	private static final Logger LOG = Logger.getLogger(DefaultReservationStrategy.class);

	private ReservationService reservationService;

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sap.retail.oaa.commerce.services.reservation.strategy.ReservationStrategy#updateReservation(de.hybris.platform
	 * .core.model.order.AbstractOrderModel, java.lang.String)
	 */
	@Override
	public ReservationResponse updateReservation(final AbstractOrderModel abstractOrderModel, final String reservationStatus)
	{
		LOG.info("Update Reservation: " + abstractOrderModel.getGuid());

		try
		{
			return getReservationService().updateReservation(abstractOrderModel, reservationStatus);
		}
		catch (final ReservationException e)
		{
			LOG.error("Could not update Reservation: " + abstractOrderModel.getGuid(), e);
		}
		catch (final CARBackendDownException e)
		{
			LOG.error(e.getMessage(), e);
		}

		return null;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sap.retail.oaa.commerce.services.reservation.strategy.ReservationStrategy#deleteReservation(de.hybris.platform
	 * .core.model.order.AbstractOrderModel)
	 */
	@Override
	public boolean deleteReservation(final AbstractOrderModel abstractOrderModel)
	{
		// No Reservation can be deleted return Successful
		if (!isOrderValidForDeletion(abstractOrderModel))
		{
			return true;
		}

		LOG.info("Delete Reservation: " + abstractOrderModel.getGuid());

		try
		{
			getReservationService().deleteReservation(abstractOrderModel);
		}
		catch (final ReservationException e)
		{
			LOG.error("Could not delete Reservation: " + abstractOrderModel.getGuid(), e);
			return false;
		}
		catch (final CARBackendDownException e)
		{
			LOG.error(e.getMessage(), e);
		}

		// In the default implementation it will return true - even when the CAR is not responding
		return true;
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see
	 * com.sap.retail.oaa.commerce.services.reservation.strategy.ReservationStrategy#deleteReservationItem(de.hybris.
	 * platform.core.model.order.AbstractOrderModel, de.hybris.platform.core.model.order.AbstractOrderEntryModel)
	 */
	@Override
	public boolean deleteReservationItem(final AbstractOrderModel abstractOrderModel,
			final AbstractOrderEntryModel abstractOrderEntryModel)
	{
		// No Reservation or entry can be deleted return Successful
		if (!isOrderValidForDeletion(abstractOrderModel) || !isOrderEntryValidForDeletion(abstractOrderEntryModel))
		{
			return true;
		}

		LOG.info("Delete Reservation Item: " + abstractOrderEntryModel.getEntryNumber().toString() + " for Reservation: "
				+ abstractOrderModel.getGuid());

		try
		{
			getReservationService().deleteReservationItem(abstractOrderModel, abstractOrderEntryModel);
		}
		catch (final ReservationException e)
		{
			LOG.error("Could not delete Reservation Item: " + abstractOrderEntryModel.getEntryNumber().toString()
					+ " for Reservation: " + abstractOrderModel.getGuid(), e);
			return false;
		}
		catch (final CARBackendDownException e)
		{
			LOG.error(e.getMessage(), e);
		}

		// In the default implementation it will return true - even when the CAR is not responding
		return true;
	}

	protected boolean isOrderEntryValidForDeletion(final AbstractOrderEntryModel abstractOrderEntryModel)
	{
		if (abstractOrderEntryModel == null || abstractOrderEntryModel.getEntryNumber() == null
				|| abstractOrderEntryModel.getSapCarReservation() == null
				|| abstractOrderEntryModel.getSapCarReservation() == Boolean.FALSE)
		{
			return false;
		}
		return true;
	}

	protected boolean isOrderValidForDeletion(final AbstractOrderModel abstractOrderModel)
	{
		// Check Order Model & GUID
		if (abstractOrderModel == null || abstractOrderModel.getGuid() == null || abstractOrderModel.getGuid().isEmpty())
		{
			return false;
		}
		// Check SAP CAR Reservation
		if (abstractOrderModel.getSapCarReservation() == null || abstractOrderModel.getSapCarReservation() == Boolean.FALSE)
		{
			return false;
		}
		return true;
	}

	/**
	 * @return the reservationService
	 */
	public ReservationService getReservationService()
	{
		return reservationService;
	}

	/**
	 * @param reservationService
	 *           the reservationService to set
	 */
	public void setReservationService(final ReservationService reservationService)
	{
		this.reservationService = reservationService;
	}
}
