/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2013 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 * 
 *  
 */
package com.sap.retail.oaa.commerce.facades.constants;

/**
 * Global class for all Sapoaacommercefacades constants. You can add global constants for your extension into this class.
 */
public final class SapoaacommercefacadesConstants extends GeneratedSapoaacommercefacadesConstants
{
	public static final String EXTENSIONNAME = "sapoaacommercefacades";

	private SapoaacommercefacadesConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
