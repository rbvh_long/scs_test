/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 21, 2017 12:54:25 PM                    ---
 * ----------------------------------------------------------------
 */
package com.sap.retail.oaa.backoffice.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedSapoaabackofficeConstants
{
	public static final String EXTENSIONNAME = "sapoaabackoffice";
	
	protected GeneratedSapoaabackofficeConstants()
	{
		// private constructor
	}
	
	
}
