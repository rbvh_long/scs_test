/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 21, 2017 12:54:25 PM                    ---
 * ----------------------------------------------------------------
 */
package com.sap.retail.oaa.orderexchange.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedSapoaaorderexchangeConstants
{
	public static final String EXTENSIONNAME = "sapoaaorderexchange";
	
	protected GeneratedSapoaaorderexchangeConstants()
	{
		// private constructor
	}
	
	
}
