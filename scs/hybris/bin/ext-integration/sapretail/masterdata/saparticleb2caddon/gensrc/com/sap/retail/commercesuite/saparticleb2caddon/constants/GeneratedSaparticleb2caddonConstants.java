/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 21, 2017 12:54:25 PM                    ---
 * ----------------------------------------------------------------
 */
package com.sap.retail.commercesuite.saparticleb2caddon.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedSaparticleb2caddonConstants
{
	public static final String EXTENSIONNAME = "saparticleb2caddon";
	public static class TC
	{
		public static final String ARTICLECOMPONENTTABPARAGRAPHCOMPONENT = "ArticleComponentTabParagraphComponent".intern();
	}
	
	protected GeneratedSaparticleb2caddonConstants()
	{
		// private constructor
	}
	
	
}
