/*****************************************************************************
    Class:        Saparticleb2caddonConstants.java
    Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 
 *****************************************************************************/
package com.sap.retail.commercesuite.saparticleb2caddon.constants;



@SuppressWarnings("PMD")
public class Saparticleb2caddonConstants extends GeneratedSaparticleb2caddonConstants
{
	public static final String EXTENSIONNAME = "saparticleb2caddon";

	private Saparticleb2caddonConstants()
	{
		//empty
	}


}
