/*****************************************************************************
    Class:        SaparticlebackofficeConstants.java
    Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 
*****************************************************************************/
package com.sap.retail.commercesuite.saparticlebackoffice.constants;

/**
 * Global class for all Ybackoffice constants. You can add global constants for your extension into this class.
 */
public final class SaparticlebackofficeConstants extends GeneratedSaparticlebackofficeConstants
{
	public static final String EXTENSIONNAME = "saparticlebackoffice";

	private SaparticlebackofficeConstants()
	{
		//private to avoid instantiating this constant class
	}

}
