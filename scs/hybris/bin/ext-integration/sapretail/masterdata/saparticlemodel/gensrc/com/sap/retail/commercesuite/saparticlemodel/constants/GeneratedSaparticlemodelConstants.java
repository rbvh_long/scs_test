/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 21, 2017 12:54:25 PM                    ---
 * ----------------------------------------------------------------
 */
package com.sap.retail.commercesuite.saparticlemodel.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedSaparticlemodelConstants
{
	public static final String EXTENSIONNAME = "saparticlemodel";
	public static class TC
	{
		public static final String ARTICLECOMPONENT = "ArticleComponent".intern();
		public static final String STRUCTUREDARTICLETYPE = "StructuredArticleType".intern();
	}
	public static class Attributes
	{
		public static class Product
		{
			public static final String COMPONENT = "component".intern();
			public static final String DISCOUNTABLE = "discountable".intern();
			public static final String STRUCTUREDARTICLETYPE = "structuredArticleType".intern();
		}
	}
	public static class Enumerations
	{
		public static class StructuredArticleType
		{
			public static final String DISPLAY = "DISPLAY".intern();
			public static final String SALES_SET = "SALES_SET".intern();
			public static final String PREPACK = "PREPACK".intern();
		}
	}
	public static class Relations
	{
		public static final String ARTICLECOMPONENTS = "ArticleComponents".intern();
	}
	
	protected GeneratedSaparticlemodelConstants()
	{
		// private constructor
	}
	
	
}
