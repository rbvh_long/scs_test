/*****************************************************************************
    Class:        SaparticlemodelConstants.java
    Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 
*****************************************************************************/
package com.sap.retail.commercesuite.saparticlemodel.constants;

/**
 * Global class for all saparticlemodel constants. You can add global constants for your extension into this class.
 */
public final class SaparticlemodelConstants extends GeneratedSaparticlemodelConstants
{
	/**
	 * Extension name.
	 */
	public static final String EXTENSIONNAME = "saparticlemodel";

	private SaparticlemodelConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
