/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 21, 2017 11:45:26 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sap.retail.isce.service.common.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedInstorecsserviceConstants
{
	public static final String EXTENSIONNAME = "instorecsservice";
	public static class Attributes
	{
		public static class AbstractOrderEntry
		{
			public static final String CREATEDINASMMODE = "createdInAsmMode".intern();
			public static final String EMPLOYEEID = "employeeId".intern();
		}
	}
	
	protected GeneratedInstorecsserviceConstants()
	{
		// private constructor
	}
	
	
}
