/*****************************************************************************
 Class:        DataContainerCombinedDefaultImpl
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/

package com.sap.retail.isce.container.impl;

import com.sap.retail.isce.container.DataContainerCombined;


/**
 * Default implementation for combined data container
 */
public abstract class DataContainerCombinedDefaultImpl extends DataContainerDefaultImpl implements DataContainerCombined
{
	// nothing common to be implemented here
}
