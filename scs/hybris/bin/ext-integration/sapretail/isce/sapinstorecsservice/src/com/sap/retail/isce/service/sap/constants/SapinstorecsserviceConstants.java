/*****************************************************************************
 Class:        SapinstorecsserviceConstants.java
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.service.sap.constants;

/**
 * Global class for all Sapinstorecsservice constants. You can add global constants for your extension into this class.
 */
public final class SapinstorecsserviceConstants extends GeneratedSapinstorecsserviceConstants
{
	public static final String EXTENSIONNAME = "sapinstorecsservice";

	public static final String PDF_MIME_TYPE = "application/pdf";

	private SapinstorecsserviceConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
