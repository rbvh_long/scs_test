/*****************************************************************************
 Class:        ISCEObjectScoresEditorUnitTest
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.cms.editor;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import de.hybris.bootstrap.annotations.UnitTest;
import de.hybris.platform.cockpit.model.editor.EditorListener;
import de.hybris.platform.cockpit.model.meta.PropertyDescriptor;
import de.hybris.platform.cockpit.zk.mock.DummyExecution;
import de.hybris.platform.cockpit.zk.mock.DummyWebApp;
import de.hybris.platform.servicelayer.session.SessionService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.context.ApplicationContext;
import org.zkoss.zk.ui.AbstractComponent;
import org.zkoss.zk.ui.HtmlBasedComponent;
import org.zkoss.zk.ui.Page;
import org.zkoss.zk.ui.impl.PageImpl;
import org.zkoss.zk.ui.metainfo.ComponentDefinitionMap;
import org.zkoss.zk.ui.metainfo.LanguageDefinition;
import org.zkoss.zk.ui.metainfo.PageDefinition;
import org.zkoss.zk.ui.metainfo.impl.ComponentDefinitionImpl;
import org.zkoss.zk.ui.sys.ExecutionsCtrl;
import org.zkoss.zul.Button;
import org.zkoss.zul.Div;
import org.zkoss.zul.Label;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listhead;
import org.zkoss.zul.Listheader;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Popup;
import org.zkoss.zul.Textbox;

import com.sap.retail.isce.container.impl.AllObjectScoresDataContainer;
import com.sap.retail.isce.service.DataContainerService;
import com.sap.retail.isce.service.sap.result.ISCEObjectScoreResult;
import com.sap.retail.isce.service.util.SpringUtil;


/**
 * Unit Test class for ISCEObjectScoresEditor
 */
@UnitTest
public class ISCEObjectScoresEditorUnitTest
{
	private class ISCEObjectScoresEditorTest extends ISCEObjectScoresEditor
	{
		private ApplicationContext applicationContext;

		@Override
		protected String getLocalizedString(final String resKey)
		{
			return resKey;
		}

		/**
		 * @param applicationContextMock
		 */
		public void setApplicationContext(final ApplicationContext applicationContext)
		{
			this.applicationContext = applicationContext;
		}

		@Override
		protected ApplicationContext getApplicationContext()
		{
			return applicationContext;
		}
	}

	private class ComponentDefinitionTest extends ComponentDefinitionImpl
	{

		public ComponentDefinitionTest(final LanguageDefinition langdef, final PageDefinition pgdef, final String name,
				final String clsnm)
		{
			super(langdef, pgdef, name, clsnm);
		}

		@Override
		public boolean hasMold(final String name)
		{
			return true;
		}
	}

	private class ISCEDummyExecution extends DummyExecution
	{
		ComponentDefinitionMap compDefMap = new ComponentDefinitionMap(true);
		private final PageImpl page;

		@Override
		public Page getCurrentPage()
		{
			return page;
		}

		public ISCEDummyExecution(final ApplicationContext applicationContext)
		{
			super(applicationContext);
			page = new PageImpl(new LanguageDefinition("xml", "a", "a", null, "a", "a", true, false, new DummyWebApp(
					applicationContextMock)), compDefMap, "", "");
		}
	}

	private class AllObjectScoresDataContainerTest extends AllObjectScoresDataContainer
	{
		/**
		 * @param yMktHttpDestination
		 */
		public AllObjectScoresDataContainerTest(final String yMktHttpDestination)
		{
			super();
		}

		/**
		 * Sets the unencoded list containing the object scores (description and weight).
		 *
		 * @param objectScoresList
		 *           the scores set.
		 */
		public void setUnencodedObjectScoresList(final List<ISCEObjectScoreResult> objectScoresList)
		{
			this.unencodedObjectScoresList = objectScoresList;
		}
	}

	private final AllObjectScoresDataContainerTest allObjectScores = new AllObjectScoresDataContainerTest(
			"ISCEHybrisMarketingHTTPDestination");

	private ISCEObjectScoresEditorTest classUnderTest;
	@Mock
	private EditorListener listenerSpy;
	@Mock
	private ApplicationContext applicationContextMock;
	@Mock
	private SessionService sessionServiceMock;
	@Mock
	private DataContainerService dataContainerServiceMock;
	@Mock
	private SpringUtil springUtilMock;

	@Before
	public void setUp()
	{
		classUnderTest = new ISCEObjectScoresEditorTest();
		MockitoAnnotations.initMocks(this);
		Mockito.doNothing().when(dataContainerServiceMock).readDataContainers(Arrays.asList(allObjectScores), null);
		Mockito.doReturn(dataContainerServiceMock).when(springUtilMock).getBean("defaultDataContainerService");
		Mockito.doReturn(allObjectScores).when(springUtilMock).getBean("allObjectScoresDataContainer");
		Mockito.doReturn(springUtilMock).when(applicationContextMock).getBean("springUtil");
		Mockito.doReturn(sessionServiceMock).when(applicationContextMock).getBean("sessionService");
		classUnderTest.setApplicationContext(applicationContextMock);
	}

	/**
	 * @param applicationContextMock2
	 */
	private void setApplicationContext(final ApplicationContext applicationContextMock2)
	{
		// YTODO Auto-generated method stub

	}

	protected void prepareZulRuntime()
	{
		final ISCEDummyExecution dummyExec = new ISCEDummyExecution(applicationContextMock);
		dummyExec.getCurrentPage().getComponentDefinitionMap()
				.add(new ComponentDefinitionTest(null, null, "org.zkoss.zul.Listbox", "org.zkoss.zul.Listbox"));
		dummyExec.getCurrentPage().getComponentDefinitionMap()
				.add(new ComponentDefinitionTest(null, null, "org.zkoss.zul.Paging", "org.zkoss.zul.Paging"));
		dummyExec.getCurrentPage().getComponentDefinitionMap()
				.add(new ComponentDefinitionTest(null, null, "org.zkoss.zul.Hbox", "org.zkoss.zul.Hbox"));

		ExecutionsCtrl.setCurrent(dummyExec);
	}

	@Test
	public void testCreateViewComponent()
	{
		// editable true - destination exception
		allObjectScores.setErrorState(Boolean.TRUE);
		HtmlBasedComponent result = classUnderTest.createViewComponent("1, 2", null, listenerSpy);
		assertEquals("createViewComponent - div must be returned", "org.zkoss.zul.Div", result.getClass().getName());
		AbstractComponent child = (AbstractComponent) result.getChildren().get(0);
		assertEquals("createViewComponent - div must contain textbox", "org.zkoss.zul.Textbox", child.getClass().getName());
		assertEquals("createViewComponent - textbox must contain initial value", "1, 2", ((Textbox) child).getValue());
		assertEquals("createViewComponent - div must contain 3 childs", 3, result.getChildren().size());
		assertEquals("createViewComponent - div must contain button", "org.zkoss.zul.Button", result.getChildren().get(1)
				.getClass().getName());
		child = (AbstractComponent) result.getChildren().get(2);
		assertEquals("createViewComponent - div must contain popup", "org.zkoss.zul.Popup", child.getClass().getName());
		AbstractComponent grandchild = (AbstractComponent) child.getChildren().get(0);
		assertEquals("createViewComponent - popup must contain Label", "org.zkoss.zul.Label", grandchild.getClass().getName());
		assertEquals("createViewComponent - Label must contain message 'type.CMSGenericScoreEditor.destinationError'",
				"type.CMSGenericScoreEditor.destinationError", ((Label) grandchild).getValue());

		// editable true - no scores
		allObjectScores.setErrorState(Boolean.FALSE);
		result = classUnderTest.createViewComponent("1, 2", null, listenerSpy);
		assertEquals("createViewComponent - div must be returned", "org.zkoss.zul.Div", result.getClass().getName());
		child = (AbstractComponent) result.getChildren().get(0);
		assertEquals("createViewComponent - div must contain textbox", "org.zkoss.zul.Textbox", child.getClass().getName());
		assertEquals("createViewComponent - textbox must contain initial value", "1, 2", ((Textbox) child).getValue());
		assertEquals("createViewComponent - div must contain 3 childs", 3, result.getChildren().size());
		assertEquals("createViewComponent - div must contain button", "org.zkoss.zul.Button", result.getChildren().get(1)
				.getClass().getName());
		child = (AbstractComponent) result.getChildren().get(2);
		assertEquals("createViewComponent - div must contain popup", "org.zkoss.zul.Popup", child.getClass().getName());
		grandchild = (AbstractComponent) child.getChildren().get(0);
		assertEquals("createViewComponent - popup must contain Label", "org.zkoss.zul.Label", grandchild.getClass().getName());
		assertEquals("createViewComponent - Label must contain message 'type.CMSGenericScoreEditor.noScores'",
				"type.CMSGenericScoreEditor.noScores", ((Label) grandchild).getValue());

		// editable true - with scores
		prepareZulRuntime();
		final List objectScoresList = new ArrayList<ISCEObjectScoreResult>();
		objectScoresList.add(new ISCEObjectScoreResult("id", "name", null));
		allObjectScores.setUnencodedObjectScoresList(objectScoresList);

		result = classUnderTest.createViewComponent("1, 2", null, listenerSpy);
		assertEquals("createViewComponent - div must be returned", "org.zkoss.zul.Div", result.getClass().getName());
		child = (AbstractComponent) result.getChildren().get(0);
		assertEquals("createViewComponent - div must contain textbox", "org.zkoss.zul.Textbox", child.getClass().getName());
		assertEquals("createViewComponent - textbox must contain initial value", "1, 2", ((Textbox) child).getValue());
		assertEquals("createViewComponent - div must contain button", "org.zkoss.zul.Button", result.getChildren().get(1)
				.getClass().getName());
		child = (AbstractComponent) result.getChildren().get(2);
		assertEquals("createViewComponent - div must contain popup", "org.zkoss.zul.Popup", child.getClass().getName());
		grandchild = (AbstractComponent) child.getChildren().get(0);
		assertEquals("createViewComponent - popup must contain Listbox", "org.zkoss.zul.Listbox", grandchild.getClass().getName());
		assertEquals("createViewComponent - div must contain 3 childs", 3, result.getChildren().size());

		// editable false
		classUnderTest.setEditable(false);
		result = classUnderTest.createViewComponent("1, 2", null, listenerSpy);
		assertEquals("createViewComponent - editable false - label must be returned", "org.zkoss.zul.Label", result.getClass()
				.getName());
		assertEquals("createViewComponent - editable false - label must contain initial value", "1, 2", ((Label) result).getValue());
	}

	@Test
	public void testReadAllObjectScores()
	{
		final AllObjectScoresDataContainer result = classUnderTest.readAllObjectScores();
		assertEquals("readAllObjectScores - must not be empty ", result, allObjectScores);
		Mockito.verify(dataContainerServiceMock, Mockito.times(1)).readDataContainers(Arrays.asList(allObjectScores), null);
	}

	@Test
	public void testAddValueToText()
	{
		final Textbox text = new Textbox();
		classUnderTest.addValueToText(text, "a", listenerSpy);
		assertEquals("addValueToText - text must be 'a'", "a", text.getText());
		assertEquals("addValueToText - value must be 'a'", "a", classUnderTest.getValue());
		Mockito.verify(listenerSpy, Mockito.times(1)).valueChanged("a");

		classUnderTest.addValueToText(text, "b", listenerSpy);
		assertEquals("addValueToText - text must be 'a, b'", "a, b", text.getText());
		assertEquals("addValueToText - value must be 'a, b'", "a, b", classUnderTest.getValue());
		Mockito.verify(listenerSpy, Mockito.times(1)).valueChanged("a, b");

		classUnderTest.addValueToText(text, "", listenerSpy);
		assertEquals("addValueToText - text must be 'a, b'", "a, b", text.getText());
		assertEquals("addValueToText - value must be 'a, b'", "a, b", classUnderTest.getValue());
		Mockito.verify(listenerSpy, Mockito.times(1)).valueChanged("a, b");
	}

	@Test
	public void testGetModelContentObjectScores()
	{
		List<String[]> model = classUnderTest.getModelContentObjectScores(null);
		assertTrue("getModelContentObjectScores - modell content must be empty ", model.isEmpty());

		final AllObjectScoresDataContainerTest allObjectScores = new AllObjectScoresDataContainerTest(
				"ISCEHybrisMarketingHTTPDestination");

		model = classUnderTest.getModelContentObjectScores(allObjectScores);
		assertTrue("getModelContentObjectScores - modell content must be empty ", model.isEmpty());

		final List objectScoresList = new ArrayList<ISCEObjectScoreResult>();
		objectScoresList.add(new ISCEObjectScoreResult("id", "name", null));

		allObjectScores.setUnencodedObjectScoresList(objectScoresList);

		model = classUnderTest.getModelContentObjectScores(allObjectScores);
		assertEquals("getModelContentObjectScores - modell should contain 1 entry ", 1, model.size());

		final String[] content = model.get(0);
		assertEquals("getModelContentObjectScores - entry first part should be 'name'", "name", content[0]);
		assertEquals("getModelContentObjectScores - entry second part should be 'id'", "id", content[1]);
	}

	@Test
	public void testCreateListBox()
	{
		prepareZulRuntime();

		final List<String[]> modelContent = new ArrayList();

		final String[] val1 =
		{ "wert1", "a" };
		modelContent.add(val1);
		final String[] val2 =
		{ "wert2", "b" };
		modelContent.add(val2);

		final Listbox listbox = classUnderTest.createListBox(listenerSpy, new Listhead(), modelContent);

		assertEquals("createListBox - listbox model size must be identical to model content", modelContent.size(), listbox
				.getModel().getSize());
		assertEquals("createListBox - listbox must be peagable", "paging", listbox.getMold());
		assertEquals("createListBox - listbox must have 10 entries per page", 10, listbox.getPageSize());
		assertEquals("createListBox - listbox must have head", "org.zkoss.zul.Listhead", listbox.getChildren().get(0).getClass()
				.getName());
		assertNotNull("createListBox - listbox must have an item Renderer", listbox.getItemRenderer());
	}

	@Test
	public void testDefineListItem()
	{
		final String[] data =
		{ "wert1", "a" };

		final Listitem listitem = new Listitem();

		classUnderTest.defineListItem(listitem, data, listenerSpy);

		assertEquals("defineListItem - item label should be 'wert1'", "wert1", listitem.getLabel());
		assertEquals("defineListItem - item value should be 'a'", "a", listitem.getValue());
		assertEquals("defineListItem - item must have Listcell", "org.zkoss.zul.Listcell", listitem.getChildren().get(0).getClass()
				.getName());
		final Iterator onClickHandler = listitem.getListenerIterator("onClick");
		assertNotNull(onClickHandler.next());
	}

	@Test
	public void testCreateAddScoreButton()
	{
		final Popup addScorePopup = new Popup();

		final Button button = classUnderTest.createAddScoreButton(new Div(), addScorePopup);
		assertEquals("CreateAddScoreButton - label must be '+'", "+", button.getLabel());

		final Iterator onClickHandler = button.getListenerIterator("onClick");
		assertNotNull(onClickHandler.next());
	}

	@Test
	public void testCreateTextbox()
	{
		Textbox text = classUnderTest.createTextBox("", listenerSpy);
		assertEquals("createTextbox - text must be ''", "", text.getText());
		text = classUnderTest.createTextBox("a, b, c", listenerSpy);
		assertEquals("createTextbox - text must be 'a, b, c'", "a, b, c", text.getText());

		final Iterator onChangeHandler = text.getListenerIterator("onChange");
		assertNotNull(onChangeHandler.next());
	}

	@Test
	public void testCreateListHead()
	{
		final Listhead listHead = classUnderTest.createListHead();
		assertNotNull(listHead);
		assertEquals("createListHead - must conatin to header entries", 2, listHead.getChildren().size());

		assertEquals("createListHead - child 1 must have label 'type.CMSGenericScoreEditor.scoreName'",
				"type.CMSGenericScoreEditor.scoreName", ((Listheader) listHead.getChildren().get(0)).getLabel());
	}

	@Test
	public void testIsInline()
	{
		assertTrue("IsInline - must be true", classUnderTest.isInline());
	}

	@Test
	public void testIsOptional()
	{
		assertFalse("IsOptional - must be false", classUnderTest.isOptional());
	}

	@Test
	public void testGetEditorType()
	{
		assertEquals("getEditorType - must be Text", PropertyDescriptor.TEXT, classUnderTest.getEditorType());
	}

}
