/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 21, 2017 11:45:26 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sap.retail.isce.facade.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedInstorecsfacadeConstants
{
	public static final String EXTENSIONNAME = "instorecsfacade";
	
	protected GeneratedInstorecsfacadeConstants()
	{
		// private constructor
	}
	
	
}
