/*****************************************************************************
 Class:        InstorecsbackofficeConstants
 Copyright (c) 2016, SAP SE, Germany, All rights reserved.
 *****************************************************************************/
package com.sap.retail.isce.constants;


/**
 * Global class for all InstorecsbackofficeConstants constants. You can add global constants for your extension into
 * this class.
 */
public final class InstorecsbackofficeConstants extends GeneratedInstorecsbackofficeConstants
{
	public static final String EXTENSIONNAME = "instorecsbackoffice";

	private InstorecsbackofficeConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
