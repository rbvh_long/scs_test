/*
 * ----------------------------------------------------------------
 * --- WARNING: THIS FILE IS GENERATED AND WILL BE OVERWRITTEN! ---
 * --- Generated at Dec 21, 2017 11:45:26 AM                    ---
 * ----------------------------------------------------------------
 */
package com.sap.retail.isce.constants;

/**
 * @deprecated since ages - use constants in Model classes instead
 */
@Deprecated
@SuppressWarnings({"unused","cast","PMD"})
public class GeneratedInstorecsbackofficeConstants
{
	public static final String EXTENSIONNAME = "instorecsbackoffice";
	public static class Attributes
	{
		public static class SAPConfiguration
		{
			public static final String INSTORECSASSISTEDSERVICESTOREFRONT_CAR_CLIENT = "instorecsassistedservicestorefront_CAR_client".intern();
			public static final String INSTORECSASSISTEDSERVICESTOREFRONT_CAR_HTTPDESTINATION = "instorecsassistedservicestorefront_CAR_HTTPDestination".intern();
			public static final String INSTORECSASSISTEDSERVICESTOREFRONT_CAR_ONLINECHANNELLIST = "instorecsassistedservicestorefront_CAR_onlineChannelList".intern();
			public static final String INSTORECSASSISTEDSERVICESTOREFRONT_CAR_POSCHANNELLIST = "instorecsassistedservicestorefront_CAR_posChannelList".intern();
			public static final String INSTORECSASSISTEDSERVICESTOREFRONT_CAR_PURCHASEHISTORYCUSTOMERORDERS_MAXNUMBEROFTRANSACTIONS = "instorecsassistedservicestorefront_CAR_purchaseHistoryCustomerOrders_maxNumberofTransactions".intern();
			public static final String INSTORECSASSISTEDSERVICESTOREFRONT_CAR_SERVICENAME = "instorecsassistedservicestorefront_CAR_serviceName".intern();
			public static final String INSTORECSASSISTEDSERVICESTOREFRONT_YMKT_HTTPDESTINATION = "instorecsassistedservicestorefront_yMkt_HTTPDestination".intern();
		}
	}
	
	protected GeneratedInstorecsbackofficeConstants()
	{
		// private constructor
	}
	
	
}
