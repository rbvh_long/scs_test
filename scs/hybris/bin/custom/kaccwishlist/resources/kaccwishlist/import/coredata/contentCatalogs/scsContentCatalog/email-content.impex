# ImpEx for Importing Email Content

# Macros / Replacement Parameter definitions
$version=Staged
$contentCatalog=scsContentCatalog
$contentCV=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalog]),CatalogVersion.version[default=$version])[default=$contentCatalog:$version]
$wideContent=CMSImageComponent,BannerComponent,CMSParagraphComponent

# Language
$lang=en

#$picture=media(code, $contentCV);

$jarResourceCms=jar:com.keyrus.accelerator.kaccwishlist.core.setup.WishlistSystemSetup&/kaccwishlist/import/cockpits/cmscockpit
$emailResource=jar:com.keyrus.accelerator.kaccwishlist.core.setup.WishlistSystemSetup&/kaccwishlist/import/emails
$emailPackageName=com.keyrus.accelerator.kaccwishlist.facades.process.email.context

# Import modulegen config properties into impex macros
#UPDATE GenericItem[processor=de.hybris.platform.commerceservices.impex.impl.ConfigPropertyImportProcessor];pk[unique=true]
#$jarResourceCms=$config-jarResourceCmsValue
#$emailResource=$config-emailResourceValue
#$emailPackageName=$config-emailContextPackageName


# CMS components and Email velocity templates
INSERT_UPDATE RendererTemplate;code[unique=true];contextClass;rendererType(code)[default='velocity'];content[lang=$lang]
# ;kacc-CMSImageComponent-template;java.util.Map;""
# ;kacc-BannerComponent-template;java.util.Map;""
# ;kacc-CMSLinkComponent-template;java.util.Map;""
# ;kacc-CMSParagraphComponent-template;java.util.Map;""

UPDATE RendererTemplate;code[unique=true];templateScript[lang=$lang,translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
 # ;kacc-BannerComponent-template;$emailResource/email-bannerComponentTemplate.vm
 # ;kacc-CMSImageComponent-template;$emailResource/email-cmsImageComponentTemplate.vm
 # ;kacc-CMSLinkComponent-template;$emailResource/email-cmsLinkComponentTemplate.vm
 # ;kacc-CMSParagraphComponent-template;$emailResource/email-cmsParagraphComponentTemplate.vm
 
 # ;kacc_Email_Send_Wishlist_Subject;$emailResource/email-sendWishlistSubject.vm
 # ;kacc_Email_Send_Wishlist_Body;$emailResource/email-sendWishlistBody.vm
 
 # Media
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];realfilename;@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator];mime[default='image/jpeg'];folder(qualifier)[default='images']
# ;;/images/email/keyrus-logo.png;keyrus-logo.png;$emailResource/keyrus-logo.png;
 
# CMS Image Components
INSERT_UPDATE CMSImageComponent;$contentCV[unique=true];uid[unique=true];name
# ;;EmailSiteLogoImage;Email Site Logo Image;

UPDATE CMSImageComponent;$contentCV[unique=true];uid[unique=true];media(code, $contentCV)[lang=$lang]
# ;;EmailSiteLogoImage;/images/theme/logo-hybris.png

# CMS Paragraph Components
INSERT_UPDATE CMSParagraphComponent;$contentCV[unique=true];uid[unique=true];content[lang=$lang]
# ;;TopHrefParagraph;"<font color=""#FFFFFF"" size=""2"" face=""Arial, Helvetica, sans-serif""> <a href=""#"" target=""_blank"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #1e6583; text-decoration: none;""></a>";
# ;;BottomHrefParagraph;"<a href=""#"" target=""_blank"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #1e6583; text-decoration: none;""></a> | <a href=""http://www.hybris.com/fr/"" target=""_blank"" style=""font-family: Arial, Helvetica, sans-serif; font-size: 14px; color: #1e6583; text-decoration: none;"">CGV </a>";

# Content Slots
INSERT_UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];name;active
# ;;EmailTopSlot;Default Email Top Slot;true
# ;;EmailBottomSlot;Default Email Bottom Slot;true
# ;;EmailSiteLogoSlot;Default Email Site Slot;true

UPDATE ContentSlot;$contentCV[unique=true];uid[unique=true];cmsComponents(uid,$contentCV)
# ;;EmailTopSlot;
# ;;EmailBottomSlot;
# ;;EmailSiteLogoSlot;EmailSiteLogoImage

# Email page Template
INSERT_UPDATE EmailPageTemplate;$contentCV[unique=true];uid[unique=true];name;active;frontendTemplateName;subject(code);htmlTemplate(code);restrictedPageTypes(code)
# ;;SendWishlistEmailTemplate;Send WishList Email Template;true;sendWishlistEmail;kacc_Email_Send_Wishlist_Subject;kacc_Email_Send_Wishlist_Body;EmailPage

# Templates for CMS Cockpit Page Edit
UPDATE EmailPageTemplate;$contentCV[unique=true];uid[unique=true];velocityTemplate[translator=de.hybris.platform.commerceservices.impex.impl.FileLoaderValueTranslator]
# ;;SendWishlistEmailTemplate;$jarResourceCms/structure-view/structure_WishlistEmailTemplate.vm

INSERT_UPDATE ContentSlotName;name[unique=true];template(uid,$contentCV)[unique=true][default='SendWishlistEmailTemplate'];validComponentTypes(code)
# ;SiteLogo;;;logo
# ;TopContent;;$wideContent;
# ;BottomContent;;$wideContent;

# Bind Content Slots to Email Page Templates
INSERT_UPDATE ContentSlotForTemplate;$contentCV[unique=true];uid[unique=true];position[unique=true];pageTemplate(uid,$contentCV)[unique=true][default='SendWishlistEmailTemplate'];contentSlot(uid,$contentCV)[unique=true];allowOverwrite
# ;;SiteLogo-SendWishlistEmail;SiteLogo;;EmailSiteLogoSlot;true
# ;;TopContent-SendWishlistEmail;TopContent;;EmailTopSlot;true
# ;;BottomContent-SendWishlistEmail;BottomContent;;EmailBottomSlot;true

# Wishlist Email Page
INSERT_UPDATE EmailPage;$contentCV[unique=true];uid[unique=true];name;masterTemplate(uid,$contentCV);defaultPage;approvalStatus(code)[default='approved'];fromEmail[lang=$lang];fromName[lang=$lang]
# ;;SendWishlistEmail;Send Wishlist Email;SendWishlistEmailTemplate;true;;customerservices@hybris.com;Customer Services Team


# Email velocity templates
INSERT_UPDATE RendererTemplate;code[unique=true];contextClass;rendererType(code)[default='velocity']
# ;kacc_Email_Send_Wishlist_Body;$emailPackageName.SendWishlistEmailContext
# ;kacc_Email_Send_Wishlist_Subject;$emailPackageName.SendWishlistEmailContext

# Preview Image for use in the CMS Cockpit
INSERT_UPDATE Media;$contentCV[unique=true];code[unique=true];mime;realfilename;@media[translator=de.hybris.platform.impex.jalo.media.MediaDataTranslator][forceWrite=true]
# ;;EmailPageModel_preview;text/gif;EmailPageModel_preview.gif;$jarResourceCms/preview-images/EmailPageModel_preview.gif

UPDATE EmailPage;$contentCV[unique=true];uid[unique=true];previewImage(code, $contentCV)
# ;;SendWishlistEmail;EmailPageModel_preview
