package com.keyrus.accelerator.kaccwishlist.custom.form;

/**
 * @author Islam.Elghaoui
 * 
 */
public class AddWishListForm
{
	private String wishListName;

	/**
	 * @return the wishListName
	 */
	public String getWishListName()
	{
		return wishListName;
	}


	/**
	 * @param wishListName
	 *           the wishListName to set
	 */
	public void setWishListName(final String wishListName)
	{
		this.wishListName = wishListName;
	}
}
