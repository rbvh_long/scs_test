package com.keyrus.accelerator.kaccwishlist.controllers.pages;

import de.hybris.platform.acceleratorstorefrontcommons.annotations.RequireHardLogIn;
import de.hybris.platform.acceleratorstorefrontcommons.breadcrumb.impl.DefaultResourceBreadcrumbBuilder;
import de.hybris.platform.acceleratorstorefrontcommons.constants.WebConstants;
import de.hybris.platform.acceleratorstorefrontcommons.controllers.util.GlobalMessages;
import de.hybris.platform.addonsupport.controllers.page.AbstractAddOnPageController;
import de.hybris.platform.catalog.CatalogVersionService;
import de.hybris.platform.catalog.model.CatalogVersionModel;
import de.hybris.platform.cms2.exceptions.CMSItemNotFoundException;
import de.hybris.platform.commercefacades.customer.CustomerFacade;
import de.hybris.platform.commercefacades.order.CartFacade;
import de.hybris.platform.commercefacades.order.data.CartData;
import de.hybris.platform.commercefacades.order.data.OrderEntryData;
import de.hybris.platform.commercefacades.product.ProductOption;
import de.hybris.platform.commercefacades.product.data.ProductData;
import de.hybris.platform.commercefacades.user.data.CustomerData;
import de.hybris.platform.core.Constants.USER;
import de.hybris.platform.core.model.product.ProductModel;
import de.hybris.platform.core.model.user.CustomerModel;
import de.hybris.platform.core.model.user.UserModel;
import de.hybris.platform.product.ProductService;
import de.hybris.platform.servicelayer.event.EventService;
import de.hybris.platform.servicelayer.exceptions.UnknownIdentifierException;
import de.hybris.platform.servicelayer.i18n.CommonI18NService;
import de.hybris.platform.servicelayer.session.SessionService;
import de.hybris.platform.servicelayer.user.UserService;
import de.hybris.platform.site.BaseSiteService;
import de.hybris.platform.store.services.BaseStoreService;
import de.hybris.platform.util.localization.Localization;
import de.hybris.platform.wishlist2.model.Wishlist2Model;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.keyrus.accelerator.kaccwishlist.controllers.KaccwishlistControllerConstants;
import com.keyrus.accelerator.kaccwishlist.core.exceptions.KaccWishlistException;
import com.keyrus.accelerator.kaccwishlist.custom.form.ShareWishlistForm;
import com.keyrus.accelerator.kaccwishlist.facades.KaccProductFacade;
import com.keyrus.accelerator.kaccwishlist.facades.KaccWishlistFacade;
import com.keyrus.accelerator.kaccwishlist.facades.KaccWishlistResult;
import com.keyrus.accelerator.kaccwishlist.utils.WishListFormatterUtils;
import com.keyrus.accelerator.utils.emailmanagement.events.wishlist.CustomKeyrusWishlistEmailSenderEvent;
import com.keyrus.accelerator.wishlist.data.WishlistData;
import com.keyrus.accelerator.wishlist.data.WishlistEntryData;


/**
 * The Class KaccWishListController.
 *
 * @author mohamed.othmani
 */
@Controller
public class KaccWishListController extends AbstractAddOnPageController
{

	/** The log. */
	private static final Logger LOG = LogManager.getLogger(KaccWishListController.class);

	private static final String TEXT_ACCOUNT_WISHLIST = "text.account.wishlist";
	private static final String TEXT_ACCOUNT_WISHLIST_DETAILS = "text.account.wishlistDetails";
	private static final String MY_ACCOUNT_WISHLIST_URL = "/my-account/wishlist";

	private static final String REDIRECT_WISHLIST = "redirect:/my-account/wishlist";
	private static final String REDIRECT_WISHLIST_PARAM = "redirect:/my-account/wishlist/detail?wishlistParam=";
	private static final String KEY_ERROR = "wishlist.error.remove.message";
	private static final String KEY_SUCCESS_ENTRY = "wishlist.success.remove.entry.message";
	private static final String KEY_SUCCESS_WISHLIST = "wishlist.success.remove.message";

	/** The Constant GUEST_WISHLIST_URL. */
	private static final String GUEST_WISHLIST_URL = REDIRECT_PREFIX + "/guest/wishlist";

	/** The wishlist cms page. */
	private static final String WISHLIST_CMS_PAGE = "wishlist-view";
	private static final String WISHLIST_DETAIL_CMS_PAGE = "wishlist-detail-view";

	/** The wishlist for visitor cms page. */
	private static final String WISHLIST_LAYOUT_PAGE = "wishlist-visitor";

	private static final String ONLINE_CATALOG_VERSION_NAME = "Online";

	private static final String CATALOG_ID = "scsProductCatalog";
	
	private static final String WISHLISTNAME = "wishlistName";
	
	private static final String RESULT = "result";

	/** The user service. */
	@Autowired
	private UserService userService;

	/** The kacc wishlist facade. */
	@Autowired
	private KaccWishlistFacade kaccWishlistFacade;

	/** The kacc product facade. */
	@Autowired
	private KaccProductFacade productFacade;

	@Resource(name = "productService")
	private ProductService productService;

	@Resource
	private CatalogVersionService catalogVersionService;

	@Autowired
	private CustomerFacade customerFacade;

	/** The event service. */
	@Resource
	private EventService eventService;

	@Resource
	private CommonI18NService commonI18NService;

	@Resource(name = "baseSiteService")
	private BaseSiteService baseSite;

	/** The base store. */
	@Resource
	private BaseStoreService baseStore;

	/** Cart Facade **/
	@Resource
	private CartFacade cartFacade;

	@Resource(name = "accountBreadcrumbBuilder")
	private DefaultResourceBreadcrumbBuilder breadcrumbBuilder;

	@Resource(name = "sessionService")
	private SessionService sessionService;

	private void configurePopup(final Model model)
	{
		final UserModel userModel = userService.getCurrentUser();
		final String userUID = userModel.getUid();
		if (!USER.ANONYMOUS_CUSTOMER.equals(userUID))
		{
			final List<WishlistData> wishlistDatas = kaccWishlistFacade.getLastModifiedWishlists(userModel);
			if (CollectionUtils.isNotEmpty(wishlistDatas))
			{
				model.addAttribute("wishlists", wishlistDatas);
			}
			model.addAttribute("isConnected", Boolean.TRUE);
		}
		else
		{
			model.addAttribute("isConnected", Boolean.FALSE);
		}
	}

	/**
	 * Gets the wishlist popup.
	 *
	 * @param code
	 *
	 * @param model
	 *
	 * @return the wishlist popup
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/wishlist/addCartToList")
	public String addCartToList(final Model model)
	{
		configurePopup(model);
		model.addAttribute("listFromCart", Boolean.TRUE);
		return KaccwishlistControllerConstants.Views.Pages.Popins.AddProductWishListPopup;
	}

	/**
	 * Gets the wishlist popup.
	 *
	 * @param code
	 *
	 * @param model
	 *
	 * @return the wishlist popup
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/wishlist/addProductToWishlistPopup/{code}")
	public String getWishlistPopup(@PathVariable final String code, final Model model)
	{
		configurePopup(model);
		model.addAttribute("listFromCart", Boolean.FALSE);
		model.addAttribute("productCode", code);
		return KaccwishlistControllerConstants.Views.Pages.Popins.AddProductWishListPopup;
	}

	/**
	 * Adds the product to wishlist popup.
	 *
	 * @param productCode
	 *           the product code
	 * @param wishlistName
	 *           the wishlist name
	 * @return the kacc wishlist result
	 */
	@RequestMapping(value = "/wishlist/addProductWishlist", method = RequestMethod.POST)
	public String addProductToWishlistPopup(@RequestParam("productCode") final String productCode,
			@RequestParam(value = "quantity", required = false) final String quantity,
			@RequestParam("wishlistName") final String wishlistName,
			@RequestParam(value = "listFromCart", required = false) final boolean listFromCart,
			@RequestParam(value = "configProduct", required = false) final String configProduct, final Model model)
	{

		final String userId = userService.getCurrentUser().getUid();
		// if the adding is the whole cart or just one product
		if (listFromCart)
		{
			final CartData cart = cartFacade.getSessionCart();
			KaccWishlistResult result = null;
			for (final OrderEntryData entry : cart.getEntries())
			{
				result = addNewEntry(userId, entry.getProduct().getCode(), String.valueOf(entry.getQuantity()),
						wishlistName, configProduct, false);
			}
			model.addAttribute(RESULT, result);
		}
		else
		{
			final KaccWishlistResult result = addNewEntry(userId, productCode, quantity, wishlistName, configProduct, false);

			if (result !=null && !result.isError())
			{
				final ProductData productData = productFacade.getProductForCode(productCode);
				model.addAttribute(WISHLISTNAME, wishlistName);
				model.addAttribute("product", productData);
			}
			model.addAttribute(RESULT, result);
		}
		return KaccwishlistControllerConstants.Views.Fragments.product.AddProductWishListResult;
	}

	/**
	 * Method that does the assignment of data before adding the product to one list
	 *
	 * @param userId
	 * @param productCode
	 * @param quantity
	 * @param wishlistName
	 * @param newList
	 * @return the Result KaccWishlistResult
	 */
	private KaccWishlistResult addNewEntry(final String userId, final String productCode, final String quantity,
			final String wishlistName, final String configProduct, final boolean newList)
	{
		KaccWishlistResult result = null;
		try
		{

			if (StringUtils.isNotEmpty(userId) && StringUtils.isNotEmpty(productCode) && StringUtils.isNotEmpty(quantity))
			{
				final ProductData product = new ProductData();
				product.setCode(productCode);
				final WishlistEntryData entryData = new WishlistEntryData();
				entryData.setProduct(product);
				entryData.setQuantity(NumberUtils.toDouble(quantity, 1));
				if (StringUtils.isNotBlank(configProduct))
				{
					entryData.setConfProduct(configProduct);
				}
				if (newList)
				{
					result = kaccWishlistFacade.addEntryToNewWishlist(entryData, wishlistName, userId);
				}
				else
				{
					result = kaccWishlistFacade.addWishlistEntry(entryData, wishlistName, userId);
				}
			}
		}
		catch (final KaccWishlistException e)
		{
			if (LOG.isWarnEnabled())
			{
				LOG.warn("Error adding product to wishlist", e);
			}
			result = new KaccWishlistResult(true, e.getMessage());
		}
		return result;
	}

	/**
	 * Adds the new wishlist popup.
	 *
	 * @param productCode
	 *           the product code
	 * @param wishlistName
	 *           the wishlist name
	 * @return the kacc wishlist result
	 */
	@RequestMapping(value = "/wishlist/addNewWishlist", method = RequestMethod.POST)
	public String addNewWishlistPopup(@RequestParam(value = "productCode", required = true) final String productCode,
			@RequestParam(value = "quantity", required = false) final String quantity,
			@RequestParam(value = "wishlistName", required = true) final String wishlistName,
			@RequestParam(value = "listFromCart", required = false) final boolean listFromCart,
			@RequestParam(value = "configProduct", required = false) final String configProduct, final Model model)
	{
		KaccWishlistResult result;
		final UserModel user = userService.getCurrentUser();
		if (listFromCart)
		{
			result = kaccWishlistFacade.createEmptyWishlist(user, wishlistName, StringUtils.EMPTY);

			if (!result.isError())
			{
				final CartData cart = cartFacade.getSessionCart();
				for (final OrderEntryData entry : cart.getEntries())
				{
					result = addNewEntry(user.getUid(), entry.getProduct().getCode(),
							String.valueOf(entry.getQuantity()), wishlistName, configProduct, false);
				}
			}
		}
		else
		{

			result = addNewEntry(user.getUid(), productCode, quantity, wishlistName, configProduct, true);

			if (!result.isError())
			{
				final ProductData productData = productFacade.getProductForCode(productCode);
				model.addAttribute("product", productData);
			}
		}
		model.addAttribute(WISHLISTNAME, wishlistName);
		model.addAttribute(RESULT, result);
		return KaccwishlistControllerConstants.Views.Fragments.product.AddProductWishListResult;
	}

	/**
	 *
	 * @param productCode
	 * @param wishlistName
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/wishlist/askremove", method = RequestMethod.GET)
	@RequireHardLogIn
	public String askRemoveWishlistEntry(@RequestParam(value = "productCode", required = true) final String productCode,
			@RequestParam(value = "wishlistName", required = true) final String wishlistName, final Model model)
	{
		model.addAttribute("productCode", productCode);
		model.addAttribute(WISHLISTNAME, wishlistName);
		return KaccwishlistControllerConstants.Views.Fragments.product.RemoveWishlistEntry;
	}

	/**
	 * @param productCode
	 * @param wishlistName
	 * @param redirectModel
	 * @return
	 */
	@RequestMapping(value = "/wishlist/removeEntry", method = RequestMethod.GET)
	@RequireHardLogIn
	public String removeWishlistEntry(@RequestParam(value = "productCode") final String productCode,
			@RequestParam(value = "wishlistName") final String wishlistName, final RedirectAttributes redirectModel)
	{
		if (StringUtils.isNotBlank(productCode) && StringUtils.isNotBlank(wishlistName) && !kaccWishlistFacade
				.removeWishlistEntry(productCode, wishlistName, userService.getCurrentUser().getUid()).isError())
		{
			final ProductData productData = productFacade.getProductForCode(productCode);
			final Object[] arguments =
			{ wishlistName, productData.getName() };
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					Localization.getLocalizedString(KEY_SUCCESS_ENTRY, arguments));
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, KEY_ERROR);
		}

		return REDIRECT_WISHLIST_PARAM + wishlistName;
	}

	/**
	 * @param wishlistName
	 * @param redirectModel
	 * @return
	 */
	@RequestMapping(value = "/wishlist/remove", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public boolean removeWishlist(@RequestParam(value = "wishlistName") final String wishlistName,
			final RedirectAttributes redirectModel)
	{

		if (StringUtils.isNotBlank(wishlistName)
				&& !kaccWishlistFacade.removeWishlist(wishlistName, userService.getCurrentUser().getUid()).isError())
		{

			final Object[] arguments =
			{ wishlistName };
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.CONF_MESSAGES_HOLDER,
					Localization.getLocalizedString(KEY_SUCCESS_WISHLIST, arguments));
		}
		else
		{
			GlobalMessages.addFlashMessage(redirectModel, GlobalMessages.ERROR_MESSAGES_HOLDER, KEY_ERROR);
		}

		return Boolean.TRUE;
	}

	/**
	 * Visualisation of wish and product.
	 *
	 * @param model
	 *           the model
	 * @param name
	 *           the name
	 * @return the string
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/my-account/wishlist", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getWishlistByNameForUser(final Model model,
			@RequestParam(value = "wishlistParam", required = false) final String wishlistParam)
			throws de.hybris.platform.cms2.exceptions.CMSItemNotFoundException
	{
		final UserModel user = userService.getCurrentUser();
		final String userUid = user.getUid();

		if (!USER.ANONYMOUS_CUSTOMER.equals(userUid))
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Getting " + wishlistParam + " wishlist for User with UID : " + userUid);
			}
			final List<WishlistData> wishlists = kaccWishlistFacade.getWishlists(user);
			if (CollectionUtils.isNotEmpty(wishlists))
			{
				model.addAttribute("wishlistDatas", wishlists);
			}

			storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_CMS_PAGE));
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					breadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_WISHLIST));
			return getViewForPage(model);
		}
		else
		{
			return REDIRECT_PREFIX + GUEST_WISHLIST_URL;
		}
	}

	@RequestMapping(value = "/my-account/wishlist/detail", method = RequestMethod.GET)
	@RequireHardLogIn
	public String getWishlistDetailByNameForUser(final Model model,
			@RequestParam(value = "wishlistParam", required = false) final String wishlistParam)
			throws de.hybris.platform.cms2.exceptions.CMSItemNotFoundException
	{
		final UserModel user = userService.getCurrentUser();
		final String userUid = user.getUid();

		if (!USER.ANONYMOUS_CUSTOMER.equals(userUid))
		{
			if (LOG.isDebugEnabled())
			{
				LOG.debug("Getting " + wishlistParam + " wishlist for User with UID : " + userUid);
			}

			model.addAttribute("selectedWishList", wishlistParam);

			final Wishlist2Model wishlist2Model = kaccWishlistFacade.getWishListForCurrentUser(wishlistParam, userUid);

			// Accenture shopping list. Set isDefault at false => disable the
			// link on the account page
			if (wishlist2Model != null && Boolean.TRUE.equals(wishlist2Model.getDefault()))
			{
				wishlist2Model.setDefault(Boolean.FALSE);
				kaccWishlistFacade.updateWishlist(wishlist2Model);
			}

			final WishlistData selectedWishListData = kaccWishlistFacade.getWishListData(wishlist2Model);
			final List<WishlistEntryData> wishlistDataEntries = selectedWishListData.getEntries();
			final List<WishlistEntryData> wishlistEntries = isProductInWishList(wishlistDataEntries);
			if (CollectionUtils.isNotEmpty(wishlistEntries))
			{
				populateClassif(wishlistEntries);
				model.addAttribute("wishlistEntries", wishlistEntries);
				populateEntrieslists(wishlistEntries, model);
			}

			storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_DETAIL_CMS_PAGE));
			breadcrumbBuilder.setParentBreadcrumbResourceKey(TEXT_ACCOUNT_WISHLIST);
			breadcrumbBuilder.setParentBreadcrumbLinkPath(MY_ACCOUNT_WISHLIST_URL);
			model.addAttribute(WebConstants.BREADCRUMBS_KEY,
					breadcrumbBuilder.getBreadcrumbs(TEXT_ACCOUNT_WISHLIST_DETAILS));
			model.addAttribute("nonSelectableProductsSkus", getNonSelectableProductsSkus(selectedWishListData));

			return getViewForPage(model);
		}
		else
		{
			return REDIRECT_PREFIX + GUEST_WISHLIST_URL;
		}
	}

	/**
	 * Gets the wish list by name for guest.
	 *
	 * @param model
	 *           the model
	 * @param name
	 *           the name
	 * @return the wish list by name of guest
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/guest/wishlist", method = RequestMethod.GET)
	public String getWishlistByNameForGuest(final Model model,
			@RequestParam(value = "name", defaultValue = "", required = true) final String name,
			@RequestParam(value = "user", defaultValue = "", required = true) final String userUid)
			throws de.hybris.platform.cms2.exceptions.CMSItemNotFoundException
	{
		if (!StringUtils.isEmpty(name))
		{
			final WishlistData wishData = kaccWishlistFacade.getWishListDataForCurrentUser(name, userUid);

			final List<WishlistEntryData> selectedWishlistEntries = wishData.getEntries();
			if (CollectionUtils.isNotEmpty(selectedWishlistEntries))
			{
				model.addAttribute("wishlistEntries", selectedWishlistEntries);
			}
			model.addAttribute("wishData", wishData);
		}
		if (LOG.isDebugEnabled())
		{
			LOG.debug("Anonymous User is accessing to wishlist  : " + name);
		}
		return KaccwishlistControllerConstants.Views.Pages.Guest.GuestWishListPage;
	}

	/**
	 * Gets the wish list by email for guest.
	 *
	 * @param model
	 *           the model
	 * @param wishlistCode
	 *           the name wishlist
	 *
	 * @return the wish list
	 * @throws CMSItemNotFoundException
	 *            the CMS item not found exception
	 */
	@RequestMapping(value = "/my-account/wishlist/share", method = RequestMethod.GET)
	public String shareWishlistByEmail(final Model model, @RequestParam("wishlitCode") final String wishlistCode)
			throws CMSItemNotFoundException, UnsupportedEncodingException
	{

		final CustomerData customerData = customerFacade.getCurrentCustomer();

		if (LOG.isDebugEnabled())
		{
			LOG.debug("User  : " + customerData.getUid() + " is trying to share wishlist : " + wishlistCode);
		}
		final ShareWishlistForm shareForm = new ShareWishlistForm();

		model.addAttribute("customerData", customerData);
		model.addAttribute("shareForm", shareForm);
		model.addAttribute("wishlistCode", wishlistCode);

		return KaccwishlistControllerConstants.Views.Pages.Popins.ShareWishlistPopin;
	}

	/**
	 * Share wish list.
	 *
	 * @param model
	 *           the model
	 * @return the string
	 */
	@RequestMapping(value = "/my-account/wishlist/share", method = RequestMethod.POST)
	@ResponseBody
	public String shareWishList(@RequestParam("wishlitCode") final String wishlistCode,
			@RequestParam("receiverName") final String receiverName,
			@RequestParam("receiverEmail") final String receiverEmail)
			throws CMSItemNotFoundException, UnsupportedEncodingException
	{

		final CustomerData customerData = customerFacade.getCurrentCustomer();

		final CustomKeyrusWishlistEmailSenderEvent event = new CustomKeyrusWishlistEmailSenderEvent();
		event.setCustomer((CustomerModel) userService.getCurrentUser());
		event.setCurrency(commonI18NService.getCurrentCurrency());
		event.setLanguage(commonI18NService.getCurrentLanguage());
		event.setBaseStore(baseStore.getCurrentBaseStore());
		event.setSite(baseSite.getCurrentBaseSite());
		event.setReceiverName(receiverName);
		event.setReceiverEmail(receiverEmail);
		event.setCustomerName(customerData.getFirstName());
		// Printable shopping list
		final UserModel userModel = userService.getCurrentUser();
		final String userUID = userModel.getUid();
		final WishlistData wishData = kaccWishlistFacade.getWishListDataForCurrentUser(wishlistCode, userUID);
		event.setPrintableShoppingList(WishListFormatterUtils.getFormattedWhishList(wishData));

		// Publish the event
		eventService.publishEvent(event);

		return REDIRECT_WISHLIST;
	}

	@RequestMapping(value = "/wishlist/addwishlist", method = RequestMethod.GET)
	@RequireHardLogIn
	public String addWishList(final Model model)
	{
		return KaccwishlistControllerConstants.Views.Pages.Popins.AddWishListPopup;
	}

	@RequestMapping(value = "/wishlist/addwishlist", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public KaccWishlistResult addWishListRequest(@RequestParam(value = "wishlistName") final String wishlistName)
	{
		final UserModel user = userService.getCurrentUser();
		return kaccWishlistFacade.createEmptyWishlist(user, wishlistName, StringUtils.EMPTY);
	}

	@RequestMapping(value = "/wishlist/renamewishlist", method = RequestMethod.GET)
	@RequireHardLogIn
	public String renameWishList(final Model model)
	{
		return KaccwishlistControllerConstants.Views.Pages.Popins.RenameWishListPopup;
	}

	@RequestMapping(value = "/wishlist/renamewishlist", method = RequestMethod.POST)
	@RequireHardLogIn
	@ResponseBody
	public KaccWishlistResult renameWishListRequest(@RequestParam(value = "wishlistName") final String wishlistName,
			@RequestParam(value = "newWishlistName") final String newWishlistName)
	{
		final String userUid = userService.getCurrentUser().getUid();
		return kaccWishlistFacade.renameWishlist(userUid, wishlistName, newWishlistName, StringUtils.EMPTY);
	}

	@RequestMapping(value = "/share/{wishlitCode:.*}/{userCode:.*}/wishlist", method = RequestMethod.GET)
	public String sharewishlistwithVisitor(final Model model, @PathVariable("wishlitCode") final String wishlitCode,
			@PathVariable("userCode") final String userCode)
			throws CMSItemNotFoundException, UnsupportedEncodingException
	{
		storeCmsPageInModel(model, getContentPageForLabelOrId(WISHLIST_LAYOUT_PAGE));
		setUpMetaDataForContentPage(model, getContentPageForLabelOrId(WISHLIST_LAYOUT_PAGE));
		final UserModel user = userService.getUserForUID(userCode);
		URLEncoder.encode(wishlitCode, "UTF-8").replace(" ", "%20");
		final WishlistData wishData = kaccWishlistFacade.getWishListDataForCurrentUser(wishlitCode, userCode);
		final List<WishlistEntryData> wishlistEntryData = wishData.getEntries();
		model.addAttribute("wishlistEntryData", wishlistEntryData);
		model.addAttribute("nameOfUser", user.getName());

		return KaccwishlistControllerConstants.Views.Pages.Layout.WishListPageLayout;
	}

	/**
	 *
	 * @param wishListData
	 * @param model
	 */
	private void populateEntrieslists(final List<WishlistEntryData> wishListEntriesData, final Model model)
	{
		final List<WishlistEntryData> allEntries = new ArrayList<>();

		for (final WishlistEntryData entryData : wishListEntriesData)
		{
			allEntries.add(entryData);
		}
		model.addAttribute("allEntries", allEntries);
	}

	protected List<String> getNonSelectableProductsSkus(final WishlistData selectedWishListData)
	{
		final List<String> NonSelectableProductsSkus = new ArrayList<>();
		final List<WishlistEntryData> wishlistEntries = selectedWishListData.getEntries();

		for (final WishlistEntryData entry : wishlistEntries)
		{
			boolean isOK = false;
			try
			{
				isOK = addToCart(entry.getProduct().getCode(), entry.getDesired(), null);
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.error("There is no product with code [" + entry.getProduct().getCode() + "]");
			}
			if (!isOK)
			{
				NonSelectableProductsSkus.add(entry.getProduct().getCode());
			}
		}

		return NonSelectableProductsSkus;

	}

	public boolean addToCart(final String code, final double quantity, final String productType)
	{
		boolean isOk = true;
		final long qty = (long) quantity;

		if (qty <= 0)
		{
			isOk = false;
		}
		return isOk;
	}

	/**
	 *
	 * @param wishListEntries
	 * @return List<WishlistEntryData>
	 */
	private List<WishlistEntryData> isProductInWishList(final List<WishlistEntryData> wishListEntries)
	{
		final List<WishlistEntryData> publicationListWishlistEntryData = new ArrayList<>();

		for (final WishlistEntryData wishlistEntryData : wishListEntries)
		{
			try
			{
				final ProductModel productModel = productService.getProductForCode(getOnlineProductCatalogVersion(),
						wishlistEntryData.getProduct().getCode());
				if (productModel != null)
				{
					publicationListWishlistEntryData.add(wishlistEntryData);
				}
			}
			catch (final UnknownIdentifierException e)
			{
				LOG.error("Product with code " + wishlistEntryData.getProduct().getCode()
						+ " and CatalogVersion '" + CATALOG_ID + "." + ONLINE_CATALOG_VERSION_NAME + "' not found!", e);
			}

		}
		return publicationListWishlistEntryData;
	}

	/**
	 * Add to the product data the classifications to be displayed in the wishlist page
	 * 
	 * @param wishlistEntries
	 */
	private void populateClassif(final List<WishlistEntryData> wishlistEntries)
	{

		//populate classification in wishlist page
		final List<ProductOption> extraOptions = Arrays.asList(ProductOption.CLASSIFICATION);
		for (final WishlistEntryData orderEntryData : wishlistEntries)
		{
			final ProductData entryProductData = orderEntryData.getProduct();
			final ProductData productData = productFacade.getProductForCodeAndOptions(entryProductData.getCode(), extraOptions);
			entryProductData.setClassifications(productData.getClassifications());
		}
	}

	/**
	 *
	 * @return CatalogVersionModel
	 */
	private CatalogVersionModel getOnlineProductCatalogVersion()
	{
		return catalogVersionService.getCatalogVersion(CATALOG_ID, ONLINE_CATALOG_VERSION_NAME);
	}
}
