<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<input type="hidden" id="idproductCode" value="${productCode}" />
<input type="hidden" id="idwishlistName" value="${wishlistName}" />

<div id="wishListPopin" class="wishListPopin">
    <h1><spring:theme code="wishlist.remove.popup.title" /></h1>
   	<div class="actions">
	  	<button type="button" class="btn btn-alt js-wishlist-entry-delete-cancel">
	  		<spring:theme code="wishlist.remove.popup.cancelbutton" />
	  	</button>
  		<button type="button" class="btn btn-alt js-wishlist-entry-delete-confirm">
			<spring:theme code="wishlist.removeProduct.popup.submit" />
		</button>
	</div>
</div>
