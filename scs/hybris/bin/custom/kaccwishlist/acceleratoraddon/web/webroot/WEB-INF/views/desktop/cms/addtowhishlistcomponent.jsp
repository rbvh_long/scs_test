<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/responsive/product" %>

<addonProduct:productAddToWishlistButton product="${product}" />
