<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>


<a href="javascript:void();" id="idFav" class="addtolist add-button-whishlist-link js-add-product-to-wishlist"
		data-add-product-to-wishlist-url="${pageContext.request.contextPath}/wishlist/addProductToWishlistPopup/${product.code}"
		data-title="${addProductToWishlistTitle}"><spring:theme code="text.whishlist.add" /></a>
