<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/desktop/storepickup" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- Based on productListerItem.tag and productAddToCartPanel.tag from the yacceleratorstorefront extension --%>

<div class="productAddToCart">


	<c:url value="/cart/add" var="addToCartUrl"/>
	<div class="clear_fix">
		<form:form method="post" id="addToCartForm${product.code}" class="add_to_cart_form" action="${addToCartUrl}">

			<input type="hidden" name="productCodePost" value="${product.code}"/>
			<c:set var="buttonType">button</c:set>

			<c:if test="${product.purchasable and product.stock.stockLevelStatus.code eq 'inStock' }">
				<c:set var="buttonType">submit</c:set>
			</c:if>

			<c:choose>
				<c:when test="${fn:contains(buttonType, 'button')}">
					<button type="${buttonType}" class="addToCartButton outOfStock" disabled="disabled">
						<spring:theme code="product.variants.out.of.stock"/>
					</button>
				</c:when>
				<c:otherwise>
					<button type="${buttonType}" class="addToCartButton">
						<spring:theme code="basket.add.to.basket"/>
					</button>
				</c:otherwise>
			</c:choose>

		</form:form>
	</div>

</div>
