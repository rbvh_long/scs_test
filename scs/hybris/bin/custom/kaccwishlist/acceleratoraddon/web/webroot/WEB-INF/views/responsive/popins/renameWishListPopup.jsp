<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<div class="wishListPopin">
		
	<h1><spring:theme code="kacc.wishlist.account.name"/></h1>
	
	<div class="text">
		<label for="idWishlistName"><spring:theme code="kacc.wishlist.account.name"/></label>
		<input type="text" placeholder="<spring:theme code="kacc.wishlist.account.name"/>" 
			class="required" name="idWishlistName" id="idWishlistName">	

		<div class="alert alert-danger errorValidationEmpty" style="display:none;">
			<spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/>
		</div>
		<div class="alert alert-danger errorValidationUnique" style="display:none;">
			<spring:theme code="kacc.wishlist.account.add.newWishlist.errorUnique"/>
		</div>
		<div class="alert alert-danger errorSizeMessage" style="display:none;">
			<spring:theme code="kacc.wishlist.account.add.newWishlist.maxSizeTen"/>
		</div>
		<div class="alert alert-danger errorMaxCharacter" style="display:none;">
			<spring:theme code="kacc.wishlist.empty.wishlist.sizeError"/>
		</div>
	</div>
	
	<div class="actions">
		<button type="button" class="js-wishlist-new-cancel button button--secondary">
			<spring:theme code="wishlist.remove.popup.cancelbutton"/>  
		</button>
		<button type="button" id="idWishlistSaveButton" class="js-wishlist-rename-confirm button button--secondary">
			<spring:theme code="kacc.wishlist.account.submit.rename" />
		</button>
	</div>

</div>
