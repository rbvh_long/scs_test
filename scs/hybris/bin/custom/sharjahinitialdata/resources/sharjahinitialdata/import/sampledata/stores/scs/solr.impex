# -----------------------------------------------------------------------
# [y] hybris Platform
#
# Copyright (c) 2017 SAP SE or an SAP affiliate company.  All rights reserved.
#
# This software is the confidential and proprietary information of SAP
# ("Confidential Information"). You shall not disclose such Confidential
# Information and shall use it only in accordance with the terms of the
# license agreement you entered into with SAP.
# -----------------------------------------------------------------------
#
# Add additional sample solr index configuration
# - indexed properties for the classification features that we want to index
#
$version=Staged
#$version=Online
$classSystemVersion=systemVersion(catalog(id[default='scsClassification']),version[default='1.0'])
$classCatalogVersion=catalogVersion(catalog(id[default='scsClassification']),version[default='1.0'])
$classAttribute=classificationAttribute(code,$classSystemVersion)
$classClass=classificationClass(code,$classCatalogVersion)
$classAttributeAssignment=classAttributeAssignment($classClass,$classAttribute,$classSystemVersion)
$contentCatalogName=scsContentCatalog
$contentCatalogVersion=catalogVersion(CatalogVersion.catalog(Catalog.id[default=$contentCatalogName]),CatalogVersion.version[default=$version])[default=$contentCatalogName:$version]
$productCatalog=scsProductCatalog
$productCatalogVersion=catalogversion(catalog(id[default=$productCatalog]),version[default=$version])[unique=true,default=$productCatalog:$version]

# Define price range set
INSERT_UPDATE SolrValueRangeSet ; name[unique=true] ; qualifier ; type   ; solrValueRanges(&rangeValueRefID)                                                                                                                                                                                                                                                                                                                                                                        

# Define Megapixel ranges       
INSERT_UPDATE SolrValueRange ; &rangeValueRefID    ; solrValueRangeSet(name)[unique=true] ; name[unique=true] ; from ; to     


INSERT_UPDATE SolrIndexedProperty ; solrIndexedType(identifier)[unique=true] ; name[unique=true]       ; type(code) ; sortableType(code) ; currency[default=false] ; localized[default=false] ; multiValue[default=false] ; facet[default=false] ; facetType(code) ; facetSort(code) ; priority ; visible ; fieldValueProvider                          ; customFacetSortProvider      ; rangeSets(name) ; $classAttributeAssignment         

INSERT_UPDATE SolrIndexedProperty ; solrIndexedType(identifier)[unique=true] ; name[unique=true]  ; type(code) ; displayName ; multiValue[default=true] ; facet[default=true] ; facetType(code) ; facetSort(code) ; priority ; visible ; categoryField[default=true] ; fieldValueProvider                 ; facetDisplayNameProvider         ; topValuesProvider       
                                  ; scsProductType                         ; stickers           ; string     ;             ;                          ; false              ; Refine          ; Alpha           ; 5000     ; true    ; false                       ; sharjahProductStickersValueResolver       ;                                  ;                         
                                  ; scsProductType                         ; stickerEndDate     ; string     ;             ;                          ; false              ; Refine          ; Alpha           ; 5000     ; true    ; false                       ; sharjahProductStickersValueResolver       ;                                  ;                        
                                  ; scsProductType                         ; stickerStartDate   ; string     ;             ;                          ; false              ; Refine          ; Alpha           ; 5000     ; true    ; false                       ; sharjahProductStickersValueResolver       ;                                  ;                        

# Import classification features  
INSERT_UPDATE SolrIndexedProperty ; solrIndexedType(identifier)[unique=true] ; name[unique=true] ; type(code) ; localized[default=true] ; multiValue[default=false] ; fieldValueProvider                          ; $classAttributeAssignment            

# Show the classification features in the product lister
INSERT_UPDATE ClassAttributeAssignment ; $classClass[unique=true] ; $classAttribute[unique=true] ; $classSystemVersion[unique=true] ; listable
                                       ; 622                      ; Size, 1147                   ;                                  ; true    
                                       ; 622                      ; Video memory capacity, 5167  ;                                  ; true    
                                       ; 622                      ; Aperture setting, 5775       ;                                  ; true    
                                       ; 622                      ; Picture mode, 2030           ;                                  ; true    
                                       ; 42                       ; Compatible memory cards, 730 ;                                  ; true    
                                       ; 41                       ; Display, 83                  ;                                  ; true    

# Redirect page URL                    
INSERT_UPDATE SolrURIRedirect ; url[unique=true]                                
                              ; "/cart"                                         
                              ; "http://www.hybris.com/multichannel-accelerator"

INSERT_UPDATE SolrPageRedirect ; redirectItem(uid,$contentCatalogVersion)[unique=true]
                               ; faq                                                  

INSERT_UPDATE SolrCategoryRedirect ; redirectItem(code,$productCatalogVersion)[unique=true]
                                   ; brand_5                                               
                                   ; brand_10                                              
                                   ; 902                                                   

INSERT_UPDATE SolrProductRedirect ; redirectItem(code,$productCatalogVersion)[unique=true]
                                  ; 1382080                                               
                                  ; 2053266                                               
                                  ; 816780                                                
