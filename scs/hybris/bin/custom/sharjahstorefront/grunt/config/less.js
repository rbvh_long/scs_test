/*global require, module,  __dirname */
module.exports = {
	dev: {
		options: {
			compress: false,
			sourceMap: true,
			sourceMapFileInline: true,
			outputSourceFiles: true,
			plugins: [
				new(require('less-plugin-autoprefix'))({
					browsers: ["last 3 versions", ">1%"]
				}),
				require('less-plugin-glob')
			]
		},
		files: [{
			expand: true,
			cwd: '<%= store.baseSrc %>',
			src: ['<%= store.src.less.targets %>'],
			dest: '<%= store.baseDist %>',
			ext: '<%= store.dist.css.nominExt %>',
			rename: function(dest, src) {
				var nsrc = src.replace(new RegExp("/themes/(.*)/less"), "/theme-$1/css");
				return dest + nsrc;
			}
		}]
	},
	dist: {
		options: {
			compress: false,
			sourceMap: false,
			plugins: [
				new(require('less-plugin-autoprefix'))({
					browsers: ["last 3 versions", ">1%"]
				}),
				require('less-plugin-glob')
			]
		},
		files: [{
			expand: true,
			cwd: '<%= store.baseSrc %>',
			src: ['<%= store.src.less.targets %>'],
			dest: '<%= store.baseDist %>',
			ext: '<%= store.dist.css.minExt %>',
			rename: function(dest, src) {
				var nsrc = src.replace(new RegExp("/themes/(.*)/less"), "/theme-$1/css");
				return dest + nsrc;
			}
		}]
	}
};
