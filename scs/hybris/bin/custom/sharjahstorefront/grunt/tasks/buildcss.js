module.exports = function(grunt) {
	var pkg = grunt.config('pkg');
	var store = grunt.config('store');


	//= For debugging
	// if (grunt.option('debug')) {
	// 	console.log('==============');
	// 	console.log('store.theme.themeType.selected : \n\t\t');
	// 	console.log(JSON.stringify(store.theme.themeType.selected));
	// 	console.log('==============');
	// 	grunt.fail.warn('stop');
	// }
	// grunt.registerTask('buildcss', ['printConfig','lesshint','lessdev','lessdist']); // Line for debug
	if (store.theme.themeType.selected.length > 0) {
		grunt.registerTask('buildcss', ['buildLessTargets', 'lesshint', 'clean:css', 'less', 'px_to_rem', 'rtlcss']);
	} else {
		grunt.registerTask('buildcss', ['choosethemetype', 'buildLessTargets', 'lesshint', 'clean:css', 'less', 'postcss']);
	}
};
