module.exports = function(grunt) {
	var pkg = grunt.config('pkg');
	var store = grunt.config('store');

	if (store.theme.themeType.selected.length > 0) {
		grunt.registerTask('buildall', ['folder_list', 'clean:images', 'clean:fonts', 'buildcss', 'buildjs', 'sync']);
	} else {
		grunt.registerTask('buildall', ['folder_list', 'clean:images', 'clean:fonts', 'choosethemetype', 'buildcss', 'buildjs', 'sync']);
	}
};
