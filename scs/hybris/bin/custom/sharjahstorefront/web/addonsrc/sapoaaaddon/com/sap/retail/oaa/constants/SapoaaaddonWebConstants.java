/*
 * [y] hybris Platform
 *
 * Copyright (c) 2000-2015 hybris AG
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of hybris
 * ("Confidential Information"). You shall not disclose such Confidential
 * Information and shall use it only in accordance with the terms of the
 * license agreement you entered into with hybris.
 *
 *
 */
package com.sap.retail.oaa.constants;

/**
 * Global class for all Sapoaaaddon web constants. You can add global constants for your extension into this class.
 */
public final class SapoaaaddonWebConstants
{
	public static final String OAA_CHECKOUT_ERROR = "com.sap.retail.oaa.checkout.error";

	private SapoaaaddonWebConstants()
	{
		//empty to avoid instantiating this constant class
	}

	// implement here constants used by this extension
}
