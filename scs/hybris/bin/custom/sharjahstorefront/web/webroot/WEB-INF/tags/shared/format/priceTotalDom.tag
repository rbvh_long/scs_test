<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="priceData" required="true" type="de.hybris.platform.commercefacades.product.data.PriceData" %>
<%@ attribute name="displayFreeForZero" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<%--
 Tag to render a currency formatted price.
 Includes the currency symbol for the specific currency.
--%>
<c:choose>
	<c:when test="${priceData.value > 0}">
		<span class="price--currency">${priceData.currencyIso}&nbsp;</span>
		<span class="price--total">${priceData.value}</span>
	</c:when>
	<c:otherwise>
		<c:if test="${displayFreeForZero}">
			<span class="price--total-free"><spring:theme code="text.free"/></span>
		</c:if>
		<c:if test="${not displayFreeForZero}">
			<span class="price--currency">${priceData.currencyIso}&nbsp;</span>
			<span class="price--total-discount">${priceData.value}</span>
		</c:if>
	</c:otherwise>
</c:choose>
