<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>

<link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic" />

<%-- Custom generated bundle CSS --%>
<c:choose>
	<%-- Test to load css for debug needs (add ?debug=true to URL) --%>
	<c:when test="${param.debug>=1}">
		<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style${currentLanguage.isocode == 'ar' ? '-rtl' : ''}.css" />
	</c:when>
	<c:otherwise>
		<link rel="stylesheet" type="text/css" media="all" href="${themeResourcePath}/css/style${currentLanguage.isocode == 'ar' ? '-rtl' : ''}.min.css" />
	</c:otherwise>
</c:choose>

<%--  AddOn Theme CSS files --%>
<c:forEach items="${addOnThemeCssPaths}" var="addOnThemeCss">
	<link rel="stylesheet" type="text/css" media="all" href="${addOnThemeCss}"/>
</c:forEach>

<%-- <link rel="stylesheet" href="${commonResourcePath}/blueprint/print.css" type="text/css" media="print" />
<style type="text/css" media="print">
	@IMPORT url("${commonResourcePath}/blueprint/print.css");
</style>
 --%>

<cms:previewCSS cmsPageRequestContextData="${cmsPageRequestContextData}" />
