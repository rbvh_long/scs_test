<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="nav" tagdir="/WEB-INF/tags/responsive/nav" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="pagination" tagdir="/WEB-INF/tags/responsive/nav/pagination" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>


<spring:url value="/my-account/order/" var="orderDetailsUrl"/>
<c:set var="searchUrl" value="/my-account/orders?sort=${searchPageData.pagination.sort}"/>

<div class="account-section-header">
	<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
	<span><spring:theme code="text.account.orderHistory" /></span>
</div>

<div>
	<select id="orderSortBySelectFilter">
		<option value="0"><spring:theme code="text.account.orderHistory.page.sort" /></option>
		<option value="1"><spring:theme code="text.account.orderHistory.orderNumber" /></option>
		<option value="2"><spring:theme code="text.account.orderHistory.orderStatus"/></option>
		<option value="3"><spring:theme code="text.account.orderHistory.datePlaced"/></option>
		<option value="4"><spring:theme code="text.account.orderHistory.total"/></option>
	</select>
</div>

<c:if test="${empty searchPageData.results}">
	<div class="account-section-content content-empty">
		<ycommerce:testId code="orderHistory_noOrders_label">
			<spring:theme code="text.account.orderHistory.noOrders" />
		</ycommerce:testId>
	</div>
</c:if>
<c:if test="${not empty searchPageData.results}">
	<div class="account-section-content	">
		<div class="account-orderhistory">
            <div class="account-overview-table">
            	<div class="account-history-found">
            		<spring:theme code="order.history.found" arguments="${fn:length(searchPageData.results)}" />
            	</div>
				<table id="orderHistoryDataTable" class="display orderhistory-list-table responsive-table">
				<thead>
					<tr class="account-orderhistory-table-head responsive-table-head hidden-xs">
						<th id="sortFilter_1"><spring:theme code="text.account.orderHistory.orderNumber" /></th>
						<th id="sortFilter_2"><spring:theme code="text.account.orderHistory.orderStatus"/></th>
						<th id="sortFilter_3"><spring:theme code="text.account.orderHistory.datePlaced"/></th>
						<th id="sortFilter_4"><spring:theme code="text.account.orderHistory.total"/></th>
					</tr>
				</thead>
				<tbody>
					<c:forEach items="${searchPageData.results}" var="order">
						<tr class="responsive-table-item">
							<ycommerce:testId code="orderHistoryItem_orderDetails_link">
								<td class="responsive-table-cell">
									<span class="visible-xs responsive-table-item--left"><a href="${orderDetailsUrl}${order.code}" class="responsive-table-link"><spring:theme code="text.account.orderHistory.orderNumber" /></a></span>
									<a href="${orderDetailsUrl}${order.code}" class="responsive-table-link">
										${order.code}
									</a>
								</td>
								<td class="status">
									<span class="visible-xs responsive-table-item--left"><spring:theme code="text.account.orderHistory.orderStatus"/></span>
									<spring:theme code="text.account.order.status.display.${order.statusDisplay}"/>
								</td>
								<td class="responsive-table-cell">
									<span class="visible-xs responsive-table-item--left"><spring:theme code="text.account.orderHistory.datePlaced"/></span>
									<fmt:formatDate value="${order.placed}" dateStyle="medium" timeStyle="short" type="both"/>
								</td>
								<td class="responsive-table-cell responsive-table-cell-bold">
									<span class="visible-xs responsive-table-item--left"><spring:theme code="text.account.orderHistory.total"/></span>
									${order.total.formattedValue}
								</td>
							</ycommerce:testId>
						</tr>
					</c:forEach>
					</tbody>
				</table>
            </div>
		</div>
	</div>
</c:if>
