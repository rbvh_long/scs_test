<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="slotRange" required="true" type="java.lang.Integer" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>


<table>
	<tr>
		<c:forEach items="${deliverySlots}" end="${slotRange - 1}" var="entry">
			<th>
				<fmt:formatDate pattern="EEEE dd/MM" value="${entry.key}" />
			</th>
		</c:forEach>
	</tr>

	<c:forEach var="i" begin="0" end="${maxDeliverySlots - 1}">
		<tr>
			<c:forEach items="${deliverySlots}" end="${slotRange - 1}" var="entry">
				<fmt:formatDate pattern="yyyy-MM-dd" value="${entry.key}" var="slotDate" />
				<c:set value="${entry.value}" var="slotDataList" />
				<c:set value="${slotDataList[i].capacity le 0 or slotDataList[i].expired ? 'unavailable' : 'available'}" var="slotStatus" />

				<td class="deliverySlot" style="white-space: nowrap;" data-status="${slotStatus}" data-selected="${slotDataList[i].selected}"
						data-day="${slotDataList[i].dayOfWeek}" data-date="${slotDate}" data-slot-code="${slotDataList[i].deliverySlotCode}"
						data-pos="${slotDataList[i].pos}">
						${slotDataList[i].beginTime}-${slotDataList[i].endTime}
				</td>
			</c:forEach>
		</tr>
	</c:forEach>
</table>
