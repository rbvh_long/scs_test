<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%--
 SVG close icon
--%>
<?xml version="1.0" encoding="iso-8859-1"?>
<svg id="close" data-name="close" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="45px" height="45px">
	<title>close</title>
	<polygon class="cls-1" points="35.43 13.87 34.13 12.57 24 22.7 13.87 12.57 12.57 13.87 22.7 24 12.57 34.13 13.87 35.43 24 25.3 34.13 35.43 35.43 34.13 25.3 24 35.43 13.87" />
</svg>
