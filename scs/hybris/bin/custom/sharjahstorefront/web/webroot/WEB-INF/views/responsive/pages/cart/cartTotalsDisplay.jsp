<%@ taglib prefix="cart" tagdir="/WEB-INF/tags/responsive/cart" %>

<%-- Verified that there's a pre-existing bug regarding the setting of showTax; created issue  --%>
<div id="cartTotalsDisplay" class="cartTotalsDisplay cart--bottom">
	<cart:cartVoucher cartData="${cartData}"/>
	<cart:cartTotals cartData="${cartData}" showTax="false"/>
	<%--<cart:ajaxCartTotals/>--%>
</div>
