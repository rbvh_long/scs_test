<%@ tag body-content="empty" trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product"%>


<div class="product-information">
	<%-- DESCRIPTION --%>
	<div id="description" class="product-information--description">
		<p class="title"><spring:theme code="product.details.description.title"/></p>
		<product:productDetailsTab product="${product}" />
	</div>

	<%-- INFORMATION --%>
	<div class="product-information--informations">
		<p class="title"><spring:theme code="product.details.information.title"/></p>
		<product:productDetailsClassifications product="${product}" />
	</div>

</div>
<%-- NUTRITION --%>
<c:forEach items="${product.classifications}" var="classification">
	<c:if test="${fn:toLowerCase(classification.name) eq 'nutrition'}">
		<c:set value="${classification}" var="nutritionClassification"></c:set>
	</c:if>
</c:forEach>
<c:if test="${not empty nutritionClassification}">
	<div class="product-nutrition">
		<p class="title"><spring:theme code="product.details.nutrition.title"/></p>
		<!-- <table class="table">
		<tbody> -->
		<div class="product-nutrition--container">
			<c:forEach items="${nutritionClassification.features}" var="feature">
				<!-- <tr> -->
				<div class="product-nutrition--container-category ${feature.name}">
					<p class="product-nutrition--container-category-title">${feature.name}</p>
					<!-- <td> -->
					<div class="product-nutrition--container-category-info">
						<c:forEach items="${feature.featureValues}" var="value" varStatus="status">
							<span>${fn:escapeXml(value.value)}</span>
							<c:choose>
								<c:when test="${feature.range}">
									${not status.last ? '-' : feature.featureUnit.symbol}
								</c:when>
								<c:otherwise>
									${feature.featureUnit.symbol}
									${(fn:length(feature.featureValues) - 1) == status.count ? '<br/>' : ''}
								</c:otherwise>
							</c:choose>
						</c:forEach>
					</div>
				</div>
			</c:forEach>
		</div>
	</div>
</c:if>

