<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="order" required="true" type="de.hybris.platform.commercefacades.order.data.OrderData" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>

<div class="well well-quinary well-xs">
	<c:choose>
		<c:when test="${entry.deliveryPointOfService ne null}">
			<div class="well-content">
				<div class="row">
					<div class="col-sm-12 col-md-9">
						<order:storeAddressItem deliveryPointOfService="${entry.deliveryPointOfService}" />
					</div>
				</div>
			</div>
		</c:when>
		<c:otherwise>
			<div class="well-content">
				<div class="row">
					<div class="col-md-5">
						<div class="order-detail-overview--shipping">
							<div class="order-detail-overview--shipping-label">
								<spring:theme code="text.account.order.shipto" />
							</div>
							<div class="order-detail-overview--shipping-value">
								<order:addressItem address="${orderData.deliveryAddress}"/>
							</div>
						</div>
					</div>
				</div>
			</div>
		</c:otherwise>
	</c:choose>
</div>

<c:forEach items="${order.unconsignedEntries}" var="entry" varStatus="loop">

    <ul class="item__list">
        <order:orderEntryDetails orderEntry="${entry}" order="${order}" itemIndex="${loop.index}"/>
    </ul>
</c:forEach>
