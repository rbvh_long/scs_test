<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%-- <%@ taglib prefix="account" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/account" %> --%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%-- <%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %> --%>
<%-- <%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %> --%>
<%-- <%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %> --%>

<body>
	<div class="account-section my-account-wishlist">
		
		<div class="account-section-header ${noBorder}">
					<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
		   <span> <spring:theme code="kacc.wishlist.account.myWishlist"/></span>
		</div>
		
<!-- 		<p class="alert alert-info"> -->
<%-- 			<strong> <spring:theme code="kacc.wishlist.account.shortText"/></strong><br>  --%>
<%-- 			<span> <spring:theme code="kacc.wishlist.account.longText"/> </span> --%>
<!-- 		</p> -->
		
		
			<input type="hidden" id="wishlistName" />
		
			<c:choose>
				<c:when test="${not empty  wishlistDatas}">
				
				<div class="account-shopping-list">
					<table>
						<tbody>
						<c:forEach var="item" items="${wishlistDatas}">
							<tr class="account-shopping-list-item">
								<td class="name"><a href="#" class="js-wishlist-click linkWidhList" data-wishlist-item-name="${item.name}">${item.name}</a></td>
								<td><a href="#" class="js-wishlist-delete delete" data-wishlist-item-name="${item.name}"></a></td>
							</tr>
						</c:forEach>
						</tbody>
					</table>
				</div>
		
				</c:when>
				
				<c:otherwise>
					<p class="alert"><spring:theme code="kacc.wishlist.empty.wishlist.message" /></p>
				</c:otherwise>
			</c:choose>
				
			<!-- NEW WISHLIST -->
			<div>
				<button class="js-wishlist-new button button--secondary" title="">
					<spring:theme code="kacc.wishlist.account.addWishlist"/>  
				</button>
			</div>
			 
		
		<!-- DELETE WISHLIST POPIN -->
		<div id="whishConfirmationMssg" class="alert alert-info" style="display:none;"> 
			<p><spring:theme code="kacc.wishlist.account.deleteWishlist"/></p>
			<div class="actions">
				<button class="js-wishlist-delete-cancel button button--secondary">
					<spring:theme code="wishlist.remove.popup.cancelbutton"/>
				</button>
				<button class="js-wishlist-delete-confirm button button--secondary" >
					<spring:theme code="wishlist.remove.popup.submit"/>
				</button>
			</div>	
		</div>
	
	</div>

</body>
</html>
