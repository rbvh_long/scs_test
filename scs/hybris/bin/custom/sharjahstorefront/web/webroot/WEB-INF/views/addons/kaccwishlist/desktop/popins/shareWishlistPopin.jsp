<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/desktop/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/desktop/common" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/desktop/formElement" %>

 <script src="${contextPath}/_ui/addons/kaccwishlist/${fn:toLowerCase(uiExperienceLevel)}/common/js/jquery.validate.min.js"></script>

 <div id="partage-form">
 <input type="hidden" id="wishlistCode" value="${wishlistCode}" />
<!--  <div id="idResultMail"></div> -->

        <form:form method="POST" commandName="shareForm" name="shareForm" >
            <div class="wrapper-form">
                <p>
                    De : ${fn:escapeXml(shareForm.firstName)}&nbsp;${fn:escapeXml(shareForm.lastName)}&nbsp;(${fn:escapeXml(shareForm.emailSender)})
                </p>
                <div class="emailContent">
 
                <form:input type="text-input" cssClass="text-input" id="email" path="email" placeholderKey="wishListShareForm.emailRecipient.placeholder" placeholder="Pour" title="" maxlength="60" />
                <div class="errorValidation">
						<spring:theme code="kacc.wishlist.empty.wishlist.mandatory"/>
					</div>
<div class="errorSize">
	<spring:theme code="wishListShareForm.email.invalid"/>
</div>
                </div>
                <span>

            			<form:textarea cssClass="text-input" id="message" path="message"  rows="5" placeholderKey="wishListShareForm.message.placeholder"/>
            			
            	</span>
<div class="errorSizeMsg" >
	<spring:theme code="wishListShareForm.message.invalid.size"/>
</div>
            </div>
            <label class="checkbox"><form:checkbox path="copyEmail" /><spring:theme code="wishListShareForm.copyEmail" /></label>
            <button  id="idSubmitSendWish" type="submit" value="valider" class="btn-primary btn-primary-round"><i class="ico ico-arrow-right"></i> valider</button>
        </form:form>
    </div>
    
   <div id="div-popin-pwd-ok" class="hide">
	
		<h2><spring:theme code="wishListShareForm.message.confirmation.title"/></h2>
		<p><i></i><spring:theme code="wishListShareForm.message.confirmation" /></p>
	
</div>


<script type="text/javascript">
        var url = '/my-account/wishlist/share?wishlitCode='+$('#wishlistCode').attr('value');
        formShareList(url);
</script>

