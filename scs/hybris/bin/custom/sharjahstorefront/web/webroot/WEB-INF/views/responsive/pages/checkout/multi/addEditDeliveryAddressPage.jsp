<%@ page trimDirectiveWhitespaces="true"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common"%>
<%@ taglib prefix="address" tagdir="/WEB-INF/tags/responsive/address"%>
<%@ taglib prefix="multi-checkout" tagdir="/WEB-INF/tags/responsive/checkout/multi"%>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>

<template:page pageTitle="${pageTitle}" hideHeaderLinks="true">
	<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
		<cms:component component="${component}" />
	</cms:pageSlot>

<div class="row">

	<div class="account-section-header">
    <a href="javascript:history.back();">
    
<div class="account-section-header ${noBorder}">
					<span class="glyphicon account-section-header-orderhistory ${currentLanguage.isocode == 'ar' ? 'glyphicon-chevron-right' : ' glyphicon-chevron-left'}"></span>
	<span> <spring:theme code="checkout.multi.secure.checkout" /></span>
</div>
   </a>
	</div>

	<div class="col-md-5">
			<multi-checkout:checkoutSteps checkoutSteps="${checkoutSteps}" progressBarId="${progressBarId}">
			<jsp:body>
				<ycommerce:testId code="checkoutStepOne">
					<div class="checkout-shipping">
						<multi-checkout:shipmentItems cartData="${cartData}" showDeliveryAddress="false" />
						<div class="checkout-indent">
							<address:addressFormSelector supportedCountries="${countries}" regions="${regions}" cancelUrl="${currentStepUrl}" country="${country}" />
							<%-- Hide the wrapper to use the content in colorbox --%>
							<div style="display:none;">
								<div id="addressbook" class="addressBook-flex">
									<c:forEach items="${deliveryAddresses}" var="deliveryAddress" varStatus="status">
										<div class="addressEntry">
											<form action="${request.contextPath}/checkout/multi/delivery-address/select" method="GET">
												<input type="hidden" name="selectedAddressCode" value="${deliveryAddress.id}" />
												<ul>
													<li>
													    <c:if test="${not empty fn:escapeXml(deliveryAddress.name)}">
														<strong>${fn:escapeXml(deliveryAddress.name)}</strong>
														</c:if>
														<c:if test="${not empty fn:escapeXml(deliveryAddress.firstName) and not empty fn:escapeXml(deliveryAddress.lastName)}">
														<br>
														${fn:escapeXml(deliveryAddress.firstName)}&nbsp;
														${fn:escapeXml(deliveryAddress.lastName)}
														</c:if>
                                                        <c:if test="${not empty fn:escapeXml(deliveryAddress.building)}">
                                                        <br>
														${fn:escapeXml(deliveryAddress.building)}
														</c:if>
														<c:if test="${not empty fn:escapeXml(deliveryAddress.apartment)}">
														<br>
														${fn:escapeXml(deliveryAddress.apartment)}
														</c:if>
														<c:if test="${not empty fn:escapeXml(deliveryAddress.landmark)}">
														<br>
														${fn:escapeXml(deliveryAddress.landmark)}
														</c:if>
														<br>
														${fn:escapeXml(deliveryAddress.line1)}&nbsp;
														${fn:escapeXml(deliveryAddress.line2)}
														<br>
														${fn:escapeXml(deliveryAddress.town)}
														<c:if test="${not empty deliveryAddress.region.name}">
														&nbsp;${fn:escapeXml(deliveryAddress.region.name)}
														</c:if>
														<br>
														${fn:escapeXml(deliveryAddress.country.name)}&nbsp;
														${fn:escapeXml(deliveryAddress.postalCode)}
													</li>
												</ul>
												<button type="submit" class="button button--secondary">
													<spring:theme code="checkout.multi.deliveryAddress.useThisAddress" text="Use this Address" />
												</button>
											</form>
										</div>
									</c:forEach>
								</div>
							</div>
							<address:suggestedAddresses selectedAddressUrl="/checkout/multi/delivery-address/select" />
						</div>
						<multi-checkout:pickupGroups cartData="${cartData}" />
					</div>
					<multi-checkout:deliverySlots />
					<div class="form">
						<button id="addressSubmit" type="button" class="button button--secondary checkout-next"><spring:theme code="checkout.multi.deliveryAddress.continue" text="Next"/></button>
					</div>
				</ycommerce:testId>
			</jsp:body>
		</multi-checkout:checkoutSteps>
	</div>
	<div class="col-md-1">
	</div>
	<div class="col-md-6 hidden-xs">
		<multi-checkout:checkoutOrderDetails cartData="${cartData}" showDeliveryAddress="false" showPaymentInfo="false" showTaxEstimate="false" showTax="true" />
	</div>
	<div class="col-sm-12 col-lg-12">
		<cms:pageSlot position="SideContent" var="feature" element="div" class="checkout-help">
			<cms:component component="${feature}"/>
		</cms:pageSlot>
	</div>
</div>
</template:page>
