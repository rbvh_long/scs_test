<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="cartData" required="true" type="de.hybris.platform.commercefacades.order.data.CartData" %>
<%@ attribute name="showDeliveryAddress" required="true" type="java.lang.Boolean" %>
<%@ attribute name="showPotentialPromotions" required="false" type="java.lang.Boolean" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="order" tagdir="/WEB-INF/tags/responsive/order" %>
<%@ taglib prefix="grid" tagdir="/WEB-INF/tags/responsive/grid" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>

<c:set var="hasShippedItems" value="${cartData.deliveryItemsQuantity > 0}" />
<c:set var="deliveryAddress" value="${cartData.deliveryAddress}"/>

<c:if test="${not hasShippedItems}">
	<spring:theme code="checkout.pickup.no.delivery.required"/>
</c:if>
<c:if test="${hasShippedItems}">
	<c:choose>
		<c:when test="${showDeliveryAddress and not empty deliveryAddress}">
		<div class="checkout-order-summary-list-heading">
			<div class="title"><spring:theme code="checkout.pickup.items.to.be.shipped"/></div>
			<div class="address">
				<div class="">${fn:escapeXml(deliveryAddress.firstName)}&nbsp;${fn:escapeXml(deliveryAddress.lastName)}</div>
				<div class="">
					<c:if test="${ not empty deliveryAddress.line1 }">
						<span class="">${fn:escapeXml(deliveryAddress.line1)},</span>&nbsp;
					</c:if>
					<c:if test="${ not empty deliveryAddress.apartment }">
						<span class="">${fn:escapeXml(deliveryAddress.apartment)},</span>&nbsp;
					</c:if>
					<c:if test="${ not empty deliveryAddress.building }">
						<span class="">${fn:escapeXml(deliveryAddress.building)},</span>&nbsp;
					</c:if>
				</div>
				<div class="">
					<c:if test="${ not empty deliveryAddress.deliveryArea }">
						<span class="">${fn:escapeXml(deliveryAddress.deliveryArea.code)},</span>&nbsp;
					</c:if>
					<c:if test="${not empty deliveryAddress.town }">
						<span class="">${fn:escapeXml(deliveryAddress.town)},</span>&nbsp;
					</c:if>
					<c:if test="${ not empty deliveryAddress.country.name }">
						<span class="">${fn:escapeXml(deliveryAddress.country.name)}</span>
					</c:if>
					<br/>
					<c:if test="${ not empty deliveryAddress.landmark}">
						<span class="">${fn:escapeXml(deliveryAddress.landmark)}</span>
					</c:if>
				</div>
			</div>
		</div>
		</c:when>
		<c:otherwise>
		<div class="checkout-order-summary-list-heading empty">
			<spring:theme code="checkout.pickup.items.to.be.delivered" />
		</div>
		</c:otherwise>
	</c:choose>
</c:if>
<ul class="checkout-order-summary-list">
<c:forEach items="${cartData.entries}" var="entry" varStatus="loop">
	<c:if test="${entry.deliveryPointOfService == null}">
		<c:url value="${entry.product.url}" var="productUrl"/>
		<li class="checkout-order-summary-list-items">
			<div class="checkout-order-summary-list-items-thumb">
				<a href="${productUrl}">
					<product:productPrimaryImage product="${entry.product}" format="thumbnail"/>
				</a>
			</div>
			<div class="checkout-order-summary-list-items-details">
				<div class="checkout-order-summary-list-items-name"><a href="${productUrl}">${fn:escapeXml(entry.product.name)}</a></div>
				<%-- this div isn't needed in the psd --%>
				<%--<div>
										<span class="label-spacing"><spring:theme code="order.itemPrice" />:</span>
					<c:if test="${entry.product.multidimensional}">--%>
						<%-- if product is multidimensional with different prices, show range, else, show unique price --%>
						<%--<c:choose>
							<c:when test="${entry.product.priceRange.minPrice.value ne entry.product.priceRange.maxPrice.value}">
								<format:price priceData="${entry.product.priceRange.minPrice}" /> - <format:price priceData="${entry.product.priceRange.maxPrice}" />
							</c:when>
														<c:when test="${entry.product.priceRange.minPrice.value eq entry.product.priceRange.maxPrice.value}">
																<format:price priceData="${entry.product.priceRange.minPrice}" />
														</c:when>
							<c:otherwise>
								<format:price priceData="${entry.product.price}" />
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${! entry.product.multidimensional}">
						<order:orderEntryBasePriceDisplay entry="${entry }"/>
					</c:if>
				</div><--%>
				<c:if test="${not empty entry.entryConf}">
					<div class="checkout-order-summary-list-items-config">${entry.entryConf}</div>
				</c:if>
				<div>
					<c:forEach items="${entry.product.baseOptions}" var="option">
						<c:if test="${not empty option.selected and option.selected.url eq entry.product.url}">
							<c:forEach items="${option.selected.variantOptionQualifiers}" var="selectedOption">
								<div>${selectedOption.name}: ${selectedOption.value}</div>
							</c:forEach>
						</c:if>
					</c:forEach>

					<c:if test="${ycommerce:doesPotentialPromotionExistForOrderEntry(cartData, entry.entryNumber) && showPotentialPromotions}">
						<c:forEach items="${cartData.potentialProductPromotions}" var="promotion">
							<c:set var="displayed" value="false"/>
							<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
								<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
									<c:set var="displayed" value="true"/>
									<span class="promotion">${promotion.description}</span>
								</c:if>
							</c:forEach>
						</c:forEach>
					</c:if>

					<c:if test="${ycommerce:doesAppliedPromotionExistForOrderEntry(cartData, entry.entryNumber)}">
						<c:forEach items="${cartData.appliedProductPromotions}" var="promotion">
							<c:set var="displayed" value="false"/>
							<c:forEach items="${promotion.consumedEntries}" var="consumedEntry">
								<c:if test="${not displayed && consumedEntry.orderEntryNumber == entry.entryNumber}">
									<c:set var="displayed" value="true"/>
									<span class="promotion">${promotion.description}</span>
								</c:if>
							</c:forEach>
						</c:forEach>
					</c:if>
					<common:configurationInfos entry="${entry}"/>
				</div>

				<%-- UNIT PRICE AND QANTITY --%>
				<div class="price price--line">
					<div class="price--line firstline">
						<c:if test="${entry.product.discountPrice ne null}">
							<div class="price--sticker">${entry.product.percentageDiscount}<spring:theme code="product.percentage.off"/></div>
						</c:if>
						<div class="price--line secondline">
							<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
								<product:productPricePanel product="${entry.product}" noStrikeCurrency="true"/>
							</ycommerce:testId>
						</div>
						<div class="checkout-order-summary-list-items-qty"><span><spring:theme code="basket.page.quantity"/>: </span>${entry.quantity}</div>
					</div>
					<div class="checkout-order-summary-list-items-price price"><format:priceDom priceData="${entry.totalPrice}" displayFreeForZero="false"/></div>
				</div>
				<c:if test="${entry.product.multidimensional}" >
					<a href="#" id="QuantityProductToggle" data-index="${loop.index}" class="showQuantityProductOverlay updateQuantityProduct-toggle">
						<span><spring:theme code="order.product.seeDetails"/></span>
					</a>
				</c:if>
				<spring:url value="/checkout/multi/getReadOnlyProductVariantMatrix" var="targetUrl"/>
				<grid:gridWrapper entry="${entry}" index="${loop.index}" styleClass="display-none"
					targetUrl="${targetUrl}"/>
			</div>
		</li>
	</c:if>
</c:forEach>
</ul>
