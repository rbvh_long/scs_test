<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%--
 SVG info icon
--%>
<?xml version="1.0" encoding="iso-8859-1"?>
<svg id="info" data-name="info" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="45px" height="45px">
	<title>info</title>
	<path class="cls-1" d="M23.56,1a23,23,0,1,0,23,23A23,23,0,0,0,23.56,1Zm0,44a21,21,0,1,1,21-21A21,21,0,0,1,23.56,45Z" />
	<path class="cls-1" d="M29.56,37h-5V20a1,1,0,0,0-1-1h-4a1,1,0,1,0,0,2h3V37h-5a1,1,0,1,0,0,2h12a1,1,0,0,0,0-2Z" />
	<circle class="cls-1" cx="22.56" cy="13" r="2" />
</svg>