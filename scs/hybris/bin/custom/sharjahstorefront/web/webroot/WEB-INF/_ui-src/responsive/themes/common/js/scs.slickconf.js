$(document).ready(function() {
	var classCarousel = 'js-carousel';
	var $carouselProduct = $('.' + classCarousel);
	var hiddenClass = 'hiddenClass';

	console.log('scs.slickconf.js - loaded');

	$carouselProduct.on('init', function(slick) {
		console.log('test');
		$(this).css({ opacity: 0 }).animate({ opacity: 1 }, 1000);
	});

	// Initialisation of the slick carousel
	$carouselProduct.each(function(e) {
		$(this).slickActivator();
	});


	// Sliding to chosen gallery image
	$('.js-gallery-carousel[data-slick-to-click="true"]').on('click', 'a.gallery-carousel--item', function(event) {
		event.preventDefault();
		if ((typeof $(this).data('index')) === 'number') {
			$carouselProduct.slick('slickGoTo', $(this).data('index'));
		}
	});
	$carouselProduct.each(function() {
		$(this).on('afterChange', function() {
			// Display of the slide depending on the device screen
			if (ACC.device.type === 'mobile') {
				$(this).find('.js-qty-selector').removeClass(hiddenClass);
				$(this).find('.slick-slide').not('.slick-current').each(function() {
					$(this).find('.js-qty-selector').addClass(hiddenClass);
				});
			}
		});
		// We will resize only product-tiles items
		if ($(this).find('.product-tile').length) {
			// Resize every product tiles with the highest height element from siblings
			$(this).on('setPosition', function() {
				$(this).find('.slick-slide').height('auto');
				var slickTrack = $(this).find('.slick-track');
				var slickTrackHeight = $(slickTrack).outerHeight();
				$(this).find('.slick-slide').css('height', slickTrackHeight + 'px');
			});
		}
	});


	var initSlick = function() {
		// Display of the slide depending on the device screen
		if (ACC.device.type === 'mobile') {
			$(this).find('.slick-current').next().find('.js-qty-selector').addClass(hiddenClass);
		}
		// The best selling block type
		// if (ACC.device.type === 'desktop') {
		// 	$('.best-selling .js-carousel').each(function(e) {
		// 		$(this).slick('unslick').addClass('unslicked');
		// 		$(this).addClass('best-selling--container');
		// 		$(this).removeClass('slick-carousel--product');
		// 	});
		// }
	};

	$(window).resize(function() {
		initSlick();
	}).resize();

});
