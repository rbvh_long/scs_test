<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>
<%@ taglib prefix="cms" uri="http://hybris.com/tld/cmstags" %>
<%@ taglib prefix="common" tagdir="/WEB-INF/tags/responsive/common" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>

<template:page pageTitle="${pageTitle}">

	<cms:pageSlot position="BottomHeaderSlot" var="component" element="div"	class="container-fluid">
	    <cms:component component="${component}" />
	</cms:pageSlot>

	<cms:pageSlot position="Section1" var="feature" element="div" class="product-grid-section1-slot">
		<cms:component component="${feature}" element="div" class="yComponentWrapper map product-grid-section1-component"/>
	</cms:pageSlot>

	<div class="product-list">
		<cms:pageSlot position="ProductLeftRefinements" var="feature" element="div" class="product-list--filters product-grid-left-refinements-slot">
			<cms:component component="${feature}" element="div" class="yComponentWrapper product-list--filters--container product-grid-left-refinements-component"/>
		</cms:pageSlot>

		<div class="product-list--results">
			<cms:pageSlot position="BandeauEmerchandising" var="feature" class="">
				<cms:component component="${feature}"  />
			</cms:pageSlot>

			<cms:pageSlot position="ProductGridSlot" var="feature" element="div" class="product-grid-right-result-slot">
				<cms:component component="${feature}" element="div" class="product-list--results--container product__list--wrapper yComponentWrapper product-grid-right-result-component"/>
			</cms:pageSlot>
		</div>
	</div>

	<storepickup:pickupStorePopup />
</template:page>
