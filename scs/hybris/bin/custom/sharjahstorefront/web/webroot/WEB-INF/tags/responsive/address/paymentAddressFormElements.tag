<%@ attribute name="regions" required="true" type="java.util.List"%>
<%@ attribute name="country" required="false" type="java.lang.String"%>
<%@ attribute name="tabIndex" required="false" type="java.lang.Integer"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="formElement" tagdir="/WEB-INF/tags/responsive/formElement" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
	<spring:theme code="please.select" var="selectElt"/>
	
   <formElement:formInputBox idKey="address.name" labelKey="address.name" path="billingAddress.name" inputCSS="form-control" /> 
	<formElement:formInputBox idKey="address.firstName" labelKey="address.firstName" path="billingAddress.firstName" inputCSS="form-control" mandatory="true" />
	<formElement:formInputBox idKey="address.surname" labelKey="address.surname" path="billingAddress.lastName" inputCSS="form-control" mandatory="true" />
	
	<spring:theme code="address.country.default" var="defaultCountry"/>
	<formElement:formInputBox idKey="defaultCountry" labelKey="address.country" path="billingAddress.line2" inputCSS="form-control" mandatory="true" disabled="true" value="${defaultCountry}"/>
	
	<formElement:formSelectBox idKey="address.townCity" labelKey="address.townCity" path="billingAddress.townCity"  mandatory="true"  
		skipBlankMessageKey="${selectElt}" items="${citiesList}" selectedValue="${sharjahAddressForm.townCity}" selectCSSClass="form-control" />
		
	<formElement:formSelectBox idKey="address.state" labelKey="address.state" path="billingAddress.state"  mandatory="true"  
		skipBlankMessageKey="${selectElt}" items="${areasList}" selectedValue="${sharjahAddressForm.state}" selectCSSClass="form-control"  />
   <div class="input-mobilephone">
		<div class="input-mobilephone--number register-section">
			<div class="register-section--form-label form-label"><spring:theme code="address.phone.mandatory" /></div>
			
			<c:choose>
				<c:when test="${currentLanguage.isocode eq 'en'}">	
			<div class="input-membershipnumber--number-wrapper no-margin">
				<div class="input-mobilephone--prefix">
					<formElement:formInputBox idKey="mobileprefixNumber"
						path="billingAddress.mobileprefixNumber"
						inputCSS="form-control" mandatory="true" value="+971"
						disabled="true" />
				</div>
				<div class="input-mobilephone--prefix0">
					<formElement:formInputBox idKey="mobileSecondPrefixNumber"
						path="billingAddress.mobileSecondPrefixNumber"
						inputCSS="form-control" mandatory="true" value="(0)"
						disabled="true" />
				</div>
				<div class="input-mobilephone--suffix">
					<formElement:formInputBox idKey="address.phone"
						path="billingAddress.phone" inputCSS="form-control"
						mandatory="true" placeholder="XXXXXXXXX" />
				</div>
			</div>
			</c:when>
			<c:otherwise>
			<div class="input-membershipnumber--number-wrapper no-margin">
				<div class="input-mobilephone--suffix">
					<formElement:formInputBox idKey="address.phone"
						path="billingAddress.phone" inputCSS="form-control"
						mandatory="true" placeholder="XXXXXXXXX" />
				</div>
				<div class="input-mobilephone--prefix0">
					<formElement:formInputBox idKey="mobileSecondPrefixNumber"
						path="billingAddress.mobileSecondPrefixNumber"
						inputCSS="form-control" mandatory="true" value="(0)"
						disabled="true" />
				</div>
				<div class="input-mobilephone--prefix">
					<formElement:formInputBox idKey="mobileprefixNumber"
						path="billingAddress.mobileprefixNumber"
						inputCSS="form-control" mandatory="true" value="971+"
						disabled="true" />
				</div>
			</div>
			</c:otherwise>
		</c:choose>
		</div>
   </div>
   <formElement:formInputBox idKey="address.street" labelKey="address.street" path="billingAddress.line1" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.building" labelKey="address.building" path="billingAddress.building" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.apartment" labelKey="address.apartment" path="billingAddress.apartment" inputCSS="form-control" mandatory="true"/>
	<formElement:formInputBox idKey="address.landmark" labelKey="address.landmark" path="billingAddress.landmark" inputCSS="form-control" mandatory="false"/>
	