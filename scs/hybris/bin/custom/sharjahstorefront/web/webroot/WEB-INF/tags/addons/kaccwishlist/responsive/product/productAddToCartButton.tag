<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="storepickup" tagdir="/WEB-INF/tags/responsive/storepickup" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%-- Based on productListerItem.tag and productAddToCartPanel.tag from the yacceleratorstorefront extension --%>

<div class="productAddToCart">

	<c:url value="/cart/add" var="addToCartUrl"/>
	<form:form method="post" id="addToCartForm${product.code}" class="add_to_cart_form" action="${addToCartUrl}">

		<input type="hidden" name="productCodePost" value="${product.code}"/>
		<c:set var="buttonType">button</c:set>

		<c:if test="${product.purchasable and product.stock.stockLevelStatus.code ne 'outOfStock' and not fn:contains(nonSelectableProductsSkus, product.code)}">
			<div class="checkbox">
				<input type="checkbox" class="js-entry-checkbox" id="${product.code}" />
				<label for="${product.code}"></label>
			</div>
		</c:if>

	</form:form>
	
</div>
