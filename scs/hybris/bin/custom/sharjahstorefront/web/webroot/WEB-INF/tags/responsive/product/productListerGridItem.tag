<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ attribute name="product" required="true" type="de.hybris.platform.commercefacades.product.data.ProductData" %>
<%@ taglib prefix="product" tagdir="/WEB-INF/tags/responsive/product" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="format" tagdir="/WEB-INF/tags/shared/format" %>
<%@ taglib prefix="action" tagdir="/WEB-INF/tags/responsive/action" %>
<%@ taglib prefix="ycommerce" uri="http://hybris.com/tld/ycommercetags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="addonProduct" tagdir="/WEB-INF/tags/addons/kaccwishlist/responsive/product" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<spring:theme code="text.addToCart" var="addToCartText"/>
<c:url value="/cart/add" var="addToCartUrl"/>
<c:url value="${product.url}" var="productUrl"/>
<c:set value="${not empty product.potentialPromotions}" var="hasPromotion"/>

<%--
<div class="product-list--results--container--display-item">
	<ycommerce:testId code="product_wholeProduct">
		<div class ="product-list--results--container--display-item--price price">
			<c:if test="${product.discountPrice ne null}">
				<p class="price--sticker">-${product.percentageDiscount}%
				</p>
			</c:if>
		</div>
		<a class="product-list--results--container--display-item--img" href="${productUrl}" title="${fn:escapeXml(product.name)}">
			<product:productPrimaryImage product="${product}" format="product"/>
		</a>
		<ycommerce:testId code="product_productName">
			<a class="product-list--results--container--display-item--name" href="${productUrl}">
				<c:choose>
					<c:when test="${fn:length(product.name) > 30}">
						<c:out value="${fn:substring(product.name, 0, 30)}..."/>
					</c:when>
					<c:otherwise>
						<c:out value="${product.name}" />
					</c:otherwise>
				</c:choose>
			</a>
		</ycommerce:testId>

		<div class="product-list--results--container--display-item--details">

			<c:if test="${not empty product.potentialPromotions}">
				<div class="product-list--results--container--display-item--details-promo">
					<c:forEach items="${product.potentialPromotions}" var="promotion">
						${promotion.description}
					</c:forEach>
				</div>
			</c:if>

			<ycommerce:testId code="product_productPrice">
				<div class="product-list--results--container--display-item--details-price price">
				<product:productListerItemPrice product="${product}"/>

				</div>
			</ycommerce:testId>

			<c:forEach var="variantOption" items="${product.variantOptions}">
				<c:forEach items="${variantOption.variantOptionQualifiers}" var="variantOptionQualifier">
					<c:if test="${variantOptionQualifier.qualifier eq 'rollupProperty'}">
						<c:set var="rollupProperty" value="${variantOptionQualifier.value}"/>
					</c:if>
					<c:if test="${variantOptionQualifier.qualifier eq 'thumbnail'}">
						<c:set var="imageUrl" value="${variantOptionQualifier.value}"/>
					</c:if>
					<c:if test="${variantOptionQualifier.qualifier eq rollupProperty}">
						<c:set var="variantName" value="${variantOptionQualifier.value}"/>
					</c:if>
				</c:forEach>
				<img style="width: 32px; height: 32px;" src="${imageUrl}" title="${variantName}" alt="${variantName}"/>
			</c:forEach>
		</div>


		<c:set var="product" value="${product}" scope="request"/>
		<c:set var="addToCartText" value="${addToCartText}" scope="request"/>
		<c:set var="addToCartUrl" value="${addToCartUrl}" scope="request"/>
		<c:set var="isGrid" value="true" scope="request"/>
		<div class="product-list--results--container--display-item--addToCart actions-container-for-${component.uid} <c:if test="${ycommerce:checkIfPickupEnabledForStore() and product.availableForPickup}"> pickup-in-store-available</c:if>">
			<action:actions element="div" parentComponent="${component}"/>
		</div>
		<addonProduct:productAddToWishlistButton product="${product}" />
	</ycommerce:testId>
</div>
 --%>

<div class="product-tile">
	<div class="product-tile--wrapper">
		<a href="${productUrl}" class="product-tile--link">
			<div class="product-tile--infos">
				<div class="product-tile--thumb">
					<c:if test="${product.discountPrice ne null}">
						<div class="price--sticker">-${product.percentageDiscount}%</div>
					</c:if>
					<product:productPrimaryImage product="${product}" format="product"/>
				</div>
				<div class="product-tile--name">${fn:escapeXml(product.name)}</div>
			</div>
		</a>
		<div class="product-tile--price price">
			<ycommerce:testId code="productDetails_productNamePrice_label_${product.code}">
				<product:productPricePanel product="${product}" />
			</ycommerce:testId>
		</div>
		<c:if test="${not product.multidimensional }">
			<form:form id="addToCartForm${product.code}" action="${addToCartUrl}" method="post" class="add_to_cart_form">
				<c:if test="${empty showAddToCart ? true : showAddToCart}">
					<div class="product-tile--qty-selector">
						<%-- QTY Selector --%>
						<product:productQuantitySelector product="${product}" />
					</div>
				</c:if>
				<c:if test="${product.stock.stockLevel gt 0}">
					<c:set var="productStockLevel">${product.stock.stockLevel}&nbsp;
						<spring:theme code="product.variants.in.stock"/>
					</c:set>
				</c:if>
				<c:if test="${product.stock.stockLevelStatus.code eq 'lowStock'}">
					<c:set var="productStockLevel">
						<spring:theme code="product.variants.only.left" arguments="${product.stock.stockLevel}"/>
					</c:set>
				</c:if>
				<c:if test="${isForceInStock}">
					<c:set var="productStockLevel">
						<spring:theme code="product.variants.available"/>
					</c:set>
				</c:if>
				<!-- 	<div class="stock-wrapper clearfix">
					${productStockLevel}
				</div> -->
				<c:if test="${multiDimensionalProduct}" >
					<c:url value="${product.url}/orderForm" var="productOrderFormUrl"/>
					<a href="${productOrderFormUrl}" class="button button--secondary js-add-to-cart glyphicon-list-alt">
						<div class="icon icon--cart-white">&nbsp;</div>
						<div class="button--label"><spring:theme code="order.form" /></div>
					</a>
				</c:if>
				<div class="product-tile--buttons add-button">
					<div class="product-tile--buttons-item add-button-whishlist">
						<button id="idFav" class="button button--tertiary addtolist add-button-whishlist-link js-add-product-to-wishlist"	data-add-product-to-wishlist-url="${pageContext.request.contextPath}/wishlist/addProductToWishlistPopup/${product.code}" data-title="${addProductToWishlistTitle}">
							<div class="icon icon--add-list">&nbsp;</div>
							<spring:theme code="text.whishlist.add" />
						</button>
					</div>
					<ycommerce:testId code="addToCartButton">
						<input type="hidden" name="productCodePost" value="${product.code}"/>
						<input type="hidden" name="productNamePost" value="${fn:escapeXml(product.name)}"/>
						<input type="hidden" name="productPostPrice" value="${product.price.value}"/>
						<div class="product-tile--buttons-item add-button-cart">
							<c:choose>
								<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
									<button class="button button--secondary add-button-toCart" aria-disabled="true" disabled="disabled">
										<div class="icon icon--cart-white">&nbsp;</div>
										<span><spring:theme code="text.addToCart" /></span>
									</button>
								</c:when>
								<c:otherwise>
									<button class="button button--secondary add-button-toCart js-enable-btn" disabled="disabled">
										<div class="icon icon--cart-white">&nbsp;</div>
										<div class="button--label"><spring:theme code="text.addToCart" /></div>
									</button>
								</c:otherwise>
							</c:choose>
						</div>
					</ycommerce:testId>
				</div>
			</form:form>
			<form:form id="configureForm${product.code}" action="${configureProductUrl}" method="get" class="configure_form">
				<c:if test="${product.configurable}">
					<c:choose>
						<c:when test="${product.stock.stockLevelStatus.code eq 'outOfStock' }">
							<button id="configureProduct" type="button" class="btn btn-primary btn-block" disabled="disabled">
								<spring:theme code="basket.configure.product"/>
							</button>
						</c:when>
						<c:otherwise>
							<button id="configureProduct" type="button" class="btn btn-primary btn-block js-enable-btn" disabled="disabled" onclick="location.href='${configureProductUrl}'">
									<spring:theme code="basket.configure.product"/>
							</button>
						</c:otherwise>
					</c:choose>
				</c:if>
			</form:form>
		</c:if>
	</div>
</div>
