<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ attribute name="galleryImages" required="true" type="java.util.List" %>
<%@ attribute name="galleryImages3D" required="true" type="java.util.List" %>

<div class="carousel gallery-carousel js-gallery-carousel hidden-xs hidden-sm" data-slick-to-click="true">
    <c:forEach items="${galleryImages}" var="container" varStatus="varStatus">
        <a href="#" class="gallery-carousel--item" data-index="${varStatus.index}"> <img class="lazyOwl" src="${container.thumbnail.url}" alt="${container.thumbnail.altText}"></a>
    </c:forEach>
    <c:if test="${not empty galleryImages3D}">
		<div class="sticker360 gallery-carousel--item">
		   <a href="#" class="btn btn-default open-view360 js-open-view360" >&nbsp;</a>
		</div>
		<div class="js-gallery-3D">
			 <div class="listImagesView360">
	            <ul>
		            <c:forEach items="${galleryImages3D}" var="media" varStatus="mediaStatus">
						 <li><img src="${media.url}" alt="Sharjah" /></li>
					</c:forEach>
	            </ul>
	            <p class="angle-view-controls">
		            <button class="btn btn-primary prev-image">Prev</button>
		            <button class="btn btn-primary next-image">Next</button>
		        </p>
	        </div>
		        
			 <c:set var="urlMedia" value="${galleryImages3D[0].url}" />
			 <c:set var="indexPt" value="${fn:indexOf(urlMedia, '.')}" />
			 <c:set var="indexPe" value="${fn:indexOf(urlMedia, '?')}" />
			 <c:set var="indexMedindexPeia" value="${fn:indexOf(urlMedia, '/')}" />
			 <c:set var="extension" value="${fn:substring(urlMedia, indexPt, indexPe)}" />
			 <c:set var="realFileName" value="${fn:substring(urlMedia, indexMedia+8, indexPe)}" />
			 <c:set var="str" value="${fn:split(realFileName, '-')}" />
			 <c:set var="str2" value="${fn:split(urlMedia, '?')}" />
			 <c:set var="realFile" value="${str[0]}-${str[1]}" />
		</div>
	</c:if>
</div>
