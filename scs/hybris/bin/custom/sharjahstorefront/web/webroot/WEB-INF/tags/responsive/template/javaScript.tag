<%@ tag body-content="empty" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ taglib prefix="theme" tagdir="/WEB-INF/tags/shared/theme" %>
<%@ taglib prefix="cms" tagdir="/WEB-INF/tags/responsive/template/cms" %>
<%@ taglib prefix="template" tagdir="/WEB-INF/tags/responsive/template" %>

<c:url value="/" var="siteRootUrl"/>

<template:javaScriptVariables/>

<%-- Custom generated bundle JS --%>
<c:choose>
	<%-- Test to load javascript for debug needs (add ?debug=true to URL) --%>
	<c:when test="${param.debug>=1}">
		<script type="text/javascript" src="${commonResourcePath}/js/main.js"></script>
	</c:when>
	<c:otherwise>
		<script type="text/javascript" src="${commonResourcePath}/js/main.min.js"></script>
	</c:otherwise>
</c:choose>

<%-- AddOn JavaScript files --%>
<c:forEach items="${addOnJavaScriptPaths}" var="addOnJavaScript">
    <script type="text/javascript" src="${addOnJavaScript}"></script>
</c:forEach>
<%-- <script src="${commonResourcePath}/js/svg4everybody.legacy.min.js"></script> --%>
<!-- <script>svg4everybody();</script> -->

<cms:previewJS cmsPageRequestContextData="${cmsPageRequestContextData}" />